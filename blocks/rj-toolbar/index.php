<?php

/**
 * Registers all block assets so that they can be enqueued through Gutenberg in
 * the corresponding context.
 *
 * Passes translations to JavaScript.
 */
function gutenberg_rj_toolbar_register_block() {

	if ( ! function_exists( 'register_block_type' ) ) {
		// Gutenberg is not active.
		return;
	}

	wp_register_script(
		'gutenberg-rj-toolbar',
		get_template_directory_uri() . '/blocks/rj-toolbar/block.js',
		array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor', 'wp-rich-text' ),
		'1.0'
	);

	wp_register_style(
		'gutenberg-rj-toolbar-editor',
		get_template_directory_uri() . '/blocks/rj-toolbar/editor.css',
		array( 'wp-edit-blocks' )
	);

	wp_register_style(
		'gutenberg-rj-toolbar',
		get_template_directory_uri() . '/blocks/rj-toolbar/style.css',
		array( )
	);

	register_block_type( 'gutenberg-examples/rj-toolbar-controls', array(
		'style' => 'gutenberg-rj-toolbar',
		'editor_style' => 'gutenberg-rj-toolbar-editor',
		'editor_script' => 'gutenberg-rj-toolbar',
	) );

  if ( function_exists( 'wp_set_script_translations' ) ) {
    /**
     * May be extended to wp_set_script_translations( 'my-handle', 'my-domain',
     * plugin_dir_path( MY_PLUGIN ) . 'languages' ) ). For details see
     * https://make.wordpress.org/core/2018/11/09/new-javascript-i18n-support-in-wordpress/
     */
    wp_set_script_translations( 'gutenberg-rj-toolbar', 'gutenberg-examples' );
  }

}
add_action( 'init', 'gutenberg_rj_toolbar_register_block' );
