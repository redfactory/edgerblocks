<?php

function myplugin_register_template() {
  $post_type_object = get_post_type_object( 'post' );

  $template = array(
    array( 'core/paragraph', array(
      'placeholder' => 'Add a root-level paragraph',
    ) ),
    array( 'core/columns', array(), array(
      array( 'core/column', array(), array(
        array( 'core/image', array() ),
      ) ),
      array( 'core/column', array(), array(
        array( 'core/paragraph', array(
          'placeholder' => 'Add a inner paragraph'
        ) ),
      ) ),
    ) )
  );

  $post_type_object->template = $template;
  $post_type_object->template_lock = 'all';
}
add_action( 'init', 'myplugin_register_template' );
