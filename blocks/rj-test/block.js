/**
* Hello World: Step 4
*
* Adding extra controls: built-in alignment toolbar.
*/
( function( wp, blocks, editor, i18n, element ) {
  var el = element.createElement;
  var __ = i18n.__;
  var RichText = editor.RichText;
  var InspectorControls = editor.InspectorControls;
  var withSelect = wp.data.withSelect;
  var AlignmentToolbar = editor.AlignmentToolbar;
  var BlockControls = editor.BlockControls;
  var PanelBody = wp.components.PanelBody;
  var TextControl = wp.components.TextControl;
  var ColorPalette = wp.components.ColorPalette;

  blocks.registerBlockType( 'gutenberg-examples/example-rj', {
    title: __( 'Remy test', 'gutenberg-examples' ),
    icon: 'megaphone',
    category: 'layout',

    attributes: {
      content: {
        type: 'array',
        source: 'children',
        selector: 'p',
      },
      alignment: {
        type: 'string',
        default: 'none',
      },
    },

    edit: function( props ) {
      var content = props.attributes.content;
      var alignment = props.attributes.alignment;
      var background_color = props.attributes.background_color;
      var block_style = props.attributes.block_style // To bind the style of the button
       block_style = {
           backgroundColor: background_color,
           textAlign: alignment,
       }

       function onChangeBgColor ( content ) {
        props.setAttributes({background_color: content})
       }

      function onChangeContent( newContent ) {
        props.setAttributes( { content: newContent } );
      }

      function onChangeAlignment( newAlignment ) {
        props.setAttributes( { alignment: newAlignment === undefined ? 'none' : newAlignment } );
      }

      return [
        el( InspectorControls, { key: 'inspector' },
          el( PanelBody, {
            title: __( 'Background color', 'gutenblock1' ),
            className: 'custom-bg-color',
            initialOpen: true,
          },
          ),
          el( ColorPalette, {
            type: 'string',
            label: __( 'Choose a color', 'gutenblock1' ),
            value: props.attributes.background_color,
            onChange: function( newColor ) {
              props.setAttributes( { background_color: newColor } );
            },
          }
        )
      ),
    el(
      BlockControls,
      { key: 'controls' },
      el(
        AlignmentToolbar,
        {
          value: alignment,
          onChange: onChangeAlignment,
        }
      )
    ),
    el(
      RichText,
      {
        key: 'richtext',
        tagName: 'p',
        style: block_style,
        className: props.className,
        onChange: onChangeContent,
        value: content,
      }
    ),
  ];
},

save: function( props ) {
  var block_style = {
     backgroundColor: props.attributes.background_color
 };
 console.log(block_style);

  return el( RichText.Content, {
    tagName: 'p',
    className: 'gutenberg-examples-align-' + props.attributes.alignment,
    value: props.attributes.content,
    style: block_style
  } );
},
} );
}(
  window.wp,
  window.wp.blocks,
  window.wp.editor,
  window.wp.i18n,
  window.wp.element
) );
