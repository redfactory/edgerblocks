<?php

/**
 * Registers all block assets so that they can be enqueued through Gutenberg in
 * the corresponding context.
 *
 * Passes translations to JavaScript.
 */
function gutenberg_examples_rj_register_block() {

	if ( ! function_exists( 'register_block_type' ) ) {
		// Gutenberg is not active.
		return;
	}

	wp_register_script(
		'gutenberg-examples-rj',
		get_template_directory_uri() . '/blocks/rj-test/block.js',
		array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor', 'wp-rich-text', 'wp-components'),
		'1.0'
	);

	wp_register_style(
		'gutenberg-examples-rj-editor',
		get_template_directory_uri() . '/blocks/rj-test/editor.css',
		array( 'wp-edit-blocks' ),
		'1.0'
	);

	wp_register_style(
		'gutenberg-examples-rj',
		get_template_directory_uri() . '/blocks/rj-test/style.css',
		array( ),
		'1.0'
	);

	register_block_type( 'gutenberg-examples/example-rj', array(
		'style' => 'gutenberg-examples-rj',
		'editor_style' => 'gutenberg-examples-rj-editor',
		'editor_script' => 'gutenberg-examples-rj',
		'attributes' => array(
						'className' => array(
								'type' => 'string',
						),
						'background_color' => array(
							'type' => 'string',
						),
				),
	) );

  if ( function_exists( 'wp_set_script_translations' ) ) {
    /**
     * May be extended to wp_set_script_translations( 'my-handle', 'my-domain',
     * plugin_dir_path( MY_PLUGIN ) . 'languages' ) ). For details see
     * https://make.wordpress.org/core/2018/11/09/new-javascript-i18n-support-in-wordpress/
     */
    wp_set_script_translations( 'gutenberg-examples-rj', 'gutenberg-examples' );
  }

}
add_action( 'init', 'gutenberg_examples_rj_register_block' );
