<?php
/**
 * Functions to register client-side assets (scripts and stylesheets) for the
 * Gutenberg block.
 *
 * @package gutenblock1
 */

/**
 * Registers all block assets so that they can be enqueued through Gutenberg in
 * the corresponding context.
 *
 * @see https://wordpress.org/gutenberg/handbook/blocks/writing-your-first-block-type/#enqueuing-block-scripts
 */
function block1_block_init() {
	// Skip block registration if Gutenberg is not enabled/merged.
	if ( ! function_exists( 'register_block_type' ) ) {
		return;
	}

	wp_register_script(
		'block1-block-editor',
		get_template_directory_uri() . '/blocks/block1/index.js',
		array(
			'wp-blocks',
			'wp-i18n',
			'wp-element',
			'wp-components',
			'wp-editor',
		)
	);

	wp_register_style(
		'block1-block-editor',
		get_template_directory_uri() . '/blocks/block1/editor.css',
		array()
	);

	wp_register_style(
		'block1-block',
		get_template_directory_uri() . '/blocks/block1/style.css',
		array()
	);

	register_block_type( 'gutenblock1/block1', array(
		'editor_script' => 'block1-block-editor',
		'editor_style'  => 'block1-block-editor',
		'style'         => 'block1-block',
		'attributes' => array(
            'className' => array(
                'type' => 'string',
            ),
            'title' => array(
            	'type' => 'string',
            ),
            'mycheckbox' => array(
            	'type' => 'string',
            ),
            'myselect' => array(
            	'type' => 'array',
            ),
        ),
		'render_callback' => 'rffw_render_block_custom_posts',
	) );
}
add_action( 'init', 'block1_block_init' );



function rffw_render_block_custom_posts( $attributes ) {
    $recent_posts = wp_get_recent_posts( array(
        'numberposts' => 3,
        'post_status' => 'publish',
    ) );

    if ( count( $recent_posts ) === 0 ) {
        return 'No posts';
    }

    $post_list = '<div class="wp-block-gutenblock1-block1 '. $attributes['className'] .'">';

    $post_list .= '<h3>' . $attributes['title'] . '</h3>';

    $post_list .= '<ul>';
    foreach ($recent_posts as $post) {
    	$post_id = $post['ID'];
    	$post_list .= sprintf(
	        '<li><a class="wp-block-my-plugin-latest-post" href="%1$s">%2$s</a></li>',
	        esc_url( get_permalink( $post_id ) ),
	        esc_html( get_the_title( $post_id ) )
	    );
    }
    $post_list .= '</ul>';

    $post_list .= '</div>';

    return $post_list;
}
