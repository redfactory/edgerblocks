( function( wp ) {

	var registerBlockType = wp.blocks.registerBlockType;
	//var RichText = wp.blocks.RichText;
	var InspectorControls = wp.editor.InspectorControls;
	//var ColorPalette = wp.blocks.ColorPalette;
	//var source = wp.blocks.source;
	var el = wp.element.createElement;	
	var withSelect = wp.data.withSelect;
	var ServerSideRender = wp.components.ServerSideRender;
	var PanelBody = wp.components.PanelBody;
	var TextControl = wp.components.TextControl;
	var CheckboxControl = wp.components.CheckboxControl;
	var SelectControl = wp.components.SelectControl;
	var __ = wp.i18n.__;

	console.log(wp.components);
	
	registerBlockType( 'gutenblock1/block1', {
		title: __( 'Block 1', 'gutenblock1' ),
		category: 'widgets',
		supports: {
			html: false,
		},

	    edit: function( props ) {
	        return [
	        	el(ServerSideRender, {
	        	    block: "gutenblock1/block1",
	        	    attributes:  props.attributes,
	        	}),
	        	el( InspectorControls, { key: 'inspector' },
					el( PanelBody, {
						title: __( 'Post type list', 'gutenblock1' ),
						className: 'block-gb-cta-link',
						initialOpen: true,
						},
						el( TextControl, {
							type: 'string',
							label: __( 'Enter a title for this block', 'gutenblock1' ),
							value: props.attributes.title,
							onChange: function( new_val ) {
								props.setAttributes( { title: new_val } );
							},
						} ),
						el( CheckboxControl, {
							label: __( 'Yes or no', 'gutenblock1' ),
							key: 'mycheckbox',
							checked: props.attributes.mycheckbox,
							onChange: function( new_val ) {
								props.setAttributes( { mycheckbox: new_val } );
							},
						} ),
						el( SelectControl, {
							label: __( 'Select a page', 'gutenblock1' ),
							multiple: 'true',
							options: [
	                            { label: __( 'region-1' ), value: 'region-1' },
	                            { label: __( 'region-2' ), value: 'region-2' }
                            ],
                            value: props.attributes.myselect,
							onChange: function( new_val ) {
								props.setAttributes( { myselect: new_val } );
							},
						} ),
					),
				),
	        ];
	    },

		save: function() {
			return null;
		}
	} );
} )(
	window.wp
);
