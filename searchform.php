<form method="get" action="<?php echo get_home_url(); ?>" class="search-form input-group">
  <input type="search" value="<?php echo get_search_query(); ?>" name="s" class="form-control" placeholder="<?php esc_attr_e('Search for...', 'edgerblocks'); ?>">
  <button type="submit">
    <i class="glyphicon glyphicon-search"></i>
  </button>
</form>
