<?php
function rffw_comment($comment, $args, $depth) {
    if ( 'div' === $args['style'] ) {
        $tag       = 'div';
        $add_below = 'comment';
    } else {
        $tag       = 'li';
        $add_below = 'div-comment';
    }
    ?>

    <<?php echo $tag ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">

        <?php if ( 'div' != $args['style'] ) : ?>
        <div id="div-comment-<?php comment_ID() ?>" class="comment-body">
        <?php endif; ?>

        <div class="comment-avatar comment-left">
            <?php if ( $args['avatar_size'] != 0 ) echo get_avatar( $comment, $args['avatar_size'] ); ?>
        </div>

        <div class="comment-right">

            <div class="comment-author">
                <span class="fn">
                    <?php echo get_comment_author_link(); ?>
                </span>

                <span class="comment-meta commentmetadata">
                    -
                    <a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ); ?>">
                        <?php
                        /* translators: 1: date, 2: time */
                        printf( esc_html__('%1$s at %2$s', 'edgerblocks'), get_comment_date(),  get_comment_time() ); ?>
                    </a>
                    <?php edit_comment_link( esc_html__( '(Edit)', 'edgerblocks' ), '  ', '' );
                    ?>
                </span>
            </div>

            <?php if ( $comment->comment_approved == '0' ) : ?>
                <em class="comment-awaiting-moderation"><?php esc_html__( 'Your comment is awaiting moderation.', 'edgerblocks' ); ?></em>
                <br />
            <?php endif; ?>

            <?php comment_text(); ?>

            <div class="reply">
                <?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
            </div>

        </div>

    <?php if ( 'div' != $args['style'] ) : ?>
    </div>
    <?php endif; ?>

<?php
}
?>
