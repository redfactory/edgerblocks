<?php
// Disable the embed shortcode when cookies are not allowed
function rf_detect_shortcode() {

    global $wp_query, $cookie_preference, $cookie_statistics, $cookie_marketing;

    $posts = $wp_query->posts;
    $pattern = get_shortcode_regex();

    foreach ($posts as $post) {
		if (   preg_match_all( '/'. $pattern .'/s', $post->post_content, $matches )
			&& array_key_exists( 2, $matches )
			&& in_array( 'embed', $matches[2] ) )
		{
			//print_r($matches);

			foreach ($matches[5] as $key=>$value) {

				// Soundcloud
				$match = strpos($value, 'soundcloud');
				if ($match !== false && $cookie_statistics == false && $cookie_marketing == false) {
					$post->post_content = str_replace($matches[0][$key], '<a href="'.$matches[5][$key].'" target="_blank">'.$matches[5][$key].'</a>', $post->post_content);
				}

				// Twitter
				$match = strpos($value, 'twitter');
				if ($match !== false && $cookie_statistics == false) {
					$post->post_content = str_replace($matches[0][$key], '<a href="'.$matches[5][$key].'" target="_blank">'.$matches[5][$key].'</a>', $post->post_content);
				}

				// Vimeo
				$match = strpos($value, 'vimeo');
				if ($match !== false && $cookie_statistics == false) {
					$post->post_content = str_replace($matches[0][$key], '<a href="'.$matches[5][$key].'" target="_blank">'.$matches[5][$key].'</a>', $post->post_content);
				}

				// Youtube
				$match = strpos($value, 'youtube');
				if ($match !== false && $cookie_statistics == false) {
					$post->post_content = str_replace($matches[0][$key], '<a href="'.$matches[5][$key].'" target="_blank">'.$matches[5][$key].'</a>', $post->post_content);
				}

				// Issuu
				$match = strpos($value, 'issuu');
				if ($match !== false && $cookie_statistics == false && $cookie_marketing == false) {
					$post->post_content = str_replace($matches[0][$key], '<a href="'.$matches[5][$key].'" target="_blank">'.$matches[5][$key].'</a>', $post->post_content);
				}

				// Spotify
				$match = strpos($value, 'spotify');
				if ($match !== false && $cookie_statistics == false && $cookie_marketing == false) {
					$post->post_content = str_replace($matches[0][$key], '<a href="'.$matches[5][$key].'" target="_blank">'.$matches[5][$key].'</a>', $post->post_content);
				}
			}
			//break;
		}
    }
}
add_action( 'wp', 'rf_detect_shortcode' );



// Disable automatic embed detection when cookies are not allowed
function rf_embedWrapper($html, $url, $attr, $post_id) {

	global $cookie_preference, $cookie_statistics, $cookie_marketing;

    if (strpos($html, 'soundcloud') !== false && $cookie_statistics == false && $cookie_marketing == false ) {
        return '<a href="' . $url . '" target="_blank">' . $url . '</a>';
    }

    if (strpos($html, 'twitter') !== false && $cookie_statistics == false ) {
        return '<a href="' . $url . '" target="_blank">' . $url . '</a>';
    }

    if (strpos($html, 'vimeo') !== false && $cookie_statistics == false ) {
        return '<a href="' . $url . '" target="_blank">' . $url . '</a>';
    }

    if (strpos($html, 'youtube') !== false && $cookie_statistics == false ) {
        return '<a href="' . $url . '" target="_blank">' . $url . '</a>';
    }

    if (strpos($html, 'issuu') !== false && $cookie_statistics == false && $cookie_marketing == false ) {
        return '<a href="' . $url . '" target="_blank">' . $url . '</a>';
    }

    if (strpos($html, 'spotify') !== false && $cookie_statistics == false && $cookie_marketing == false ) {
        return '<a href="' . $url . '" target="_blank">' . $url . '</a>';
    }

    return $html;
}
add_filter('embed_oembed_html', 'rf_embedWrapper', 10, 4);



//Disable iframes in content when cookies are not allowed
function rf_the_content_iframe_filter($content) {

	global $cookie_preference, $cookie_statistics, $cookie_marketing;

	if ($cookie_statistics == false || $cookie_marketing == false ) {
		$string = '<i>Dit onderdeel is niet beschikbaar door de huidige cookie instellingen.</i>';

		/*$cookiebot = '<script type="text/javascript">
			function CookiebotCallback_OnAccept() {
		        if (Cookiebot.consent.statistics && Cookiebot.consent.marketing) {
		            var widgetHTML = jQuery("dont-load-iframe").html();
		            widgetHTML = widgetHTML.replace(/<dont-load-iframe>/g, "<iframe>").replace(/<\/dont-load-iframe>/g, "</iframe>");
		            jQuery("dont-load-iframe").html(widgetHTML);
		        }
		        alert("change");
		    }
		</script>';*/

		$content = $str = preg_replace("#<iframe[^>]+>.*?</iframe>#is", $string, $content);

		//$content = str_replace('<iframe', '<dont-load-iframe', $content);
		//$content = str_replace('</iframe>', '</dont-load-iframe>' . $string, $content);

		//$content .= $cookiebot;
	}

  	return $content;
}
add_filter( 'the_content', 'rf_the_content_iframe_filter' );
