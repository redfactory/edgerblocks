<?php

add_filter( 'admin_head', 'rffw_vc_disable_frontend', 1 ); // For backend
add_filter( 'wp_head', 'rffw_vc_disable_frontend', 1 ); // For front-end
function rffw_vc_disable_frontend(){
  if ( function_exists( 'vc_disable_frontend' ) ) {
     vc_disable_frontend();
  }
}


function rffw_addCustomCssVC(){
  global $post;

  // Get sections ID's in order and the content position
  if(isset($post->rffw_meta['sections']) && !empty($post->rffw_meta['sections'])){

    $sections = json_decode($post->rffw_meta['sections']);
    wp_enqueue_style('rffw_vc_custom_section',	get_template_directory_uri() . '/css/vc-custom.css'	);

    foreach($sections as $section_object){

      // skip hidden sections
      if($section_object->visible){

        // filter custom sections
        if (is_numeric($section_object->id)){

          // Load section style by ID
          $post_custom_css = get_post_meta($section_object->id, '_wpb_post_custom_css', true );

          // if found add to vc styles
          if (!empty( $post_custom_css )){
            $post_custom_css = strip_tags( $post_custom_css );
            wp_add_inline_style( 'rffw_vc_custom_section',$post_custom_css );
          }

          $shortcodes_custom_css = get_post_meta($section_object->id, '_wpb_shortcodes_custom_css', true );
          // if found add to vc styles
          if (!empty( $shortcodes_custom_css )){
            $shortcodes_custom_css = strip_tags( $shortcodes_custom_css );
            wp_add_inline_style( 'rffw_vc_custom_section', $shortcodes_custom_css );
          }
        }
      }
    }
  }
}
add_action( 'wp_enqueue_scripts', 'rffw_addCustomCssVC' ,999);

?>
