<?php

function remove_title_tag() {
	remove_post_type_support('page', 'title');
}
add_action('init', 'remove_title_tag');
