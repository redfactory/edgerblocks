<?php
add_filter('post_gallery', 'rffw_post_gallery', 10, 2);
function rffw_post_gallery($output, $attr) {


    if (isset($attr['rffw_type']) && $attr['rffw_type'] == 'slider') {

        global $post;

        if (isset($attr['orderby'])) {
            $attr['orderby'] = sanitize_sql_orderby($attr['orderby']);
            if (!$attr['orderby'])
                unset($attr['orderby']);
        }

        extract(shortcode_atts(array(
            'order' => 'ASC',
            'orderby' => 'menu_order ID',
            'id' => $post->ID,
            'itemtag' => 'dl',
            'icontag' => 'dt',
            'captiontag' => 'dd',
            'columns' => 3,
            'size' => 'thumbnail',
            'include' => '',
            'exclude' => ''
        ), $attr));

        $id = intval($id);
        if ('RAND' == $order) $orderby = 'none';

        if (!empty($include)) {
            $include = preg_replace('/[^0-9,]+/', '', $include);
            $_attachments = get_posts(array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));

            $attachments = array();
            foreach ($_attachments as $key => $val) {
                $attachments[$val->ID] = $_attachments[$key];
            }
        }

        if (empty($attachments)) return '';

        $output = '<div class="js-swiper-gallery swiper-gallery">';
          $output .= '<div class="swiper-wrapper">';

          foreach ($attachments as $id => $attachment) {
              // Fetch the thumbnail (or full image, it's up to you)
              // $img = wp_get_attachment_image_src($id, 'medium');
              // $img = wp_get_attachment_image_src($id, 'my-custom-image-size');
              $img = wp_get_attachment_image_src($id, 'large');

            $output .= '<div class="swiper-slide"><a href="'.$img[0].'" rel="lightbox">';
              $output .= "<img src=\"{$img[0]}\" width=\"{$img[1]}\" height=\"{$img[2]}\" alt=\"\"  />";
            $output .= '</a></div>';
          }



          $output .= '</div>';
          $output .= '<div class="slider-arrows">
              <div class="swiper-button-prev prev" id="swiper-prev-'.$post->ID.'"></div>
              <div class="swiper-button-next next" id="swiper-next-'.$post->ID.'"></div>
          </div>';
        $output .= '</div>';

        $output .= "<script>";
        $output .= "  var swiper = new Swiper('.js-swiper-gallery', {";
        $output .= "    slidesPerView: 2,";
        $output .= "    spaceBetween: 30,";
        $output .= " prevButton: '.swiper-button-prev', nextButton: '.swiper-button-next', ";
        $output .= "breakpoints: {";
        $output .= "  991: {";
        $output .= "    slidesPerView: 2,";
        $output .= "    spaceBetween: 15,";
        $output .= "  },";
        $output .= "  767: {";
        $output .= "    slidesPerView: 1,";
        $output .= "    spaceBetween: 15,";
        $output .= "  }";
        $output .= "  }";

        $output .= "  });";
        $output .= "</script>";
    }
    return $output;
}


add_action('print_media_templates', function(){

  // define your backbone template;
  // the "tmpl-" prefix is required,
  // and your input field should have a data-setting attribute
  // matching the shortcode name
  ?>
  <script type="text/html" id="tmpl-rffw-gallery-setting">
    <label class="setting">
      <span><?php _e('RPO options', 'edgerblocks'); ?></span>
      <select data-setting="rffw_type">
        <option value="slider"> Slider </option>
        <option value="default"> Default </option>
      </select>
    </label>
  </script>

  <script>

    jQuery(document).ready(function(){

      // add your shortcode attribute and its default value to the
      // gallery settings list; $.extend should work as well...
      _.extend(wp.media.gallery.defaults, {
        rffw_type: 'default'
      });

      // merge default gallery settings template with yours
      wp.media.view.Settings.Gallery = wp.media.view.Settings.Gallery.extend({
        template: function(view){
          return wp.media.template('gallery-settings')(view)
               + wp.media.template('rffw-gallery-setting')(view);
        }
      });

    });

  </script>
  <?php

});
