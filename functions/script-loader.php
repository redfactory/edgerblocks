<?php
/**
 * Enqueue scripts and styles.
 */
function rffw_scripts() {
	global $edgerblocks;

	// Set empty if redux is not installed
	if(!isset($edgerblocks['REDUX_last_saved'])) $edgerblocks['REDUX_last_saved'] = '';

	wp_enqueue_style('bootstrap',  get_template_directory_uri().'/css/bootstrap.min.css', '3.3.7', true);

	wp_enqueue_style('edgerblocks-style', get_stylesheet_uri() , RFFW_VERSION);

  wp_enqueue_style('rffw_style', get_template_directory_uri().'/css/style.css', array('edgerblocks-style'), RFFW_VERSION.'.'.$edgerblocks['REDUX_last_saved']);

	wp_enqueue_script('bootstrap', get_template_directory_uri().'/js/bootstrap.min.js', array('jquery'), '3.3.4', true );

	wp_enqueue_script('fontawesome', get_template_directory_uri().'/js/fontawesome-all.min.js', array() , '5.0.6');

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	// These lines fix the Visual Composer bug where styles were not loaded on section pages
	if ( in_array( 'js_composer/js_composer.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
		wp_enqueue_style('js_composer_front', get_template_directory_uri().'/../../plugins/js_composer/assets/css/js_composer_front.css', array('style'));
	}

	wp_enqueue_script('modernizr', get_template_directory_uri().'/js/modernizr.custom.js', array('jquery'));

	wp_enqueue_script('dlmenu', get_template_directory_uri().'/js/jquery.dlmenu.js', array('jquery'));

	$dlmenu_data = array(
	    'backtext' => esc_html('Terug', 'edgerblocks'),
	);
	wp_localize_script( 'dlmenu', 'dlmenuData', $dlmenu_data );


	wp_enqueue_style('rffw_swiper', get_template_directory_uri().'/css/swiper.css', array('edgerblocks-style'));

	wp_enqueue_script('swiper', get_template_directory_uri().'/js/swiper.jquery.min.js', array('jquery'), '3.3.1');

	wp_enqueue_script('parallax', get_template_directory_uri().'/js/parallax.min.js', array('jquery'));

	wp_enqueue_script('snap', get_template_directory_uri().'/js/snap.svg-min.js', array('jquery'));

	wp_enqueue_script('masonry');

	wp_enqueue_script( 'jquery-ui-accordion' );
	wp_enqueue_script( 'jquery-ui-tabs' );

	wp_enqueue_script('imagesloaded');

	wp_enqueue_script('rffw_main', get_template_directory_uri().'/js/main.js', array('jquery'), RFFW_VERSION.'.'.$edgerblocks['REDUX_last_saved'], true);

	if (current_user_can('edit_posts')){
		wp_enqueue_style('rffw_frontendedit', get_template_directory_uri().'/admin/rffw-framework/css/frontend.css', array('edgerblocks-style'),  RFFW_VERSION.'.'.$edgerblocks['REDUX_last_saved']);
		wp_enqueue_script('rffw_frontendedit', get_template_directory_uri().'/admin/rffw-framework/js/frontend-edit.js', array('jquery'), RFFW_VERSION.'.'.$edgerblocks['REDUX_last_saved']);
	}
}
add_action('wp_enqueue_scripts', 'rffw_scripts' );
?>
