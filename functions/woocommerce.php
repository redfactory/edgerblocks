<?php

/**
 * Change number or products per row to 3
 */
add_filter('loop_shop_columns', 'loop_columns');
if (!function_exists('loop_columns')) {
	function loop_columns() {
		return 3; // 3 products per row
	}
}

//Tweak WooCommerce functions
add_filter('template_redirect', 'rffw_woocommerce_change_actions');
if (!function_exists('rffw_woocommerce_change_actions')) {
  function rffw_woocommerce_change_actions(){
    remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);
  }
}

?>
