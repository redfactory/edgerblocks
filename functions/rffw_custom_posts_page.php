<?php
function get_page_for_index_ids($posts_included=true) {

  $page_ids = array();

  foreach ( get_post_types( array(), 'objects' ) as $post_type ) {

    if('post' == $post_type->name && !$posts_included)  continue;

    if ( (! $post_type->has_archive && 'post' != $post_type->name) || ! $post_type->publicly_queryable ) {
      continue;
    }

    if ( 'post' === $post_type->name ) {
      $page_id = get_option( 'page_for_posts' );
    } else {
      $page_id = get_option( "page_for_{$post_type->name}" );
    }

    if ( ! $page_id ) {
      continue;
    }

    $page_ids[ $post_type->name ] =  apply_filters( 'wpml_object_id', $page_id, 'page', true);
  }

  return $page_ids;
}


add_filter( 'display_post_states', 'rffw_add_post_state', 10, 2 );
function rffw_add_post_state( $post_states, $post ) {
  $index_page_ids = get_page_for_index_ids(false);
	if( in_array($post->ID, $index_page_ids)) {
    $index_page_ids = array_flip($index_page_ids);
    $obj = get_post_type_object( $index_page_ids[$post->ID]);
		$post_states[] = $obj->label." ".__('page', 'edgerblocks');
	}
	return $post_states;
}



add_action( 'admin_init', 'rffw_hide_editor' );
function rffw_hide_editor() {
  // Get the Post ID.
  if(isset($_GET['post'])){
    $post_id = $_GET['post'];
  }
  elseif(isset($_POST['post_ID'])){
    $post_id = $_POST['post_ID'];
  }

  if( !isset( $post_id ) ) return;

  if(in_array($post_id, get_page_for_index_ids(false))){
    remove_post_type_support('page', 'editor');
    add_action( 'edit_form_after_title', 'rffw_wp_posts_page_notice' );
  }
}


if ( ! function_exists( 'get_page_for_post_type' ) ) {

	/**
	 * Get the page ID for the given or current post type
	 *
	 * @param bool|string $post_type
	 * @return bool|int
	 */
	function get_page_for_post_type( $post_type = false ) {
		if ( ! $post_type && is_post_type_archive() ) {
			$post_type = get_queried_object()->name;
		}
		if ( ! $post_type && is_singular() ) {
			$post_type = get_queried_object()->post_type;
		}
		if ( ! $post_type && in_the_loop() ) {
			$post_type = get_post_type();
		}
		if ( $post_type && in_array( $post_type, get_post_types() ) ) {
			return get_option( "page_for_{$post_type}", false );
		}
		return false;
	}
}

function rffw_wp_posts_page_notice() {
  echo '<div class="notice notice-warning inline"><p>' . __( 'You are currently editing the page that shows your latest custom posts.', 'edgerblocks'). '</p></div>';
}

add_action('template_redirect', 'rffw_change_object');
function rffw_change_object(){
    global $wp_the_query;

    // check for post type archive
    if( is_post_type_archive() ) {
      $post_id = get_page_for_post_type();

      if ($post_id) {
          $wp_the_query->queried_object = get_post( $post_id );
          $wp_the_query->queried_object_id = (int) $wp_the_query->queried_object->ID;
      }
    }
}
