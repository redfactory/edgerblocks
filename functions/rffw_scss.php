<?php
function rffw_scss_set_variables(){
  global $edgerblocks;

  $rf = (is_array($edgerblocks))? $edgerblocks : array();

    // Rebuild array flatten to work with scss
    function rffw_flatten($array, $prefix = '') {
        $result = array();
        foreach($array as $key=>$value) {
            if(is_array($value)) {
                $result = $result + rffw_flatten($value, $prefix . $key . '-');
            }
            else {
                $result[$prefix . $key] = $value;
            }
        }
        return $result;
    }

    // Flaten array with fuction
    $rf = rffw_flatten($rf);

    foreach ($rf as $key => $value) {
      // font-style empty fix add normal to font-style if empty so scss compiler works
      if(substr($key, -10) == 'font-style' && empty($value) ){
        $rf[$key] = 'normal';
      }
      // fix for url var's
      if(substr($key, -3) == 'url' || substr($key, -9) == 'thumbnail'){
        $rf[$key] = '"'.$value.'"';
      }
      // fix empty
      if(empty($rf[$key]) && ($rf[$key] != 0 || $rf[$key] != '0')){
        $rf[$key] = '""';
      }
    }


  return $rf;
}

add_filter('rffw_scss_variables','rffw_scss_set_variables');
