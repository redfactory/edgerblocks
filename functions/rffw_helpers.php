<?php

function rffw_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'rffw_mime_types');


add_filter('body_class', 'rffw_body_classes');
function rffw_body_classes($classes) {

    $classes[] = rffw_get_theme('layout-type');

    if (rffw_is_theme('search-on-type')) $classes[] = 'search-on-type';

    return $classes;
}


function rffw_top_section_border($post){
  $bg_color = rffw_get_border_color($post);

  if(rffw_is_meta('top-border', $post)){

    $border_type = rffw_get_meta('top-border', $post);
    if ($border_type == 'diagonal') {
        $border_path = 'M0 0 L100 100 L0 100 Z';
        $border_start = 'M0 100 L100 100 L0 100 Z';
    } elseif ($border_type == 'diagonal-down') {
        $border_path = 'M0 100 L100 0 L100 100 Z';
        $border_start = 'M0 100 L100 100 L100 100 Z';
    } elseif ($border_type == 'point-up') {
        $border_path = 'M0 100 L50 0 L100 100 Z';
        $border_start = 'M0 100 L50 100 L100 100 Z';
    } elseif ($border_type == 'point-down') {
        $border_path = 'M0 100 L0 0 L50 100 L100 0 L100 100 Z';
        $border_start = 'M0 100 L0 100 L50 100 L100 100 L100 100 Z';
    } elseif ($border_type == 'diagonal-tab') {
        $border_path = 'M0 100 L0 100 L10 0 L100 0 L100 100 Z';
        $border_start = 'M0 100 L0 0 L0 0 L100 0 L100 100 Z';
    } elseif ($border_type == 'waves') {
      ?>

      <svg viewBox="0 0 1350 120" preserveAspectRatio="none" class="wave border border-top loop <?php rffw_the_meta('bottom-border', $post); ?>">
          <path
            style="fill: <?php echo $bg_color; ?>;"
            id="wave-line-1"
            d="M287,64C414.3,64,1350,0,1350,0H0C0,0,116.2,64,287,64z"
            data-easing="mina.easeinout"
            data-speed="8000"
            data-path-loop="M1010.1,64C1137.4,64,1350,0,1350,0H0C0,0,839.3,64,1010.1,64z"
            data-path-original="M287,64C414.3,64,1350,0,1350,0H0C0,0,116.2,64,287,64z"
          />
          <path
            style="fill: <?php echo $bg_color; ?>;"
            fill-opacity="0.5"
            id="wave-line-2"
            d="M1076.6,78.4C1247.4,78.4,1350,0.2,1350,0.2H0C0,0.2,908.6,78.4,1076.6,78.4z"
            data-easing="mina.easeinout"
            data-speed="14000"
            data-path-loop="M307.1,78.4C477.8,78.4,1350,0.2,1350,0.2H0C0,0.2,139.5,78.4,307.1,78.4z"
            data-path-original="M1076.6,78.4C1247.4,78.4,1350,0.2,1350,0.2H0C0,0.2,908.6,78.4,1076.6,78.4z"
          />
      </svg>

      <?php
      $border_path = false;
    } else {
      $border_path = false;
    }

    if ( isset($border_path) && $border_path ) {
        $border_animation = rffw_get_meta('top-border-animation', $post);
        $border_path_loop = $border_path;

        if ($border_animation == 'scroll' || $border_animation == 'loop') {
            $border_path = $border_start;
        }
        ?>

        <svg viewBox="0 0 100 100" preserveAspectRatio="none" class="border border-top <?php rffw_the_meta('top-border', $post); ?> <?php echo $border_animation; ?>">
            <path
                d="<?php echo $border_path; ?>"
                data-path-original="<?php echo $border_path; ?>"
                data-path-scroll="<?php echo $border_path_loop; ?>"
                data-path-loop="<?php echo $border_path_loop; ?>"
                style="fill: <?php echo $bg_color; ?>;"
            />
        </svg>

    <?php }
  }
}

function rffw_bottom_section_border($post){
  $bg_color = rffw_get_border_color($post);

  if(rffw_is_meta('bottom-border', $post)){

    $border_type = rffw_get_meta('bottom-border', $post);
    if ($border_type == 'diagonal') {
        $border_path = 'M0 0 L100 100 L100 0 Z';
        $border_start = 'M0 0 L100 0 L100 0 Z';
    } elseif ($border_type == 'diagonal-down') {
        $border_path = 'M0 100 L100 0 L0 0 Z';
        $border_start = 'M0 0 L100 0 L0 0 Z';
    } elseif ($border_type == 'point-up') {
        $border_path = 'M0 0 L0 100 L50 0 L100 100 L100 0 Z';
        $border_start = 'M0 0 L0 0 L50 0 L100 0 L100 0 Z';
    } elseif ($border_type == 'point-down') {
        $border_path = 'M0 0 L50 100 L100 0 Z';
        $border_start = 'M0 0 L50 0 L100 0 Z';
    } elseif ($border_type == 'diagonal-tab') {
        $border_path = 'M0 0 L0 100 L10 0 Z';
        $border_start = 'M0 0 L0 0 L0 0 Z';
    } elseif ($border_type == 'waves') {
      ?>

      <svg viewBox="0 0 1350 120" preserveAspectRatio="none" class="wave border border-bottom loop <?php rffw_the_meta('bottom-border', $post); ?>">
          <path
            style="fill: <?php echo $bg_color; ?>;"
            id="wave-line-1"
            d="M287,64C414.3,64,1350,0,1350,0H0C0,0,116.2,64,287,64z"
            data-easing="mina.easeinout"
            data-speed="9000"
            data-path-loop="M1010.1,64C1137.4,64,1350,0,1350,0H0C0,0,839.3,64,1010.1,64z"
            data-path-original="M287,64C414.3,64,1350,0,1350,0H0C0,0,116.2,64,287,64z"
          />
          <path
            style="fill: <?php echo $bg_color; ?>;"
            fill-opacity="0.5"
            id="wave-line-2"
            d="M1076.6,78.4C1247.4,78.4,1350,0.2,1350,0.2H0C0,0.2,908.6,78.4,1076.6,78.4z"
            data-easing="mina.easeinout"
            data-speed="13000"
            data-path-loop="M307.1,78.4C477.8,78.4,1350,0.2,1350,0.2H0C0,0.2,139.5,78.4,307.1,78.4z"
            data-path-original="M1076.6,78.4C1247.4,78.4,1350,0.2,1350,0.2H0C0,0.2,908.6,78.4,1076.6,78.4z"
          />
      </svg>

      <?php
      $border_path = false;

    } else {
        $border_path = false;
    }

    if ( isset($border_path) && $border_path ) {
        $border_animation = rffw_get_meta('bottom-border-animation', $post);
        $border_path_loop = $border_path;

        if ($border_animation == 'scroll' || $border_animation == 'loop') {
            $border_path = $border_start;
        }
        ?>

        <svg viewBox="0 0 100 100" preserveAspectRatio="none" class="border border-bottom <?php rffw_the_meta('bottom-border', $post); ?> <?php rffw_the_meta('bottom-border-animation', $post); ?>">
            <path
                d="<?php echo $border_path; ?>"
                data-path-original="<?php echo $border_path; ?>"
                data-path-scroll="<?php echo $border_path_loop; ?>"
                data-path-loop="<?php echo $border_path_loop; ?>"
                style="fill: <?php echo $bg_color; ?>;"
            />
        </svg>

    <?php }
  }
}

function rffw_get_border_color($post){
  if(rffw_is_meta('header-bg-color', $post)){
    return rffw_get_meta('header-bg-color', $post);
  }
  elseif(rffw_is_meta('bg-color', $post)){
    return rffw_get_meta('bg-color', $post);
  }
  else{
    return '#fff' ;
  }
}


function get_rffw_breadcrumbs($overide_title = false){
    // Set variables for later use
    $here_text        = '';
    $home_link        = home_url('/');
    $home_text        = esc_html__( 'Home' , 'edgerblocks');
    $link_before      = '<span typeof="v:Breadcrumb">';
    $link_after       = '</span>';
    $link_attr        = ' ';
    $link             = $link_before . '<a' . $link_attr . ' href="%1$s">%2$s</a>' . $link_after;
    $delimiter        = /*' &gt; '*/ ' / ';              // Delimiter between crumbs
    $before           = '<span class="current">'; // Tag before the current crumb
    $after            = '</span>';                // Tag after the current crumb
    $page_addon       = '';                       // Adds the page number if the query is paged
    $breadcrumb_trail = '';
    $category_links   = '';

    /**
     * Set our own $wp_the_query variable. Do not use the global variable version due to
     * reliability
     */
    $wp_the_query   = $GLOBALS['wp_the_query'];
    $queried_object = $wp_the_query->get_queried_object();

    // Handle single post requests which includes single pages, posts and attatchments
    if ( is_singular() || get_option( 'page_for_posts' ) == $queried_object->ID ) {
        /**
         * Set our own $post variable. Do not use the global variable version due to
         * reliability. We will set $post_object variable to $GLOBALS['wp_the_query']
         */
        $post_object = sanitize_post( $queried_object );

        // Set variables
        $title          = apply_filters( 'the_title', $post_object->post_title );
        $parent         = $post_object->post_parent;
        $post_type      = $post_object->post_type;
        $post_id        = $post_object->ID;
        $post_link      = $before . $title . $after;
        $parent_string  = '';
        $post_type_link = '';

        if ( 'post' === $post_type )
        {
            // Get the post categories
            $categories = get_the_category( $post_id );
            if ( $categories ) {
                // Lets grab the first category
                $category  = $categories[0];

                $category_links = get_category_parents( $category, true, $delimiter );
                $category_links = str_replace( '<a',   $link_before . '<a' . $link_attr, $category_links );
                $category_links = str_replace( '</a>', '</a>' . $link_after,             $category_links );
            }
        }

        if ( !in_array( $post_type, ['post', 'page', 'attachment'] ) )
        {
            $post_type_object = get_post_type_object( $post_type );
            $archive_link     = esc_url( get_post_type_archive_link( $post_type ) );

            $post_type_link   = sprintf( $link, $archive_link, $post_type_object->labels->singular_name );
        }

        // Get post parents if $parent !== 0
        if ( 0 !== $parent )
        {
            $parent_links = [];
            while ( $parent ) {
                $post_parent = get_post( $parent );

                $parent_links[] = sprintf( $link, esc_url( get_permalink( $post_parent->ID ) ), get_the_title( $post_parent->ID ) );

                $parent = $post_parent->post_parent;
            }

            $parent_links = array_reverse( $parent_links );

            $parent_string = implode( $delimiter, $parent_links );
        }

        // Lets build the breadcrumb trail
        if ( $parent_string ) {
            $breadcrumb_trail = $parent_string . $delimiter . $post_link;
        } else {
            $breadcrumb_trail = $post_link;
        }

        if ( $post_type_link )
            $breadcrumb_trail = $post_type_link . $delimiter . $breadcrumb_trail;

        if ( $category_links )
            $breadcrumb_trail = $category_links . $breadcrumb_trail;
    }

    // Handle archives which includes category-, tag-, taxonomy-, date-, custom post type archives and author archives
    if( is_archive() )
    {
        if (    is_category()
             || is_tag()
             || is_tax()
        ) {
            // Set the variables for this section
            $term_object        = get_term( $queried_object );
            $taxonomy           = $term_object->taxonomy;
            $term_id            = $term_object->term_id;
            $term_name          = $term_object->name;
            $term_parent        = $term_object->parent;
            $taxonomy_object    = get_taxonomy( $taxonomy );
            $current_term_link  = $before . $taxonomy_object->labels->singular_name . ': ' . $term_name . $after;
            $parent_term_string = '';

            if ( 0 !== $term_parent )
            {
                // Get all the current term ancestors
                $parent_term_links = [];
                while ( $term_parent ) {
                    $term = get_term( $term_parent, $taxonomy );

                    $parent_term_links[] = sprintf( $link, esc_url( get_term_link( $term ) ), $term->name );

                    $term_parent = $term->parent;
                }

                $parent_term_links  = array_reverse( $parent_term_links );
                $parent_term_string = implode( $delimiter, $parent_term_links );
            }

            if ( $parent_term_string ) {
                $breadcrumb_trail = $parent_term_string . $delimiter . $current_term_link;
            } else {
                $breadcrumb_trail = $current_term_link;
            }

        } elseif ( is_author() ) {

            $breadcrumb_trail = esc_html__( 'Author archive for ', 'edgerblocks') .  $before . $queried_object->data->display_name . $after;

        } elseif ( is_date() ) {
            // Set default variables
            $year     = $wp_the_query->query_vars['year'];
            $monthnum = $wp_the_query->query_vars['monthnum'];
            $day      = $wp_the_query->query_vars['day'];

            // Get the month name if $monthnum has a value
            if ( $monthnum ) {
                $date_time  = DateTime::createFromFormat( '!m', $monthnum );
                $month_name = $date_time->format( 'F' );
            }

            if ( is_year() ) {

                $breadcrumb_trail = $before . $year . $after;

            } elseif( is_month() ) {

                $year_link        = sprintf( $link, esc_url( get_year_link( $year ) ), $year );

                $breadcrumb_trail = $year_link . $delimiter . $before . $month_name . $after;

            } elseif( is_day() ) {

                $year_link        = sprintf( $link, esc_url( get_year_link( $year ) ),             $year       );
                $month_link       = sprintf( $link, esc_url( get_month_link( $year, $monthnum ) ), $month_name );

                $breadcrumb_trail = $year_link . $delimiter . $month_link . $delimiter . $before . $day . $after;
            }

        } elseif ( is_post_type_archive() ) {

            $post_type        = $wp_the_query->query_vars['post_type'];
            $post_type_object = get_post_type_object( $post_type );

            if($overide_title) $breadcrumb_trail = $before . $overide_title . $after;
            else $breadcrumb_trail = $before . $post_type_object->labels->name . $after;



        }
    }

    // Handle the search page
    if ( is_search() ) {
        $breadcrumb_trail = esc_html__( 'Zoekresultaten voor: ', 'edgerblocks' ) . $before . get_search_query() . $after;
    }

    // Handle 404's
    if ( is_404() ) {
        $breadcrumb_trail = $before . esc_html__( 'Error 404', 'edgerblocks' ) . $after;
    }

    // Handle paged pages
    if ( is_paged() ) {
        $current_page = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : get_query_var( 'page' );
        $page_addon   = $before . sprintf( esc_html__( ' ( Pagina %s )' , 'edgerblocks'), number_format_i18n( $current_page ) ) . $after;
    }

    $breadcrumb_output_link  = '';
    if ( is_front_page() ) {
        // Do not show breadcrumbs on page one of home and frontpage
        if ( is_paged() ) {
            $breadcrumb_output_link .= $here_text;
            $breadcrumb_output_link .= '<a href="' . $home_link . '">' . $home_text . '</a>';
            $breadcrumb_output_link .= $page_addon;
        }
    } else {
        $breadcrumb_output_link .= $here_text;
        $breadcrumb_output_link .= '<a href="' . $home_link . '">' . $home_text . '</a>';
        $breadcrumb_output_link .= $delimiter;
        $breadcrumb_output_link .= $breadcrumb_trail;
        $breadcrumb_output_link .= $page_addon;
    }

    if (isset($breadcrumb_output_link) && $breadcrumb_output_link != '') {
      return '<div class="breadcrumb">' . $breadcrumb_output_link . '</div>';
    }
}

function the_rffw_breadcrumbs($overide_title = false){
  echo get_rffw_breadcrumbs($overide_title);
}


function get_rffw_title(){

    $rffw_title       = '';
    $space          = ' ';
    $wp_the_query   = $GLOBALS['wp_the_query'];
    $queried_object = $wp_the_query->get_queried_object();


    if( is_archive() ) {

        if ( is_category() ) {
          $rffw_title = single_cat_title('', false);
          //$rffw_title = single_cat_title(esc_html__('Category: ', 'edgerblocks'), false);

        } elseif (is_tag() ) {

        	$rffw_title = single_tag_title(esc_html__('Tag: ','edgerblocks'), false);


        } elseif ( is_tax() ) {
          $taxonomy_object    = get_taxonomy( $taxonomy );
          $rffw_title = $taxonomy_object->labels->singular_name . ': ' . $term_name;


        } elseif ( is_author() ) {

          $rffw_title = esc_html__( 'Author archive for ', 'edgerblocks') .   $queried_object->data->display_name;

        } elseif ( is_date() ) {
            // Set default variables
            $year     = $wp_the_query->query_vars['year'];
            $monthnum = $wp_the_query->query_vars['monthnum'];
            $day      = $wp_the_query->query_vars['day'];

            // Get the month name if $monthnum has a value
            if ( $monthnum ) {
                $date_time  = DateTime::createFromFormat( '!m', $monthnum );
                $month_name = $date_time->format( 'F' );
            }

            if ( is_year() ) {

                $rffw_title  =  $year;

            } elseif( is_month() ) {

                $rffw_title =  $year . $space .  $month_name;

            } elseif( is_day() ) {

                $rffw_title  =  $year . $space .  $month_name . $space .  $day;
            }

        }
        elseif ( is_post_type_archive() ) {

            $post_type        = $wp_the_query->query_vars['post_type'];
            $post_type_object = get_post_type_object( $post_type );

            $rffw_title  =  $post_type_object->labels->name;
        }
    }

    // Handle the search page
    if ( is_search() ) {
        $rffw_title = esc_html__( 'Zoekresultaten: ', 'edgerblocks' ) .  get_search_query();
    }

    // Handle 404's
    if ( is_404() ) {
        $rffw_title =  esc_html__( 'Error 404', 'edgerblocks' );
    }

    if (!(is_404()) && (is_single()) || (is_page())) {
        $rffw_title = wp_title('',true);
    }

    // Handle paged pages
    if ( is_paged() ) {
        $current_page = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : get_query_var( 'page' );
        $rffw_title   .= sprintf( esc_html__( ' ( Pagina %s )' , 'edgerblocks'), number_format_i18n( $current_page ) );
    }

    if (is_home()) {
        $rffw_title = get_the_title( get_option( 'page_for_posts' ) );
    }

    return $rffw_title;
}
function the_rffw_title(){
  echo get_rffw_title();
}





// Test functie die de hele header title/subtitle/breadcrumb opmaakt
function get_rffw_complete_title($post, $index_page_post){

    $rffw_title       = '';
    $rffw_subtitle    = '';
    $space          = ' ';
    $wp_the_query   = $GLOBALS['wp_the_query'];
    $queried_object = $wp_the_query->get_queried_object();

    if ( is_singular() || (isset($index_page_post) && is_object($index_page_post)) ) {
        $post_object = sanitize_post( $queried_object );

        // Set variables
        $title          = apply_filters( 'the_title', $post_object->post_title );
        $parent         = $post_object->post_parent;
        $post_type      = $post_object->post_type;
        $post_id        = $post_object->ID;
        $post_link      = $title;
        $parent_string  = '';
        $post_type_link = '';

        if ( 'post' === $post_type ) {
            // Get the post categories
            $categories = get_the_category( $post_id );
            if ( $categories ) {
                // Lets grab the first category
                $category  = $categories[0];

                $category_links = get_category_parents( $category, true);
                $category_links = str_replace( '<a', '<span', $category_links );
                $category_links = str_replace( '</a>', '</span>', $category_links );

                $rffw_title = $category_links;
            }
        }

        if ( !in_array( $post_type, ['post', 'page', 'attachment'] ) ) {
            $post_type_object = get_post_type_object( $post_type );

            $rffw_title = $post_type_object->labels->singular_name;
        }
        // Get post parents if $parent !== 0
        if ( 0 !== $parent ) {
            $parent_links = [];
            while ( $parent ) {
                $post_parent = get_post( $parent );
                if(isset($link))
                  $parent_links[] = sprintf( $link, esc_url( get_permalink( $post_parent->ID ) ), get_the_title( $post_parent->ID ) );

                $parent = $post_parent->post_parent;
            }

            $parent_links = array_reverse( $parent_links );
            $parent_string = implode( ' / ', $parent_links );

            $rffw_title = $parent_string;
        }
        if(rffw_is_meta('subtitle', $post)) {
          $rffw_title = get_the_title(); //wp_title('',false);
          $rffw_subtitle = rffw_get_meta('subtitle', $post);
        } else {
          if ( 'post' !== $post_type ) {
            $rffw_subtitle = get_the_title(); //wp_title('',false);
          }else{
            $rffw_title = get_the_title(); //wp_title('',false);
          }
        }
    }
    if( is_archive() ) {

        // Woocommerce
        if ( function_exists('is_woocommerce') &&  is_woocommerce() && apply_filters( 'woocommerce_show_page_title', true ) ) {
            $rffw_title = '';
            $rffw_subtitle = woocommerce_page_title(false);
        }
        elseif ( is_category() ) {

          $rffw_title = '';
          //$rffw_subtitle = single_cat_title(esc_html__('Category: ', 'edgerblocks'), false);
          $rffw_subtitle = single_cat_title('', false);

        }
        elseif (is_tag() ) {

          $rffw_title = '';
          $rffw_subtitle = single_tag_title(esc_html__('Tag: ','edgerblocks'), false);

        } elseif ( is_tax() ) {
          $taxonomy_object    = get_taxonomy( $taxonomy );
          $rffw_title = '';
          $rffw_subtitle = $taxonomy_object->labels->singular_name . ': ' . $term_name;

        } elseif ( is_author() ) {

          $rffw_title = '';
          $rffw_subtitle = esc_html__( 'Author archive for ', 'edgerblocks') .   $queried_object->data->display_name;

        } elseif ( is_date() ) {
            // Set default variables
            $year     = $wp_the_query->query_vars['year'];
            $monthnum = $wp_the_query->query_vars['monthnum'];
            $day      = $wp_the_query->query_vars['day'];

            // Get the month name if $monthnum has a value
            if ( $monthnum ) {
                $date_time  = DateTime::createFromFormat( '!m', $monthnum );
                $month_name = $date_time->format( 'F' );
            }

            if ( is_year() ) {

                $rffw_subtitle  =  $year;

            } elseif( is_month() ) {

                $rffw_subtitle =  $year . $space .  $month_name;

            } elseif( is_day() ) {

                $rffw_subtitle  =  $year . $space .  $month_name . $space .  $day;
            }

        }
        elseif ( is_post_type_archive() ) {
            $post_type  = $wp_the_query->query_vars['post_type'];
            $post_id = get_page_for_post_type($post_type);
            if ($post_id) {
                $rffw_subtitle = get_the_title( $post_id );
            } else {
              $post_type_object = get_post_type_object( $post_type );
              $rffw_subtitle  =  $post_type_object->labels->name;
            }

        }
    }
    // Handle the search page
    if ( is_search() ) {
        $rffw_subtitle = esc_html__( 'Zoekresultaten: ', 'edgerblocks' ) .  get_search_query();
    }

    // Handle 404's
    if ( is_404() ) {
        $rffw_subtitle = (rffw_is_theme('404-title'))? rffw_get_theme('404-title') : esc_html__( 'Error 404 Page not found!', 'edgerblocks' );
    }

    // Handle paged pages
    if ( is_paged() ) {
        $current_page = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : get_query_var( 'page' );
        $rffw_subtitle   .= sprintf( esc_html__( ' ( Pagina %s )' , 'edgerblocks'), number_format_i18n( $current_page ) );
    }

    $rffw_output_string = '';

    if(is_single()){
      //post titles
      if (!empty($rffw_title)) $rffw_output_string .= '<h1 class="page-title title';
      if (!empty($rffw_title) && empty($rffw_subtitle)) $rffw_output_string .= ' no-margin';
      if (!empty($rffw_title)) $rffw_output_string .= '">' . $rffw_title . '</h1>';
      if (!empty($rffw_subtitle)) $rffw_output_string .= '<div class="page-subtitle">' . $rffw_subtitle . '</div>';
    }else{
      if (!empty($rffw_title)) $rffw_output_string .= '<div class="page-subtitle';
      if (!empty($rffw_title) && empty($rffw_subtitle)) $rffw_output_string .= ' no-margin';
      if (!empty($rffw_title)) $rffw_output_string .= '">' . $rffw_title . '</div>';
      if (!empty($rffw_subtitle)) $rffw_output_string = '<h1 class="page-title title">' . $rffw_subtitle . '</h1>';
    }
    return $rffw_output_string;
}

function rffw_custom_js() {
  if(rffw_is_theme('custom-js')) rffw_the_theme('custom-js');
}
add_action('wp_head', 'rffw_custom_js', 999);


function rffw_create_section_for_index($rffw_section=false){
  // not set create a new empty object
  if(!is_object($rffw_section)) $rffw_section = new stdClass();

  // if not set set default here
  $rffw_section = rffw_option_default_index($rffw_section, 'boxed');
  $rffw_section = rffw_option_default_index($rffw_section, 'click-through-link');
  $rffw_section = rffw_option_default_index($rffw_section, 'pagination');
  $rffw_section = rffw_option_default_index($rffw_section, 'show-content');
  $rffw_section = rffw_option_default_index($rffw_section, 'show-image');

  $rffw_section->sidebar 			                    = (rffw_is_meta('sidebar', $rffw_section))  ? rffw_get_meta('sidebar', $rffw_section)  : rffw_get_theme('index-sidebar');
  $rffw_section->id_attribute                     = 'section-index-'.get_post_type();
  $rffw_section->rffw_meta['inline_style']        = (isset($rffw_section->rffw_meta['inline_style_no_image']))? $rffw_section->rffw_meta['inline_style_no_image']: '';
  $rffw_section->real	                            = false;

  return $rffw_section;

}
function rffw_option_default_index($rffw_section, $option){

  $rffw_section->rffw_meta[$option] = (rffw_is_meta($option, $rffw_section)) ? rffw_get_meta($option, $rffw_section) : 1;
  return  $rffw_section;
}


function rffw_exclude_category( $query ) {
    //exclude presentatie category
    $cat_id = 43;
    if ( $query->is_home() && $query->is_main_query() &&  !$query->is_admin) {
      $child_cats = (array) get_term_children( $cat_id, 'category' );
      $child_cats[] = $cat_id;
      $query->set( 'category__not_in', array_merge($child_cats));
    }
}
add_action( 'pre_get_posts', 'rffw_exclude_category' );
