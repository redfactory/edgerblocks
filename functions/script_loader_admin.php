<?php
// Load backend scripts
function rffw_admin_script_loader()
{
    global $rffw_section_types, $edgerblocks;

    wp_enqueue_style('simplegrid', get_template_directory_uri().'/admin/rffw-framework/css/simplegrid.css');

    wp_enqueue_style('custom_redux', get_template_directory_uri().'/admin/rffw-framework/css/custom_redux.css');

    // remove file and this lines
    wp_enqueue_style('backend-main', get_template_directory_uri().'/admin/rffw-framework/css/backend-main.css');

    // Load css on iframe / iframe is active
    if(isset($_REQUEST['iframe']) && $_REQUEST['iframe'] == 1) {
        wp_enqueue_style('iframe_style', get_template_directory_uri().'/admin/rffw-framework/css/iframe.css');
    }

    // add backend color
    $theme_primary_color = (isset($edgerblocks['theme-primary-color']['color']))? $edgerblocks['theme-primary-color']['color'] : '#f56a16';
  //   $custom_css = ".rffw-admin-menu-item .wp-menu-image::before{
	// 	color: ".$theme_primary_color." !important;
	// }
	// a.rffw-admin-menu-item-sub::before{
	// 	color: ".$theme_primary_color." !important;
	// }
	// .admin-color-fresh #redux-header,
	// .wp-customizer #redux-header {
	// 	background: ".$theme_primary_color.";
	// 	border: ".$theme_primary_color.";
	// }";
  //   wp_add_inline_style('backend-main', $custom_css);

    // Add the color picker css file
    wp_enqueue_style('wp-color-picker');

    wp_enqueue_style('alpha-color-picker', get_template_directory_uri().'/admin/rffw-framework/css/alpha-color-picker.css');

    wp_enqueue_script('jquery-ui-core', array('jquery'));

    wp_enqueue_style('wp-color-picker');
    wp_enqueue_script('wp-color-picker-alpha', get_template_directory_uri().'/admin/rffw-framework/js/alpha-color-picker.js', array( 'wp-color-picker' ), '1.2');

    wp_enqueue_script('upload', get_template_directory_uri().'/admin/rffw-framework/js/upload.js', array('jquery'));

    wp_enqueue_script('media-upload');
    wp_enqueue_media();

    wp_enqueue_script('thickbox');
    wp_enqueue_style('thickbox');

    wp_enqueue_script('jquery-ui-slider');
    wp_enqueue_script('jquery-ui-sortable');

    wp_enqueue_script('jquery-ui-sortable-nested', get_template_directory_uri().'/admin/rffw-framework/js/jquery-ui-sortable-nested.js');

    wp_enqueue_script('sortable', get_template_directory_uri().'/admin/rffw-framework/js/sortable.js', array('jquery-ui-sortable-nested'));

    wp_register_script('pagetemplateselect', get_template_directory_uri().'/admin/rffw-framework/js/pagetemplateselect.js');
    wp_localize_script(
        'pagetemplateselect', 'rf', array(
        'rffw_section_types' => $rffw_section_types,
        )
    );
    wp_enqueue_script('pagetemplateselect');

    wp_enqueue_script('jsvalueslider', get_template_directory_uri().'/admin/rffw-framework/js/valueslider.js');
    wp_enqueue_script('mainjs', get_template_directory_uri().'/admin/rffw-framework/js/main.js', array('jquery-ui-accordion'));

}
add_action('admin_enqueue_scripts', 'rffw_admin_script_loader');
?>
