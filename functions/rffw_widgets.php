<?php


function rffw_areas_register_sidebars(){
		$widgets = get_posts( 'post_type=widget_area&post_status=publish&order=ASC&posts_per_page=999&fields=ids' );

		if( !empty( $widgets ) && is_array( $widgets ) ){
				foreach ( $widgets as $widget ) {
						$meta = get_post_meta( $widget );
						$post_widget = get_post($widget);

						register_sidebar( array(
								'name'          =>  __( 'Widget Area ' . $post_widget->post_title, 'edgerblocks' ),
								'id'            => 'widget-areas-' . $widget,
								'description'   => $post_widget->post_excerpt,
								'before_widget' => '<aside id="%1$s" class="widget %2$s rffw-col-replace-class">',
								'after_widget'  => '</aside>',
								'before_title'  => '<h3 class="widget-title">',
								'after_title'   => '</h3>',
						) );
				}
		}
}
add_action( 'widgets_init', 'rffw_areas_register_sidebars' );

function rffw_areas_options_page() {
	add_submenu_page(
		'themes.php',
					__( 'Widget Areas', 'edgerblocks' ),
					__( 'Widget Areas', 'edgerblocks' ),
					'manage_options',
					'edit.php?post_type=widget_area'
	);
}
add_action( 'admin_menu', 'rffw_areas_options_page', 10 );

/**
	* Register widget area.
	*
	* @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
	*/
	function rffw_widgets_init() {
		/*
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar Posts', 'edgerblocks' ),
		'id'            => 'sidebar-post',
		'description'   => esc_html__( 'Appears next to the post content', 'edgerblocks' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s rffw-col-replace-class">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	));

	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar Pages', 'edgerblocks' ),
		'id'            => 'sidebar-page',
		'description'   => esc_html__( "Appears next to the page content", 'edgerblocks' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s rffw-col-replace-class">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	));

	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar Solution', 'edgerblocks' ),
		'id'            => 'sidebar-solution',
		'description'   => esc_html__( "Appears next to the solution content", 'edgerblocks' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s rffw-col-replace-class">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	));

	register_sidebar( array(
		'name'          => esc_html__( 'Index Achive Sidebar', 'edgerblocks' ),
		'id'            => 'sidebar-index',
		'description'   => esc_html__( "Appears next to the index", 'edgerblocks' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s rffw-col-replace-class">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	));
*/
	register_sidebar(array(
		'name' => esc_html__( 'Footer widgets', 'edgerblocks'),
		'id' => 'footer-widgets',
		'description' => esc_html__( 'Appears in the footer area', 'edgerblocks'),
		'before_widget' => '<aside id="%1$s" class="widget %2$s rffw-col-replace-class">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	));

	register_sidebar(array(
		'name' => esc_html__( 'Footer bottom widgets', 'edgerblocks'),
		'id' => 'footer-bottom-widgets',
		'description' => esc_html__( 'Appears in the footer bottom area', 'edgerblocks'),
		'before_widget' => '<aside id="%1$s" class="widget %2$s rffw-col-replace-class">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	));

	register_sidebar(array(
		'name' => esc_html__( 'Footer mobile widgets', 'edgerblocks'),
		'id' => 'footer-mobile-widgets',
		'description' => esc_html__( 'Appears in the footer on mobile', 'edgerblocks'),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	));

}
add_action( 'widgets_init', 'rffw_widgets_init' );


function rffw_widget_force_title( $title ){
	if ( !isset($title) || empty($title) ) $title = '<!-- -->';
	return $title;
}
add_filter('widget_title','rffw_widget_force_title');


function rffw_the_widget_builder($sidebar_name){

	// render and save to var
	ob_start();
	dynamic_sidebar($sidebar_name);
	$sidebar = ob_get_clean();

	// count nummer of classes found
	preg_match_all( '/rffw-col-replace-class/', $sidebar, $matches );

	// Select replacement class
	$count = count( $matches[0] );
	if( $count > 0 ) {
		$replacements = array(
			1 => 'col-sm-12',
			2 => 'col-sm-6',
			3 => 'col-sm-4',
			4 => 'col-lg-3 col-sm-6',
			5 => 'col-lg-3 col-sm-6',
			6 => 'col-lg-2 col-sm-4',
			7 => 'col-lg-2 col-sm-4',
			8 => 'col-lg-2 col-sm-4',
			9 => 'col-lg-2 col-sm-4',
			10 => 'col-lg-2 col-sm-4',
			11 => 'col-lg-2 col-sm-4',
			12 => 'col-lg-2 col-sm-4',
			13 => 'col-lg-2 col-sm-4',
			14 => 'col-lg-2 col-sm-4',
			15 => 'col-lg-2 col-sm-4',
			16 => 'col-lg-2 col-sm-4',
		);
		
		// replace and echo
		echo preg_replace( '/rffw-col-replace-class/', $replacements[$count], $sidebar );
	}
}
?>
