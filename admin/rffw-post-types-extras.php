<?php
// The posttypes are set in the edgerblocks tools plugin

// Make posttype work with theme options + add default sections to postype
$rffw_post_types = array(
  "post"		=> array(
		"name" 	=> "Post",
    "default-sections" => array(
      'header' 		=> __('The page header', 'edgerblocks'),
      'content' 	=> __('The page content', 'edgerblocks'),
      'related' 	=> __('The related content', 'edgerblocks'),
    )
	),
  "page"		=> array(
		"name" 	=> "Page",
		"default-sections" => array(
			'header' 		=> __('The page header', 'edgerblocks'),
			'content' 	=> __('The page content', 'edgerblocks'),
		),
	),
  // "section"	=> array(
	// 	"name" 	=> "Section",
	// ),
  // "team"		=> array(
	// 	"name" 	=> "Team",
	// ),
  "programma-data"		=> array(
		"name" 	=> "Programma Data",
		"default-sections" => array(
			'header' 		=> __('The page header', 'edgerblocks'),
			'content' 	=> __('The page content', 'edgerblocks'),
		),
	),
  "omroep"		=> array(
    "name" 	=> "Omroep",
    "default-sections" => array(
      'header' 		=> __('The page header', 'edgerblocks'),
      'content' 	=> __('The page content', 'edgerblocks'),
    ),
  ),
  // "solution"	=> array(
	// 	"name" 	=> "Solution",
	// 	"default-sections" => array(
	// 		'header' 		=> __('The page header', 'edgerblocks'),
	// 		'content' 	=> __('The page content', 'edgerblocks'),
	// 	),
	// ),
  // "product"		=> array(
  //   "name" 	=> "Product",
  //   "default-sections" => array(
  //     'header' 		=> __('The product header', 'edgerblocks'),
  //     'content' 	=> __('The procuct content', 'edgerblocks'),
  //   ),
  // ),
  // "block"		=> array(
  //   "name" 	=> "Block",
  // ),
  // "client"		=> array(
  //   "name" 	=> "Client",
  // ),
);


      // 
      //
			// // Custom post type for sections
			// $section_labels = array(
			//   'name' => __('Sections', 'edgerblocks'),
			//   'singular_name' => __('Sections', 'edgerblocks'),
			//   'add_new' => __('Add New', 'edgerblocks'),
			//   'add_new_item' => __('Add New Section', 'edgerblocks'),
			//   'edit_item' => __('Edit Section', 'edgerblocks'),
			//   'new_item' => __('New Section', 'edgerblocks'),
			//   'view_item' => __('View Section', 'edgerblocks'),
			//   'search_items' => __('Search Sections', 'edgerblocks'),
			//   'not_found' =>  __('No Sections found', 'edgerblocks'),
			//   'not_found_in_trash' => __('No Sections found in Trash', 'edgerblocks'),
			//   'parent_item_colon' => '',
			//   'menu_name' => 'Sections',
      //
			// );
			// $section_args = array(
			//   'labels' => $section_labels,
			//   'public' => false,
			//   'publicly_queryable' => false,
			//   'exclude_from_search' => true,
			//   'show_in_admin_bar'   => false,
			//   'show_in_nav_menus'   => false,
			//   'query_var'           => false,
			//   'show_ui' => true,
			//   'show_in_menu' => true,
			//   'menu_icon' => 'dashicons-list-view',
			//   'query_var' => true,
			//   'rewrite' => false,
			//   'capability_type' => 'post',
			//   'has_archive' => false,
			//   'hierarchical' => false,
			//   'menu_position' => 20,
			//   'supports' => array('title','thumbnail','editor','custom-fields'),
      //
			// );
			// register_post_type('section', $section_args);




			$labels = array(
				'name'                  => _x( 'Omroepen', 'Post Type General Name', 'edgerblocks' ),
				'singular_name'         => _x( 'Omroep', 'Post Type Singular Name', 'edgerblocks' ),
				'menu_name'             => __( 'Omroepen', 'edgerblocks' ),
				'name_admin_bar'        => __( 'Omroep Type', 'edgerblocks' ),
				'archives'              => __( 'Item Archives', 'edgerblocks' ),
				'attributes'            => __( 'Item Attributes', 'edgerblocks' ),
				'parent_item_colon'     => __( 'Parent Item:', 'edgerblocks' ),
				'all_items'             => __( 'All Items', 'edgerblocks' ),
				'add_new_item'          => __( 'Add New Item', 'edgerblocks' ),
				'add_new'               => __( 'Add New', 'edgerblocks' ),
				'new_item'              => __( 'New Item', 'edgerblocks' ),
				'edit_item'             => __( 'Edit Item', 'edgerblocks' ),
				'update_item'           => __( 'Update Item', 'edgerblocks' ),
				'view_item'             => __( 'View Item', 'edgerblocks' ),
				'view_items'            => __( 'View Items', 'edgerblocks' ),
				'search_items'          => __( 'Search Item', 'edgerblocks' ),
				'not_found'             => __( 'Not found', 'edgerblocks' ),
				'not_found_in_trash'    => __( 'Not found in Trash', 'edgerblocks' ),
				'featured_image'        => __( 'Featured Image', 'edgerblocks' ),
				'set_featured_image'    => __( 'Set featured image', 'edgerblocks' ),
				'remove_featured_image' => __( 'Remove featured image', 'edgerblocks' ),
				'use_featured_image'    => __( 'Use as featured image', 'edgerblocks' ),
				'insert_into_item'      => __( 'Insert into item', 'edgerblocks' ),
				'uploaded_to_this_item' => __( 'Uploaded to this item', 'edgerblocks' ),
				'items_list'            => __( 'Items list', 'edgerblocks' ),
				'items_list_navigation' => __( 'Items list navigation', 'edgerblocks' ),
				'filter_items_list'     => __( 'Filter items list', 'edgerblocks' ),
			);
			$args = array(
				'label'                 => __( 'Omroep', 'edgerblocks' ),
				'description'           => __( 'Dit zijn de Omroepen', 'edgerblocks' ),
				'labels'                => $labels,
				'supports'              => array( 'title', 'thumbnail', ),
				//'taxonomies'            => array( 'deliverable', 'field' ),
				'hierarchical'          => false,
				'public'                => true,
				'show_ui'               => true,
				'show_in_menu'          => true,
			//	'menu_position'         => 5,
				'menu_icon'             => 'dashicons-welcome-view-site',
				'show_in_admin_bar'     => true,
				'show_in_nav_menus'     => true,
				'can_export'            => true,
				'has_archive'           => true,
				'exclude_from_search'   => false,
				'publicly_queryable'    => true,
				'capability_type'       => 'page',
			);
			register_post_type( 'omroep', $args );

      $labels = array(
				'name'                  => _x( 'Events programma data', 'Post Type General Name', 'edgerblocks' ),
				'singular_name'         => _x( 'Programma data', 'Post Type Singular Name', 'edgerblocks' ),
				'menu_name'             => __( 'Programma', 'edgerblocks' ),
				'name_admin_bar'        => __( 'Programma', 'edgerblocks' ),
				'archives'              => __( 'Item Archives', 'edgerblocks' ),
				'attributes'            => __( 'Item Attributes', 'edgerblocks' ),
				'parent_item_colon'     => __( 'Parent Item:', 'edgerblocks' ),
				'all_items'             => __( 'All Items', 'edgerblocks' ),
				'add_new_item'          => __( 'Add New Item', 'edgerblocks' ),
				'add_new'               => __( 'Add New', 'edgerblocks' ),
				'new_item'              => __( 'New Item', 'edgerblocks' ),
				'edit_item'             => __( 'Edit Item', 'edgerblocks' ),
				'update_item'           => __( 'Update Item', 'edgerblocks' ),
				'view_item'             => __( 'View Item', 'edgerblocks' ),
				'view_items'            => __( 'View Items', 'edgerblocks' ),
				'search_items'          => __( 'Search Item', 'edgerblocks' ),
				'not_found'             => __( 'Not found', 'edgerblocks' ),
				'not_found_in_trash'    => __( 'Not found in Trash', 'edgerblocks' ),
				'featured_image'        => __( 'Featured Image', 'edgerblocks' ),
				'set_featured_image'    => __( 'Set featured image', 'edgerblocks' ),
				'remove_featured_image' => __( 'Remove featured image', 'edgerblocks' ),
				'use_featured_image'    => __( 'Use as featured image', 'edgerblocks' ),
				'insert_into_item'      => __( 'Insert into item', 'edgerblocks' ),
				'uploaded_to_this_item' => __( 'Uploaded to this item', 'edgerblocks' ),
				'items_list'            => __( 'Items list', 'edgerblocks' ),
				'items_list_navigation' => __( 'Items list navigation', 'edgerblocks' ),
				'filter_items_list'     => __( 'Filter items list', 'edgerblocks' ),
			);
			$args = array(
				'label'                 => __( 'Programma data', 'edgerblocks' ),
				'description'           => __( 'Plaats hier de Events programma data', 'edgerblocks' ),
				'labels'                => $labels,
				'supports'              => array( 'title', 'editor', 'thumbnail', ),
				//'taxonomies'            => array( 'deliverable', 'field' ),
				'hierarchical'          => false,
				'public'                => false,
				'show_ui'               => true,
				'show_in_menu'          => true,
			//	'menu_position'         => 5,
				'menu_icon'             => 'dashicons-calendar-alt',
				'show_in_admin_bar'     => true,
				'show_in_nav_menus'     => true,
				'can_export'            => true,
				'has_archive'           => true,
				'exclude_from_search'   => false,
				'publicly_queryable'    => true,
				'capability_type'       => 'page',
			);
			register_post_type( 'programma-data', $args );


      // Labels
      $singular = 'Event Dag';
			$plural = 'Event dagen';
			$labels = array(
			  'name' => _x( $plural, "taxonomy general name", 'edgerblocks'),
			  'singular_name' => _x( $singular, "taxonomy singular name", 'edgerblocks'),
			  'search_items' =>  __("Search $singular", 'edgerblocks'),
			  'all_items' => __("All $singular", 'edgerblocks'),
			  'parent_item' => __("Parent $singular", 'edgerblocks'),
			  'parent_item_colon' => __("Parent $singular:", 'edgerblocks'),
			  'edit_item' => __("Edit $singular", 'edgerblocks'),
			  'update_item' => __("Update $singular", 'edgerblocks'),
			  'add_new_item' => __("Add New $singular", 'edgerblocks'),
			  'new_item_name' => __("New $singular Name", 'edgerblocks'),
			);
			// Register and attach to 'team' post type
			register_taxonomy( "event-dag", 'programma-data', array(
			  'public' => true,
			  'show_ui' => true,
			  'show_in_nav_menus' => true,
			  'hierarchical' => false,
			  'query_var' => true,
			  'rewrite' => false,
			  'labels' => $labels
			));

			// Labels
			$singular = 'Tijd';
			$plural = 'Tijden';
			$labels = array(
			  'name' => _x( $plural, "taxonomy general name", 'edgerblocks'),
			  'singular_name' => _x( $singular, "taxonomy singular name", 'edgerblocks'),
			  'search_items' =>  __("Search $singular", 'edgerblocks'),
			  'all_items' => __("All $singular", 'edgerblocks'),
			  'parent_item' => __("Parent $singular", 'edgerblocks'),
			  'parent_item_colon' => __("Parent $singular:", 'edgerblocks'),
			  'edit_item' => __("Edit $singular", 'edgerblocks'),
			  'update_item' => __("Update $singular", 'edgerblocks'),
			  'add_new_item' => __("Add New $singular", 'edgerblocks'),
			  'new_item_name' => __("New $singular Name", 'edgerblocks'),
			);
			// Register and attach to 'team' post type
			register_taxonomy( strtolower($singular), 'programma-data', array(
			  'public' => true,
			  'show_ui' => true,
			  'show_in_nav_menus' => true,
			  'hierarchical' => false,
			  'query_var' => true,
			  'rewrite' => false,
			  'labels' => $labels
			));

      // Labels
			$singular = 'Locatie';
			$plural = 'Locaties';
			$labels = array(
			  'name' => _x( $plural, "taxonomy general name", 'edgerblocks'),
			  'singular_name' => _x( $singular, "taxonomy singular name", 'edgerblocks'),
			  'search_items' =>  __("Search $singular", 'edgerblocks'),
			  'all_items' => __("All $singular", 'edgerblocks'),
			  'parent_item' => __("Parent $singular", 'edgerblocks'),
			  'parent_item_colon' => __("Parent $singular:", 'edgerblocks'),
			  'edit_item' => __("Edit $singular", 'edgerblocks'),
			  'update_item' => __("Update $singular", 'edgerblocks'),
			  'add_new_item' => __("Add New $singular", 'edgerblocks'),
			  'new_item_name' => __("New $singular Name", 'edgerblocks'),
			);
			// Register and attach to 'team' post type
			register_taxonomy( strtolower($singular), 'programma-data', array(
			  'public' => true,
			  'show_ui' => true,
			  'show_in_nav_menus' => true,
			  'hierarchical' => false,
			  'query_var' => true,
			  'rewrite' => false,
			  'labels' => $labels
			));


			 $labels = array(
			   'name'                  => _x( 'Widget Areas', 'Widget Area General Name', 'edgerblocks' ),
			   'singular_name'         => _x( 'Widget Area', 'Widget Area Singular Name', 'edgerblocks' ),
			   'menu_name'             => __( 'Widget Areas', 'edgerblocks' ),
			   'name_admin_bar'        => __( 'Widget Area', 'edgerblocks' ),
			   'parent_item_colon'     => __( 'Parent Item:', 'edgerblocks' ),
			   'all_items'             => __( 'All Widget Areas', 'edgerblocks' ),
			   'add_new_item'          => __( 'Add New Widget Area', 'edgerblocks' ),
			   'add_new'               => __( 'Add New', 'edgerblocks' ),
			   'new_item'              => __( 'New Widget Area', 'edgerblocks' ),
			   'edit_item'             => __( 'Edit Widget Area', 'edgerblocks' ),
			   'update_item'           => __( 'Update Widget Area', 'edgerblocks' ),
			   'view_item'             => __( 'View Widget Area', 'edgerblocks' ),
			   'view_items'            => __( 'View Widget Areas', 'edgerblocks' ),
			       'search_items'        => __( 'Search Widgets', 'edgerblocks' ),
			 'not_found'           => sprintf( __( 'Whoops, looks like you haven\'t created any widget areas yet. <a href="%s">Create one now</a>!', 'edgerblocks' ), admin_url( 'post-new.php?post_type=widget_area' ) ),
			 'not_found_in_trash'  => __( 'No items found in Trash.', 'edgerblocks' ),
			 );
			 $args = array(
			   'label'                 => __( 'Widget Area', 'edgerblocks' ),
			   'description'           => __( 'Add a widget area to your theme to use on pages', 'edgerblocks' ),
			   'labels'                => $labels,
			   'supports'              => array( 'title', 'excerpt'  ),
			   'hierarchical'          => false,
			   'public'                => false,
			   'show_ui'               => true,
			   'show_in_menu'          => false,
			   'show_in_admin_bar'     => false,
			   'show_in_nav_menus'     => false,
			   'can_export'            => true,
			   'has_archive'           => true,
			   'exclude_from_search'   => true,
			   'publicly_queryable'    => false,
			   'capability_type'       => 'page',
			 );
			 register_post_type( 'widget_area', $args );


       $labels = array(
 				'name'                  => _x( 'Nominaties', 'Post Type General Name', 'edgerblocks' ),
 				'singular_name'         => _x( 'Nominatie', 'Post Type Singular Name', 'edgerblocks' ),
 				'menu_name'             => __( 'Nominatie', 'edgerblocks' ),
 				'name_admin_bar'        => __( 'Nominatie', 'edgerblocks' ),
 				'archives'              => __( 'Item Archives', 'edgerblocks' ),
 				'attributes'            => __( 'Item Attributes', 'edgerblocks' ),
 				'parent_item_colon'     => __( 'Parent Item:', 'edgerblocks' ),
 				'all_items'             => __( 'All Items', 'edgerblocks' ),
 				'add_new_item'          => __( 'Add New Item', 'edgerblocks' ),
 				'add_new'               => __( 'Add New', 'edgerblocks' ),
 				'new_item'              => __( 'New Item', 'edgerblocks' ),
 				'edit_item'             => __( 'Edit Item', 'edgerblocks' ),
 				'update_item'           => __( 'Update Item', 'edgerblocks' ),
 				'view_item'             => __( 'View Item', 'edgerblocks' ),
 				'view_items'            => __( 'View Items', 'edgerblocks' ),
 				'search_items'          => __( 'Search Item', 'edgerblocks' ),
 				'not_found'             => __( 'Not found', 'edgerblocks' ),
 				'not_found_in_trash'    => __( 'Not found in Trash', 'edgerblocks' ),
 				'featured_image'        => __( 'Featured Image', 'edgerblocks' ),
 				'set_featured_image'    => __( 'Set featured image', 'edgerblocks' ),
 				'remove_featured_image' => __( 'Remove featured image', 'edgerblocks' ),
 				'use_featured_image'    => __( 'Use as featured image', 'edgerblocks' ),
 				'insert_into_item'      => __( 'Insert into item', 'edgerblocks' ),
 				'uploaded_to_this_item' => __( 'Uploaded to this item', 'edgerblocks' ),
 				'items_list'            => __( 'Items list', 'edgerblocks' ),
 				'items_list_navigation' => __( 'Items list navigation', 'edgerblocks' ),
 				'filter_items_list'     => __( 'Filter items list', 'edgerblocks' ),
 			);
 			$args = array(
 				'label'                 => __( 'Nominatie', 'edgerblocks' ),
 				'description'           => __( 'Plaats hier de Nominaties', 'edgerblocks' ),
 				'labels'                => $labels,
 				'supports'              => array( 'title', 'editor', 'thumbnail', ),
 				'taxonomies'            => array( 'cat-nominatie' ),
 				'hierarchical'          => false,
 				'public'                => false,
 				'show_ui'               => true,
 				'show_in_menu'          => true,
 			//	'menu_position'         => 5,
 				'menu_icon'             => 'dashicons-awards',
 				'show_in_admin_bar'     => true,
 				'show_in_nav_menus'     => true,
 				'can_export'            => true,
 				'has_archive'           => true,
 				'exclude_from_search'   => false,
 				'publicly_queryable'    => true,
 				'capability_type'       => 'page',
 			);
 			register_post_type( 'nominatie', $args );


       // Labels
       $singular = 'Nominatie Categorie';
 			$plural = 'Nominatie Categorieën';
 			$labels = array(
 			  'name' => _x( $plural, "taxonomy general name", 'edgerblocks'),
 			  'singular_name' => _x( $singular, "taxonomy singular name", 'edgerblocks'),
 			  'search_items' =>  __("Search $singular", 'edgerblocks'),
 			  'all_items' => __("All $singular", 'edgerblocks'),
 			  'parent_item' => __("Parent $singular", 'edgerblocks'),
 			  'parent_item_colon' => __("Parent $singular:", 'edgerblocks'),
 			  'edit_item' => __("Edit $singular", 'edgerblocks'),
 			  'update_item' => __("Update $singular", 'edgerblocks'),
 			  'add_new_item' => __("Add New $singular", 'edgerblocks'),
 			  'new_item_name' => __("New $singular Name", 'edgerblocks'),
 			);
 			// Register and attach to 'team' post type
 			register_taxonomy( "cat-nominatie", 'nominatie', array(
 			  'public' => true,
 			  'show_ui' => true,
 			  'show_in_nav_menus' => true,
 			  'hierarchical' => false,
 			  'query_var' => true,
 			  'rewrite' => false,
 			  'labels' => $labels
 			));


// Add Feature image to postype
add_filter('manage_block_posts_columns', 'rffw_featured_image_columns');
add_action('manage_block_posts_custom_column', 'rffw_featured_image_columns_content', 10, 2);

function rffw_featured_image_columns($defaults) {
		$defaults['featured_image'] = 'Featured Image';
		return $defaults;
}
function rffw_featured_image_columns_content($column_name, $post_ID=0) {
		if ($column_name == 'featured_image') {
        echo get_the_post_thumbnail($post_ID, 'thumbnail');
		}
}


// Add collum section type
add_filter('manage_section_posts_columns', 'rffw_columns_head_type', 10);
add_action('manage_section_posts_custom_column', 'rffw_columns_content_type', 10, 2);
function rffw_columns_head_type($defaults) {
    $new = array();
    foreach($defaults as $key=>$value) {
        $new[$key]=$value;
        if($key=='title') {  // when we find the date column
          $new['type'] = 'Type';
					$new['section_used_in'] = 'Section Used In';
        }
    }
    return $new;
}
function rffw_columns_content_type($column_name, $post_id) {
	global $rffw_section_types;
    if ($column_name == 'type') {
			$rffw_sectiontype =  get_post_meta( $post_id, '_rffw_sectiontype', true );
		  $rffw_section_type_key = array_search($rffw_sectiontype, array_column($rffw_section_types, 'slug'));
			esc_html_e($rffw_section_types[$rffw_section_type_key]['name'], 'edgerblocks');
    }
		elseif ($column_name == 'section_used_in') {
			rffw_section_in();
		}
}

// Make collum sortable
add_filter( 'manage_edit-section_sortable_columns', 'rffw_manage_sortable_columns' );
function rffw_manage_sortable_columns( $columns ) {
  $columns[ 'type'] = 'section_type';
	return $columns;
}

// Sort collum
add_action( 'pre_get_posts', 'manage_wp_posts_be_qe_pre_get_posts', 1 );
function manage_wp_posts_be_qe_pre_get_posts( $query ) {
	if ( ! is_admin() )	return;

   if ( $query->is_main_query() && ( $orderby = $query->get( 'orderby' ) ) ) {
      switch( $orderby ) {
         case 'section_type':
            $query->set( 'meta_key', '_rffw_sectiontype' );
            $query->set( 'orderby', 'meta_value' );
            break;
      }
   }
}

// Add Filter collum to section
function rffw_restrict_section_by_section_type() {
    if ( isset( $_GET['post_type'] ) && !empty( $_GET['post_type'] ) && $_GET['post_type']=='section' ) {
    global $rffw_section_types;
    ?>
    <select name="section_type" id="type">
        <option value=""><?php esc_html_e('Show all types', 'edgerblocks');?></option>
        <?php foreach ($rffw_section_types as $section_type) { ?>
        	<option value="<?php esc_attr_e( $section_type['slug'], 'edgerblocks' ); ?>" <?php if(isset($_GET['section_type']) && !empty($_GET['section_type']) ) selected($_GET['section_type'],  $section_type['slug']); ?>>
        		<?php esc_html_e($section_type['name'], 'edgerblocks'); ?>
        </option>
        <?php } ?>
    </select>
    <?php
    }
}
add_action('restrict_manage_posts','rffw_restrict_section_by_section_type');

// Make filter collum work
function posts_where_section_type( $where ) {
    if ( ! is_admin() )	return $where;

		if ( isset( $_GET['section_type'] ) && !empty( $_GET['section_type'] )) {
	    global $wpdb, $rffw_section_types;

			$rffw_section_type_key = array_search($_GET['section_type'], array_column($rffw_section_types, 'slug'));
			if(isset($rffw_section_type_key)){
		    $where .= " AND ID IN (SELECT post_id FROM " . $wpdb->postmeta ." WHERE meta_key='_rffw_sectiontype' AND meta_value='".$rffw_section_types[$rffw_section_type_key]['slug']."' )";
			}
    }
    return $where;
}
add_filter( 'posts_where' , 'posts_where_section_type' );


// Add description to post type use $post_type->description
add_filter("views_edit-section", 'post_type_desc');
// add_filter("views_edit-block", 'post_type_desc');
// add_filter("views_edit-client", 'post_type_desc');
add_filter("views_edit-omroep", 'post_type_desc');
add_filter("views_edit-event", 'post_type_desc');
add_filter("views_edit-widget_area", 'post_type_desc');
function post_type_desc($views){
		printf('<p>%s</p>', esc_html('Voor Stichting RPO', 'edgerblocks'));
		$screen = get_current_screen();
		$post_type = get_post_type_object($screen->post_type);

		if ($post_type->description) {
			printf('<h3>%s</h3>', esc_html($post_type->description, 'edgerblocks')); // echo
		}

		return $views; // return original input unchanged
}



function filter_programma_data_by_taxonomies( $post_type, $which ) {

	// Apply this only on a specific post type
	if ( 'programma-data' !== $post_type )
		return;

	// A list of taxonomy slugs to filter by
	$taxonomies = array( 'event-dag', 'tijd' );

	foreach ( $taxonomies as $taxonomy_slug ) {

		// Retrieve taxonomy data
		$taxonomy_obj = get_taxonomy( $taxonomy_slug );
		$taxonomy_name = $taxonomy_obj->labels->name;

		// Retrieve taxonomy terms
		$terms = get_terms( $taxonomy_slug );

		// Display filter HTML
		echo "<select name='{$taxonomy_slug}' id='{$taxonomy_slug}' class='postform'>";
		echo '<option value="">' . sprintf( esc_html__( 'Show All %s', 'text_domain' ), $taxonomy_name ) . '</option>';
		foreach ( $terms as $term ) {
			printf(
				'<option value="%1$s" %2$s>%3$s (%4$s)</option>',
				$term->slug,
				( ( isset( $_GET[$taxonomy_slug] ) && ( $_GET[$taxonomy_slug] == $term->slug ) ) ? ' selected="selected"' : '' ),
				$term->name,
				$term->count
			);
		}
		echo '</select>';
	}

}
add_action( 'restrict_manage_posts', 'filter_programma_data_by_taxonomies' , 10, 2);
