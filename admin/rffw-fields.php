<?php

$custom_menus =  get_terms( 'nav_menu', array( 'hide_empty' => true ) );
$custom_menu = array(
	'labels' => array('None'),
	'values' => array(''),
);
foreach($custom_menus as $m){
	$custom_menu['labels'][] = $m->name;
	$custom_menu['values'][] = $m->slug;
}


$rffw_customFields =	array(

	array(
		"name"					=> "omroep-url",
		"title"					=> __("Omroep link Url", 'edgerblocks'),
		//"description"		=> __("The video url used for the section background", 'edgerblocks'),
		"type"					=>	"url",
		"placeholder"		=> 'https://',
		"scope"					=>	array( "omroep" ),
		"capability"		=> "edit_post",
		"pagetemplate"	=> "general",
		"responsive" 		=> "1-2",
		),

		array(
			"name"					=> "programma-data-tijd",
			"title"					=> __("Tijd", 'edgerblocks'),
			//"description"		=> __("The video url used for the section background", 'edgerblocks'),
			"type"					=>	"text",
			"placeholder"		=> '',
			"scope"					=>	array( "programma-data" ),
			"capability"		=> "edit_post",
			"pagetemplate"	=> "general",
			"responsive" 		=> "1-2",
			),
	// array(
	// 	"name"					=> "section-options",
	// 	"title"					=> __("Section Settings",
	// 	"subtitle"			=> "Here you can find the settings for sections.",
	// 	"type"					=>	"options-section",
	// 	"scope"					=>	array("page", "post", "project" , "solution" , "section"),
	// 	"capability"		=> "edit_post",
	// 	"fields"				=>  array(
  //
  //
	// 	),
	// ),

	// array(
	// 	"name"					=> "sections",
	// 	"title"					=> __("Sections to display", 'edgerblocks'),
	// 	"description"		=> __("TIP: You can reorder them by dragging and dropping.", 'edgerblocks'),
	// 	"type"					=> "sections",
	// 	"scope"					=> array("page","project","solution","product"),
	// 	"capability"		=> "edit_page",
	// 	"pagetemplate"	=> "default",
	// 	"responsive" 		=> "1-1",
	// ),

	// array(
	// 	"name"					=> "sectiontype",
	// 	"title"					=> __("Section type", 'edgerblocks'),
	// 	"description"		=> __("The type of content for this section", 'edgerblocks'),
	// 	"options"				=> $rffw_section_types,
	// 	"type"					=> "section-selector",
	// 	"scope"					=> array( "section" ),
	// 	"capability"		=> "edit_post",
	// 	"pagetemplate"	=> "general",
	// 	"responsive" 		=> "1-2",
	// 	"conditional"   => true,
	// ),
	//

	array(
		"name"					=> "taxonomy-event-dag",
		"title"					=> __("Pick Event Dag to display in this section", 'edgerblocks'),
		//"description"		=> __("TIP: You can reorder them by dragging and dropping.", 'edgerblocks'),
		"type"					=> "taxonomy-selector",
		"taxonomy"			=> 'event-dag',
		"scope"					=>	array("section"),
		"capability"		=> "edit_post",
		"pagetemplate"	=> "general",
		"show-if"  			=> array(
				"sectiontype" 	=> array( "event-dag" ),
		),
	),

	array(
		"name"					=> "post-category",
		"title"					=> __("Pick Category to display in this section", 'edgerblocks'),
		//"description"		=> __("TIP: You can reorder them by dragging and dropping.", 'edgerblocks'),
		"type"					=> "taxonomy-selector",
		"taxonomy"			=> 'category',
		"scope"					=>	array("section"),
		"capability"		=> "edit_post",
		"pagetemplate"	=> "general",
		"show-if"  			=> array(
				"sectiontype" 	=> array( "grid-fixed", "slider-index", "slider-image"),
		),
	),


	array(
		"name"					=> "twitter_username",
		"title"					=> __("Twitter Username", 'edgerblocks'),
		// "description"		=> __("Set Section ID will override div number", 'edgerblocks'),
		"type"					=>	"text",
		"scope"					=>	array( "section" ),
		"capability"		=> "edit_post",
		"pagetemplate"	=> "general",
		"responsive" 		=> "1-4",
		"show-if"  			=> array(
				"sectiontype" 	=> array( "tweets"),
		),
	),
/*
	array(
		"name"					=> "widgets-area",
		"title"					=> __("Widgets Area", 'edgerblocks'),
		"description"		=> __('Or Make a ', 'edgerblocks').'<a target="_blank" href="'.admin_url().'edit.php?post_type=widget_area">'.__('new Widget Area', 'edgerblocks').'</a>',
		"type"					=>	"sidebar",
		"scope"					=>	array("section"),
		"capability"		=> "edit_post",
		"pagetemplate"	=> "general",
		"responsive" 		=> "1-2",
		"show-if"  			=> array(
				"sectiontype" 	=> array( "widgets-area" ),
		),
	),
*/
	/******************************************************************/
	/*********************** OPTIONS SECTION **************************/
	/******************************************************************/
	array(
		"name"					=> "page-header",
		"title"					=> __("Header Settings", 'edgerblocks'),
		"subtitle"			=> __("Here you can find the settings for your header.", 'edgerblocks'),
		"type"					=>	"options-section",
		"scope"					=>	array( "page" ),
		"capability"		=> "edit_post",
	  "scope"					=>	array("page", "post", "project" , "solution", 'product'),
		"fields"				=>  array(

			array(
				"name"					=> "subtitle",
				"title"					=> __("Subtitle", 'edgerblocks'),
				"description"		=> __("Set subtile if there is one", 'edgerblocks'),
				"type"					=>	"text",
			  "scope"			=>	array("page", "post", "project" , "solution", 'product'),
				"capability"		=> "edit_post",
				"pagetemplate"	=> "general"
			),
			array(
			  "name"					=> "header-bg-color",
			  "title"					=> __("Background color header", 'edgerblocks'),
			  "description"		=> __("The background color", 'edgerblocks'),
			  "type"					=>	"color",
			  "scope"					=>	array("page", "post", "project" , "solution", 'product'),
			  "capability"		=> "edit_post",
			  "pagetemplate"	=> "general",
				"responsive" 		=> "1-2",
		  ),

			array(
			  "name"					=> "text-style-header",
			  "title"					=> __("Text style header", 'edgerblocks'),
			  "description"		=> __("The text style for this element", 'edgerblocks'),
		    "options"       => array (
					"Default Text Style",
	        "Dark text",
	        "Light text"
	      ),
		    "options_value" => array(
					"",
	        "dark-text",
	        "light-text",
	      ),
			  "type"					=>	"dropdown",
			  "scope"					=>	array("page", "post", "project" , "solution", 'product'),
			  "capability"		=> "edit_post",
			  "pagetemplate"	=> "general",
				"responsive" 		=> "1-2",
			),
			array(
				"name"					=> "header-background-image",
				"title"					=> __("Header Background Image", 'edgerblocks'),
				"description"		=> __("Override background image", 'edgerblocks'),
				"type"					=> "upload",
				"scope"					=>	array("page", "post", "project" , "solution", 'product'),
				"capability"		=> "edit_post",
				"pagetemplate"	=> "general",
				"responsive" 		=> "1-2",
			),
			// array(
			// 	"name"					=> "featured-image-in-header",
			// 	"title"					=> __("Hide featured Image", 'edgerblocks'),
			// 	"type"					=> "checkbox",
			// 	"scope"					=>	array("product"),
			// 	"capability"		=> "edit_post",
			// 	"pagetemplate"	=> "general",
			// 	"default"       => true,
			// 	"responsive" 		=> "1-2",
			// ),

			array(
				"name"					=> "menubar-transparent",
				"title"					=> __("Transparent menubar", 'edgerblocks'),
				"type"					=> "checkbox",
				"scope"					=>	array("page", "post", "project" , "solution", 'product'),
				"capability"		=> "edit_post",
				"pagetemplate"	=> "general",
				"default"       => false,
				"responsive" 		=> "1-2",
			),
/*
		  array(
		    "name"					=> "header-cta",
		    "title"					=> __("Header CTA box", 'edgerblocks'),
		    "description"		=> __("This block will appear in the right side of the header. Please notice that it will overflow the header if it's too big. Rearrange your content (with columns) to make it fit.", 'edgerblocks'),
		    "type"					=> "editor",
			  "scope"					=>	array("page", "project" , "solution", 'product'),
		    "capability"		=> "edit_post",
		    "pagetemplate"	=> "general",
		    "responsive" 		=> "1-1",
		  ),
*/
		),
	),



	/******************************************************************/
	/*********************** OPTIONS SECTION **************************/
	/******************************************************************/
	array(
		"name"					=> "page-content",
		"title"					=> __("Content Settings", 'edgerblocks'),
		"subtitle"			=> __("Here you can find the settings for your content.", 'edgerblocks'),
		"type"					=>	"options-section",
		"scope"					=>	array( "page", "post", "project" , "solution" , "section", 'product'),
		"capability"		=> "edit_post",
		"pagetemplate"	=> "general",
		"fields" 				=> array(

			array(
				"name"			=> "index-grid",
				"title"			=> __("Grid type to show the index posts", 'edgerblocks'),
				"options"   => array (
					"List",
					"Fixed grid",
					"Image grid",
				),
				"options_value" => array(
					"list",
					"grid-fixed",
					"grid-image",
				),
				"type"			=>	"dropdown",
				"scope"			=>	array("index"),
				"capability"		=> "edit_post",
				"pagetemplate"	=> "general",
				"responsive" 		=> "1-1",
				),

				array(
					"name"			=> "section-plain-style",
					"title"			=> __("Section style", 'edgerblocks'),
					"options"   => array (
						"Standard",
						"Image left",
						"Image right",
						"Big Header",
						"Boxed",
						"Winnaar",
					),
					"options_value" => array(
						"standard",
						"image-left",
						"image-right",
						"big-header",
						"boxed",
						"winnaar"
					),
					"type"			=>	"dropdown",
					"scope"			=>	array("section"),
					"capability"		=> "edit_post",
					"pagetemplate"	=> "general",
					"responsive" 		=> "1-4",
					"show-if"  			=> array(
						"sectiontype" 	=> array(
							"plain-content",
						),
					),
					"conditional" => true
				),

				array(
					"name"					=> "accent-color",
					"title"					=> __("Accent color", 'edgerblocks'),
					"description"		=> __("Accent color for event template", 'edgerblocks'),
					"type"					=>	"color",
					"scope"					=>	array("page"),
					"capability"		=> "edit_post",
					"pagetemplate"	=> "general",
					"responsive" 		=> "1-4",

					"conditional" => true

				),

			array(
				"name"					=> "bg-color",
				"title"					=> __("Background color", 'edgerblocks'),
				"description"		=> __("The content background color", 'edgerblocks'),
				"type"					=>	"color",
				"scope"					=>	array("page", "post", "project" , "solution" , "section", 'product'),
				"capability"		=> "edit_post",
				"pagetemplate"	=> "general",
				"responsive" 		=> "1-4",
			),

			array(
				"name"					=> "info-background-image",
				"title"					=> __("Background Image", 'edgerblocks'),
				"description"		=> __("The Featured Image will be used", 'edgerblocks'),
				"type"					=>	"info",
				"scope"					=>	array("section"),
				"capability"		=> "edit_post",
				"pagetemplate"	=> "general",
				"responsive" 		=> "1-4",
			),
			array(
				"name"					=> "text-style",
				"title"					=> __("Text Style", 'edgerblocks'),
				"description"		=> __("The text style for the content", 'edgerblocks'),
				"options"       => array (
															"Default Text Style",
															"Dark text",
															"Light text"
														),
				"options_value" => array(
															"",
															"dark-text",
															"light-text",
													),
				"type"					=>	"dropdown",
				"scope"					=>	array("page", "post", "section", "project" , "solution", 'product'),
				"capability"		=> "edit_post",
				"pagetemplate"	=> "general",
				"responsive" 		=> "1-4",
			),
			array(
				"name"					=> "text-alignment",
				"title"					=> __("Text Alignment", 'edgerblocks'),
				"description"		=> __("The text alignment for the section", 'edgerblocks'),
				"options"       => array (
															"Left",
															"Center",
															"Right"
														),
				"options_value" => array(
															"",
															"center",
															"right",
													),
				"type"					=>	"dropdown",
				"scope"					=>	array("section"),
				"capability"		=> "edit_post",
				"pagetemplate"	=> "general",
				"responsive" 		=> "1-4",
			),

			array(
				"name"					=> "second-column",
				"title"					=> __("Second Column", 'edgerblocks'),
				"description"		=> __("Second column content", 'edgerblocks'),
				"type"					=>	"editor",
				"scope"					=>	array("section"),
				"capability"		=> "edit_post",
				"pagetemplate"	=> "general",
				"responsive" 		=> "1-1",
				"show-if"  			=> array(
					"sectiontype" 	=> array(
						"plain-content-2-cols",
					),
				),
			),


/*
			array(
				"name"					=> "sidebar",
				"title"					=> __("Sidebar settings", 'edgerblocks'),
				"description"		=> __("Override the sidebar theme setting for this page", 'edgerblocks'),
				"options"				=> array(
														"Default",
														'No sidebar',
														'Left sidebar',
														'Right sidebar',
													),
				"options_value"	=> array(
														"",
														'no-sidebar',
														"left-sidebar",
														'right-sidebar',
													),
				"type"					=> "dropdown",
				"scope"					=> array( "page", "post", "solution", 'product'),
				"capability"		=> "edit_post",
				"pagetemplate"	=> "general",
		    "responsive" 		=> "1-4",
			),
*/
			array(
				"name"					=> "media-position",
				"title"					=> __("Media Position", 'edgerblocks'),
				"options"       => array (
														"Left (default)",
														"Right",
													),
				"options_value" => array(
															"",
															"right",
														),
				"type"					=>	"dropdown",
				"scope"					=>	array("section"),
				"capability"		=> "edit_post",
				"pagetemplate"	=> "general",
				"responsive" 		=> "1-4",
				"show-if"  			=> array(
					"sectiontype" 	=> array( "plus-content" ),
				),
			),



			array(
				"name"					=> "show-title",
				"title"					=> __("Show title", 'edgerblocks'),
				"type"					=> "checkbox",
				"scope"					=>	array("page", "post", "project" , "solution", 'product'),
				"capability"		=> "edit_post",
				"pagetemplate"	=> "general",
				"default"       => false,
				"responsive" 		=> "1-4",
			),

			array(
				"name"					=> "custom-menu",
				"title"					=> __("Show menu", 'edgerblocks'),
				"type"					=> "dropdown",
				"options"       => $custom_menu['labels'],
				"options_value" => $custom_menu['values'],
				"scope"					=>	array("page"),
				"capability"		=> "edit_post",
				"pagetemplate"	=> "general",
				"default"       => false,
				"responsive" 		=> "1-4",
			),
		),
	),



	/******************************************************************/
	/*********************** OPTIONS SECTION **************************/
	/******************************************************************/
	array(
		"name"					=> "slider-grid-section-options",
		"title"					=> __("Slider/Grid/List Options", 'edgerblocks'),
		"subtitle"			=> __("Here you can find the settings for your grid/slider section.", 'edgerblocks'),
	  "type"					=>	"options-section",
	  "scope"					=>	array( "section", "index" ),
	  "capability"		=> "edit_post",
	  "pagetemplate"	=> "general",
		"show-if"  			=> array(
			"sectiontype" 	=> array(
				"slider-full-width",
				"slider-home",
				"slider-index",
				"slider-image",
				"grid-fixed",
				"grid-image",
				"grid-masonry",
				"grid-masonry-fixed",
				"list",
			),
		),
		"fields" 		=> array(
			array(
				"name"					=> "query_post_type",
				"title"					=> __("Post Type", 'edgerblocks'),
				"type"					=> "post_type",
				"description" 	=> "! Not used if this section has child sections. ",
				"scope"					=> array( "section" ),
				"show-if"  			=> array(
					"sectiontype" 	=> array(
						"slider-full-width",
						"slider-home",
						"slider-index",
						"slider-image",
						"grid-fixed",
						"grid-image",
						"grid-masonry",
						"grid-masonry-fixed",
						"list",
					),
				),
				"capability"		=> "edit_post",
				"pagetemplate"	=> "general"
			),

		  array(
			  "name"					=> "items_post_type",
			  "title"					=> __("Items to display", 'edgerblocks'),
			  "description"		=> __("The items to display. If none are selected, all will be used. TIP: You can reorder them by dragging and dropping.", 'edgerblocks'),
			  "type"					=> "selector",
			  "wp_query_args"	=>  array(), // get posts
				"scope"					=> array( "section" ),
				"show-if"  			=> array(
					"sectiontype" 	=> array(
						"slider-full-width",
						"slider-home",
						"slider-index",
						"slider-image",
						"grid-fixed",
						"grid-image",
						"grid-masonry",
						"grid-masonry-fixed",
						"list",
					),
	      ),
			  "capability"		=> "edit_post",
			  "pagetemplate"	=> "section",
				"responsive" 		=> "1-1",
		  ),

			array(
			  "name"					=> "max-amount-post",
			  "title"					=> __("Max amount of items", 'edgerblocks'),
			  "type"					=> "number",
			  "placeholder"		=> "9",
			  'min'						=> "1",
			  "scope"					=> array( "section" ),
			  "capability"		=> "edit_post",
			  "pagetemplate"	=> "general",
			  "responsive" 		=> "1-4",
			  "show-if"  			=> array(
					"sectiontype" 	=> array(
						"slider-full-width",
						"slider-home",
						"slider-index",
						"slider-image",
						"grid-fixed",
						"grid-image",
						"grid-masonry",
						"grid-masonry-fixed",
						"list",
					),
	      ),
			),

			array(
				"name"			=> "grid-columns",
				"title"			=> __("Items per row", 'edgerblocks'),
				"options"       => array (
					"4 (default)",
					"2",
					"3",
					"6",
				),
				"options_value" => array(
					"4",
					"2",
					"3",
					"6",
				),
				"type"			=>	"dropdown",
				"scope"			=>	array("section", "index" ),
				"capability"		=> "edit_post",
				"pagetemplate"	=> "general",
				"responsive" 		=> "1-4",
				"show-if"  			=> array(
					"sectiontype" 	=> array(
						"grid-fixed",
						"grid-image",
					),
				),
			),

			array(
				"name"			=> "grid-hover",
				"title"			=> __("Hover style", 'edgerblocks'),
				"options"       => array (
					"Zoom (default)",
					"Slide from bottom"
				),
				"options_value" => array(
					"zoom",
					"slide-bottom"
				),
				"type"			=>	"dropdown",
				"scope"			=>	array("section", "index" ),
				"capability"		=> "edit_post",
				"pagetemplate"	=> "general",
				"responsive" 		=> "1-4",
				"show-if"  			=> array(
					"sectiontype" 	=> array(
						"grid-image",
					),
				),
			),

		  array(
			  "name"					=> "pagination",
			  "title"					=> __("Show Pagination", 'edgerblocks'),
			  "type"					=> "checkbox",
			  "scope"					=> array(  "section",  "index"  ),
			  "capability"		=> "edit_post",
			  "pagetemplate"	=> "general",
				"show-if"  			=> array(
					"sectiontype" 	=> array(
						"grid-fixed",
						"grid-image",
						"grid-masonry",
						"grid-masonry-fixed",
						"list",
					),
				),
				"responsive" 		=> "1-4",
		  ),

			array(
				"name"					=> "showdate",
				"title"					=> __("Show Date", 'edgerblocks'),
				"type"					=> "checkbox",
				"scope"					=> array(  "section",  "index"  ),
				"capability"		=> "edit_post",
				"pagetemplate"	=> "general",
				"show-if"  			=> array(
					"sectiontype" 	=> array(
						"grid-fixed",
						"grid-masonry",
						"grid-masonry-fixed",
						"list",
					),
				),
				"responsive" 		=> "1-4",
			),

			array(
				"name"					=> "showreadmore",
				"title"					=> __("Show Read More", 'edgerblocks'),
				"type"					=> "checkbox",
				"scope"					=> array(  "section",  "index"  ),
				"capability"		=> "edit_post",
				"pagetemplate"	=> "general",
				"show-if"  			=> array(
					"sectiontype" 	=> array(
						"grid-fixed",
						"grid-masonry",
						"grid-masonry-fixed",
						"list",
					),
				),
				"responsive" 		=> "1-4",
			),


			array(
				"name"					=> "arrows",
				"title"					=> __("Show Arrows", 'edgerblocks'),
				"type"					=> "checkbox",
				"scope"					=> array( "section" ),
				"capability"		=> "edit_post",
				"pagetemplate"	=> "general",
				"show-if"  			=> array(
					"sectiontype" 	=> array(
						"slider-full-width",
						"slider-home",
						"slider-index",
						"slider-image",
					),
				),
				"responsive" 		=> "1-4",
			),

			array(
				"name"					=> "slides-per-view",
				"title"					=> __("Slides per view", 'edgerblocks'),
				"type"					=> "number",
				"placeholder"		=> "3",
				'min'						=> "1",
				'max'						=> "12",
				"scope"					=> array( "section" ),
				"capability"		=> "edit_post",
				"pagetemplate"	=> "general",
				"responsive" 		=> "1-4",
				"show-if"  			=> array(
					"sectiontype" 	=> array(
						"slider-index",
						"slider-image",
					),
				),
			),
/*
			array(
				"name"					=> "slides-height",
				"title"					=> __("Slider Height", 'edgerblocks'),
				"type"					=> "number",
				"placeholder"		=> "400",
				'min'						=> "1",
				'max'						=> "1200",
				"scope"					=> array( "section" ),
				"capability"		=> "edit_post",
				"pagetemplate"	=> "general",
				"responsive" 		=> "1-4",
				"show-if"  			=> array(
					"sectiontype" 	=> array(
						"slider-full-width",
						"slider-index",
					),
				),
			),
			*/
		),
	),



	/******************************************************************/
	/*********************** OPTIONS SECTION **************************/
	/******************************************************************/
	/*
	array(
		"name"					=> "card-section-options",
		"title"					=> __("Item Options", 'edgerblocks'),
		"type"					=>	"options-section",
		"subtitle"			=> __("Here you can find the settings for your items use in grid and sliders.", 'edgerblocks'),
		"scope"					=>	array( "section" , "index" ),
		"capability"		=> "edit_post",
		"pagetemplate"	=> "general",
		"show-if"  			=> array(
			"sectiontype" 	=> array(
				"slider-full-width",
				"slider-home",
				"slider-index",
				"slider-image",
				"grid-fixed",
				"grid-image",
				"grid-masonry",
				"grid-masonry-fixed",
				"list",
			),
		),
		"fields" 		=>   array(

			array(
				"name"					=> "show-content",
				"title"					=> __("Show item title", 'edgerblocks'),
				"type"					=> "checkbox",
				"scope"					=> array( "section" , "index" ),
				"capability"		=> "edit_post",
				"pagetemplate"	=> "general",
				"default"       => true,
				"show-if"  			=> array(
					"sectiontype" 	=> array(
						"slider-full-width",
						"slider-home",
						"slider-index",
						"grid-fixed",
						"grid-image",
						"grid-masonry",
						"grid-masonry-fixed",
						"list",
					),
				),
				"responsive" 		=> "1-4",
			),
			*/
/*
			array(
				"name"					=> "show-meta",
				"title"					=> __("Show meta data", 'edgerblocks'),
				"type"					=> "checkbox",
				"scope"					=> array( "section" , "index" ),
				"capability"		=> "edit_post",
				"pagetemplate"	=> "general",
				"default"       => true,
				"show-if"  			=> array(
					"sectiontype" 	=> array(
						"slider-full-width",
						"slider-index",
						"slider-home",
						"grid-fixed",
						"grid-image",
						"grid-masonry",
						"grid-masonry-fixed",
						"list",
					),
				),
				"responsive" 		=> "1-4",
			),
*/
/*
			array(
				"name"					=> "show-image",
				"title"					=> __("Show Image", 'edgerblocks'),
				"type"					=> "checkbox",
				"scope"					=> array( "section" , "index" ),
				"capability"		=> "edit_post",
				"pagetemplate"	=> "general",
				"default"       => true,
				"show-if"  			=> array(
					"sectiontype" 	=> array(
						"grid-fixed",
						"grid-masonry",
						"grid-masonry-fixed",
						"slider-home",
						"list",
					),
				),
				"responsive" 		=> "1-4",
			),


			array(
				"name"					=> "click-through-link",
				"title"					=> __("Click through link", 'edgerblocks'),
				"type"					=> "checkbox",
				"scope"					=> array( "section" , "index" ),
				"capability"		=> "edit_post",
				"pagetemplate"	=> "general",
		    "default"       => "yes",
				"show-if"  			=> array(
					"sectiontype" 	=> array(
						"slider-full-width",
						"slider-home",
						"slider-index",
						"grid-fixed",
						"grid-image",
						"list",
					),
				),
				"responsive" 		=> "1-4",
			),
			*/
/*
			array(
				"name"					=> "click-through-text",
				"title"					=> __("Click through text", 'edgerblocks'),
				"type"					=> "text",
				"scope"					=> array( "section" , "index" ),
				"capability"		=> "edit_post",
				"pagetemplate"	=> "general",
		    "placeholder"   => "Read more",
				"show-if"  			=> array(
					"sectiontype" 	=> array(
						"grid-masonry-fixed",
						"grid-masonry",
						"grid-image",
						"list",
					),
				),
				"responsive" 		=> "1-4",
			),
*/
/*
			array(
				"name"					=> "colorize-on-hover",
				"title"					=> __("Greyscale images", 'edgerblocks'),
				"type"					=> "checkbox",
				"scope"					=> array( "section" , "index" ),
				"capability"		=> "edit_post",
				"pagetemplate"	=> "general",
		    "default"       => "no",
				"show-if"  			=> array(
					"sectiontype" 	=> array(
						"grid-fixed",
					),
				),
				"responsive" 		=> "1-4",
			),

			array(
				"name"			=> "grid-image-width",
				"title"			=> __("Grid item image width", 'edgerblocks'),
				"options"       => array (
					"XL",
					"L",
					"M",
					"S",
					"XS",
				),
				"options_value" => array(
					"70%",
					"60%",
					"50%",
					"40%",
					"30%",
				),
				"type"			=>	"dropdown",
				"scope"			=>	array("section", "index" ),
				"capability"		=> "edit_post",
				"pagetemplate"	=> "general",
				"responsive" 		=> "1-4",
				"show-if"  			=> array(
					"sectiontype" 	=> array(
						"grid-fixed",
					),
				),
			),


		),

	),
*/


	/******************************************************************/
	/*********************** OPTIONS SECTION **************************/
	/******************************************************************/
	/*array(
		"name"					=> "meta-data-options",
		"title"					=> __("Meta data options", 'edgerblocks'),
		"type"					=>	"options-section",
		"subtitle"			=> "Here you can find the meta settings for your post.",
		"scope"					=>	array("project","solution" ),
		"capability"		=> "edit_post",
		"pagetemplate"	=> "general",
		"fields" 		=>   array(

			array(
			  "name"					=> "selectteam",
			  "title"					=> __("Pick Team profiles to display from the list", 'edgerblocks'),
			  "description"		=> __("TIP: You can reorder them by dragging and dropping.", 'edgerblocks'),
			  "type"					=> "selector",
			  "wp_query_args"	=>  array( "post_type" => 'team' ),
			  "scope"					=>	array( "project" ),
			  "capability"		=> "edit_post",
			  "pagetemplate"	=> "list-pages"
			),

			array(
			  "name"					=> "selectcase",
			  "title"					=> __("Pick Projects to display from the list", 'edgerblocks'),
			  "description"		=> __("TIP: You can reorder them by dragging and dropping.", 'edgerblocks'),
			  "type"					=> "selector",
			  "wp_query_args"	=>  array( "post_type" => 'project' ),
			  "scope"					=>	array("solution"),
			  "capability"		=> "edit_post",
			  "pagetemplate"	=> "list-pages",
			),

			array(
			  "name"					=> "client",
			  "title"					=> __("Client", 'edgerblocks'),
			  "description"		=> __("Set client from list", 'edgerblocks'),
			  "type"					=> "selector-one",
			  "wp_query_args"	=>  array( "post_type" => 'client' ),
			  "scope"					=>	array( "project" ),
			  "capability"		=> "edit_post",
			  "pagetemplate"	=> "general",
			  "responsive" 		=> "1-2",
			),

			array(
				"name"					=> "website",
				"title"					=> __("Project website", 'edgerblocks'),
				"description"		=> __("The website associated with this project", 'edgerblocks'),
				"type"					=>	"text",
				"scope"					=>	array( "project" ),
				"capability"		=> "edit_post",
				"pagetemplate"	=> "general",
				"responsive" 		=> "1-2",
			),
		),
	),
*/


	/******************************************************************/
	/*********************** OPTIONS SECTION **************************/
	/******************************************************************/
	array(
		"name"					=> "advanced-settings",
		"title"					=> __("Advanced Settings", 'edgerblocks'),
		"subtitle"			=> __("Here you can find the more specific and advanced settings.", 'edgerblocks'),
		"type"					=> "options-section",
		"scope"					=>	array("page", "post", "project", "solution", "section", "product"),
		"capability"		=> "edit_post",
		"pagetemplate"	=> "general",
		"fields"				=> array(

			array(
				"name"					=> "top-border",
				"title"					=> __("Top Border Style", 'edgerblocks'),
				"description"		=> __("Add a fancy top border", 'edgerblocks'),
				"options"       => array (
					"Horizontal (Default)",
					"Diagonal Up",
					"Diagonal Down",
					"Point up",
					"Point Down",
					"Diagonal Tab",
					"Waves",
				),
				"options_value" => array(
					"",
					"diagonal",
					"diagonal-down",
					"point-up",
					"point-down",
					"diagonal-tab",
	        "waves",
				),
				"type"			=>	"dropdown",
				"scope"			=>	array("section"),
				"capability"		=> "edit_post",
				"pagetemplate"	=> "general",
				"responsive" 		=> "1-4",
			),

			array(
				"name"					=> "top-border-animation",
				"title"					=> __("Top Border Animation", 'edgerblocks'),
				"description"		=> __("Add a fancy top border animation", 'edgerblocks'),
				"options"       => array (
														"None (Default)",
														"On Scroll in view",
														"Loop",
													),
				"options_value" => array(
														"",
														"scroll",
														"loop",
												 	),
				"type"					=>	"dropdown",
				"scope"					=>	array("section"),
				"capability"		=> "edit_post",
				"pagetemplate"	=> "general",
				"responsive" 		=> "1-4",
			),

			array(
				"name"					=> "bottom-border",
				"title"					=> __("Bottom Border Style", 'edgerblocks'),
				"description"		=> __("Add a fancy top border", 'edgerblocks'),
				"options"       => array (
														"Horizontal (Default)",
														"Diagonal Up",
														"Diagonal Down",
														"Point up",
														"Point Down",
														"Diagonal Tab",
														"Waves",
													),
				"options_value" => array(
														"",
														"diagonal",
														"diagonal-down",
														"point-up",
														"point-down",
														"diagonal-tab",
														"waves",
													),
				"type"					=>	"dropdown",
				"scope"					=>	array("section"),
				"capability"		=> "edit_post",
				"pagetemplate"	=> "general",
				"responsive" 		=> "1-4",
			),

			array(
				"name"					=> "bottom-border-animation",
				"title"					=> __("Bottom Border Animation", 'edgerblocks'),
				"description"		=> __("Add a fancy bottom border animation", 'edgerblocks'),
				"options"       => array (
														"None (Default)",
														"On Scroll in view",
														"Loop",
													),
				"options_value" => array(
														"",
														"scroll",
														"loop",
													),
				"type"					=> "dropdown",
				"scope"					=>	array("section"),
				"capability"		=> "edit_post",
				"pagetemplate"	=> "general",
				"responsive" 		=> "1-4",
			),

			array(
				"name"					=> "padding-top",
				"title"					=> __("Padding top", 'edgerblocks'),
				"description"		=> __("Will overwrite the default top padding in pixels", 'edgerblocks'),
				"type"					=> "number",
				'placeholder'		=> "88",
				"scope"			=>	array("section"),
				"capability"		=> "edit_post",
				"pagetemplate"	=> "general",
				"responsive" 		=> "1-4",
			),

			array(
				"name"					=> "padding-bottom",
				"title"					=> __("Padding bottom", 'edgerblocks'),
				"description"		=> __("Will overwrite the default bottom padding in pixels", 'edgerblocks'),
				"type"					=> "number",
				'placeholder'		=> "66",
				"scope"			=>	array("section"),
				"capability"		=> "edit_post",
				"pagetemplate"	=> "general",
				"responsive" 		=> "1-4",
			),

			array(
				"name"					=> "section-id",
				"title"					=> __("Section ID", 'edgerblocks'),
				"description"		=> __("Set Section ID will override div number", 'edgerblocks'),
				"type"					=>	"text",
				"scope"					=>	array( "section" ),
				"capability"		=> "edit_post",
				"pagetemplate"	=> "general",
				"responsive" 		=> "1-4",
			),

			array(
				"name"					=> "section-class",
				"title"					=> __("Section Class", 'edgerblocks'),
				"description"		=> __("Add classes to section if set.", 'edgerblocks'),
				"type"					=>	"text",
				"scope"					=>	array( "section" ),
				"capability"		=> "edit_post",
				"pagetemplate"	=> "general",
				"responsive" 		=> "1-4",
			),
/*
	    array(
		  "name"					=> "overview-img",
		  "title"					=> __("Overview image", 'edgerblocks'),
		  "description"		=> __("The image used for overviews, like grids for example.", 'edgerblocks'),
		  "type"					=> "upload",
		  "scope"					=>	array("page", "post", "project" , "solution" , "section", "product"),
		  "capability"		=> "edit_post",
		  "pagetemplate"	=> "general",
			"responsive" 		=> "1-4",
	  	),
*/
/*
	  	array(
	      "name"					=> "section-parallax",
	      "title"					=> __("Parallax background", 'edgerblocks'),
	      "type"					=> "checkbox",
	  	  "scope"					=>	array("section"),
	      "capability"		=> "edit_post",
	      "pagetemplate"	=> "general",
	      "default"       => false,
	      "responsive" 		=> "1-4",
	      "show-if"  			=> array(
					"sectiontype" 	=> array( "plain-content", "plain-content-2-cols" ),
			  ),
	    ),
*/
			array(
	      "name"					=> "full-width",
	      "title"					=> __("Full Width", 'edgerblocks'),
	      "type"					=> "checkbox",
	  	  "scope"					=>	array("section"),
	      "capability"		=> "edit_post",
	      "pagetemplate"	=> "general",
	      "default"       => false,
	      "responsive" 		=> "1-4",
	      "show-if"  			=> array(
					"sectiontype" 	=> array( "plain-content", "plus-content",  "plain-content-2-cols" ),
			  ),
	    ),

	    array(
				"name"					=> "show-title",
				"title"					=> __("Show title", 'edgerblocks'),
				"type"					=> "checkbox",
				"scope"					=>	array("section"),
				"capability"		=> "edit_post",
				"pagetemplate"	=> "general",
				"default"       => true,
				"responsive" 		=> "1-4",
				"show-if"  			=> array(
					"sectiontype" 	=> array( "plain-content",  "plain-content-2-cols", "slider-index", "slider-image"),
				),
			),

			/*
			array(
				"name"					=> "boxed",
				"title"					=> __("Make Boxed", 'edgerblocks'),
				"type"					=> "checkbox",
				"scope"					=>	array("section", "index"),
				"capability"		=> "edit_post",
				"pagetemplate"	=> "general",
				"default"       => false,
				"responsive" 		=> "1-4",
				"show-if"  			=> array(
					"sectiontype" 	=> array(
						"slider-full-width",
						"slider-index",
						"grid-fixed",
						"grid-image",
						"list",
					 ),
				),
			),
			*/


			array(
			  "name"					=> "custom-url",
			  "title"					=> __("Custom URL", 'edgerblocks'),
			  "description"		=> __("The url where this section will link to", 'edgerblocks'),
			  "type"					=> "url",
				"placeholder"		=> 'http://',
			  "scope"					=> array( "section", "project", 'solution' ),
			  "capability"		=> "edit_post",
			  "pagetemplate"	=> "general",
				// "show-if"  			=> array(
				// 	"sectiontype" 	=> array(
				// 		"plain-content",
				// 	 ),
				//),
				"responsive" 		=> "1-2",
			),
/*
			array(
			  "name"					=> "section-video",
			  "title"					=> __("Background video", 'edgerblocks'),
			  "description"		=> __("The video url used for the section background", 'edgerblocks'),
			  "type"					=>	"url",
				"placeholder"		=> 'http://',
			  "scope"					=>	array( "section" ),
			  "capability"		=> "edit_post",
			  "pagetemplate"	=> "general",
			  "responsive" 		=> "1-1",
				"show-if"  			=> array(
						"sectiontype" 	=> array( "plain-content" ),
				  ),
		  	),

				*/

		),
	),




	/*
	The Team profile options
	*/
	/*
	array(
	  "name"					=> "position",
	  "title"					=> __("Position", 'edgerblocks'),
	  "description"		=> __("The Position of this person", 'edgerblocks'),
	  "type"					=> "text",
	  "scope"					=> array( "team" ),
	  "capability"		=> "edit_post",
	  "pagetemplate"	=> "general"
	),
	array(
	  "name"					=> "twitter",
	  "title"					=> __("Twitter", 'edgerblocks'),
	  "description"		=> __("The Twitter url of this person", 'edgerblocks'),
	  "type"					=> "url",
		"placeholder"		=> 'http://',
	  "scope"					=> array( "team" ),
	  "capability"		=> "edit_post",
	  "pagetemplate"	=> "general"
	),
	array(
	  "name"					=> "facebook",
	  "title"					=> __("Facebook", 'edgerblocks'),
	  "description"		=> __("The Facebook url of this person", 'edgerblocks'),
	  "type"					=> "url",
		"placeholder"		=> 'http://',
	  "scope"					=> array( "team" ),
	  "capability"		=> "edit_post",
	  "pagetemplate"	=> "general"
	),
	array(
	  "name"					=> "linkedin",
	  "title"					=> __("Linkedin", 'edgerblocks'),
	  "description"		=> __("The Linkedin url of this person", 'edgerblocks'),
	  "type"					=> "url",
		"placeholder"		=> 'http://',
	  "scope"					=> array( "team" ),
	  "capability"		=> "edit_post",
	  "pagetemplate"	=> "general"
	),
	array(
	  "name"					=> "email",
	  "title"					=> __("E-mail", 'edgerblocks'),
	  "description"		=> __("The E-mail address of this person", 'edgerblocks'),
	  "type"					=> "text",
	  "scope"					=> array( "team" ),
	  "capability"		=> "edit_post",
	  "pagetemplate"	=> "general"
	),
	array(
	  "name"					=> "phonenummer",
	  "title"					=> __("Phone number", 'edgerblocks'),
	  "description"		=> __("The Phone number of this person", 'edgerblocks'),
	  "type"					=> "text",
	  "scope"					=> array( "team" ),
	  "capability"		=> "edit_post",
	  "pagetemplate"	=> "general"
	),

	*/
	/*
	The block options
	*/
	/*
array(
	"name"					=> "custom-url",
	"title"					=> __("Custom URL", 'edgerblocks'),
	"description"		=> __("The url where this element will link to", 'edgerblocks'),
	"type"					=> "url",
	"placeholder"		=> 'http://',
	"scope"					=> array( "block", "client", "team" ),
	"capability"		=> "edit_post",
	"pagetemplate"	=> "general",
	"responsive" 		=> "1-1",
),
*/
);
