<?php

include_once get_template_directory() .  '/admin/rffw_scss/scssphp/scss.inc.php';
include_once get_template_directory() . '/admin/rffw_scss/class-rffw-scss.php';

$rfscss_settings = array(
  'scss_dir'  =>  get_template_directory() . '/sass/',
  'css_dir'   =>  get_template_directory() . '/css/',
  'compiling' =>  'Leafo\ScssPhp\Formatter\Expanded',
  /*
  'Leafo\ScssPhp\Formatter\Expanded'   => 'Expanded',
  'Leafo\ScssPhp\Formatter\Nested'     => 'Nested',
  'Leafo\ScssPhp\Formatter\Compressed' => 'Compressed',
  'Leafo\ScssPhp\Formatter\Compact'    => 'Compact',
  'Leafo\ScssPhp\Formatter\Crunched'   => 'Crunched',
  'Leafo\ScssPhp\Formatter\Debug'      => 'Debug'
  */
  'errors'    =>  'show-logged-in', //'show' 'hide'
);


$rfscss_compiler = new rffw_scss(
  $rfscss_settings['scss_dir'],
  $rfscss_settings['css_dir'],
  $rfscss_settings['compiling']
);


//rffw_scss_needs_compiling() needs to be run as wp_head-action to make it possible
//for themes to set variables and decide if the style needs compiling
function rffw_scss_needs_compiling() {
  global $rfscss_compiler;

  $needs_compiling = apply_filters('rffw_scss_needs_compiling', $rfscss_compiler->needs_compiling());
  if ( $needs_compiling ) {
    rffw_scss_compile();
    rfscss_handle_errors();
  }
}

add_action('wp_head', 'rffw_scss_needs_compiling');

function rffw_scss_compile() {
  global $rfscss_compiler;
  $variables = apply_filters('rffw_scss_variables', array());
  foreach ($variables as $variable_key => $variable_value) {
    if (strlen(trim($variable_value)) == 0) {
      unset($variables[$variable_key]);
    }
  }
  $rfscss_compiler->set_variables($variables);
  $rfscss_compiler->compile();
}




function rfscss_settings_show_errors($errors) {
  echo
  '<style>
    .scss_errors {
      position: absolute;
      top: 0px;
      z-index: 99999;
      width: 100%;
      padding: 100px;
    }
    .scss_errors pre {
      background: #f5f5f5;
      border-left: 5px solid #DD3D36;
      box-shadow: 0 2px 3px rgba(51,51,51, .4);
      color: #666;
      font-family: monospace;
      font-size: 14px;
      margin: 20px 0;
      overflow: auto;
      padding: 20px;
      white-space: pre;
      white-space: pre-wrap;
      word-wrap: break-word;
    }
  </style>';

  echo '<div class="scss_errors"><pre>';
  echo '<h5 style="margin: 15px 0;"><strong>Only Admins see this error. Try Saving the theme settings to fix this error.</strong></h5>';

  foreach( $errors as $error) {
    echo '<p class="sass_error">';
    echo '<p>Sass Compiling Error</p>';
    echo '<strong>'. $error['file'] .'</strong> <br/><em>"'. $error['message'] .'"</em>';
    echo '<p>';
  }

  echo '</pre></div>';


}

function rfscss_handle_errors() {
    global $rfscss_settings, $rfscss_compiler;
    // Show to logged in users: All the methods for checking user login are set up later in the WP flow, so this only checks that there is a cookie
    if ( !is_admin() && $rfscss_settings['errors'] === 'show-logged-in' && !empty($_COOKIE[LOGGED_IN_COOKIE]) && count($rfscss_compiler->compile_errors) > 0) {
        rfscss_settings_show_errors($rfscss_compiler->compile_errors);
// Show in the header to anyone
    } else if ( !is_admin() && $rfscss_settings['errors'] === 'show' && count($rfscss_compiler->compile_errors) > 0) {
        rfscss_settings_show_errors($rfscss_compiler->compile_errors);
    } else {
      // Hide noting
    }
}
