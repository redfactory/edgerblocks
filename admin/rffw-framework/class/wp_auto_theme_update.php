<?php

if ( ! class_exists( 'WTH_Auto_Theme_Update' ) ) {
	class WTHP_Auto_Theme_Update{
		/**
		 * Plugin's current version
		 * @var string
		 */
		public $wsq_current_version;

		/**
		 * Plugin's remote path
		 * @var string
		 */
		public $wsq_remote_path = 'https://support.redfactory.nl';

		/**
		 * Plugin's Slug
		 * @var string
		 */
		public $wsq_slug;

		/**
		 * Initialize a new instance of the Auto-Update class.
		 */
		function __construct() {

			// Set Version to current theme Version
			$this->wsq_current_version = RFFW_VERSION;

			// Check for plugin updates.
			add_filter( 'pre_set_site_transient_update_themes', array( $this, 'check_updates' ),10,1 );

		}

		/**
		 * Add our plugin to the filter transient.
		 * @param  object $transient Transient Object.
		 * @return object            Transient Object.
		 */
		public function check_updates($transient) {

			if ( empty( $transient->checked ) ) {
				return $transient;
			}
			$current_theme = wp_get_theme();

			$folder_name = str_replace( get_theme_root(), '', get_stylesheet_directory() );
			$folder_name = str_replace( '/', '', $folder_name );
			$this->wsq_slug = trim( $folder_name );
			$this->wsq_current_version = $current_theme->get( 'Version' );

			// Check and Get remote version.
			$response = wp_remote_post( $this->wsq_remote_path.'/wunpupdates', array( 'body' => array( 'action' => 'version', 'plugin' => $this->wsq_slug ) ) );

			if ( ! is_wp_error( $response ) || wp_remote_retrieve_response_code( $response ) === 200 ) {

				$new_updates = unserialize( $response['body'] );
				$new_updates = $this->append_auth_parameter($new_updates);

				// If update is available, set the transient.
				if ( version_compare( $this->wsq_current_version, $new_updates->new_version, '<' ) ) {
					$transient->response[ $this->wsq_slug ] = (array) $new_updates;
				}
			}
			return $transient;
		}

		function append_auth_parameter($info) {

			$username 		= rffw_get_theme('envato-username');
			$purchase_key = rffw_get_theme('envato-purchase-key');

			if( !empty($username) and !empty($purchase_key) ) {
				$auth_url = add_query_arg( array(
					'euname' => $username,
					'epkey' => $purchase_key,
				), $info->package );
			} else {
				$auth_url = $info->package;
			}

			$auth_url = apply_filters('wupp_auth_url',$auth_url, $this->wsq_slug);
			$info->download_link = $auth_url;
			$info->package = $auth_url;

			return $info;
		}
	}
	new WTHP_Auto_Theme_Update();
}
