// Build event lisner
var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
var eventer = window[eventMethod];
var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";

// Listen to message from child window
eventer(messageEvent,function(e) {
    var key = e.message ? "message" : "data";
    var data = e[key];

	if(e.data.action=='closeUpdateIfame'){
		closeUpdateIfame(e.data);
	}
},false);

function closeUpdateIfame(post){
	// Remove thickbox iframe popup
	tb_remove();
	window.location.reload();
}
