jQuery(document).ready(function($){
"use strict";

	if(jQuery('.option-conditional :input', '#my-custom-fields').length){
		jQuery('.option-conditional :input', '#my-custom-fields').each(function(){
		    conditionalOption(jQuery(this).attr('id'), jQuery(this).val());
				jQuery(this).change(function() {
		    	conditionalOption(jQuery(this).attr('id'), jQuery(this).val());
				});
		});
	}
	if(jQuery('.option-conditional', '#my-custom-fields').length){
		jQuery('.option-conditional', '#my-custom-fields').each(function(){
				conditionalOption(jQuery(this).attr('id'), jQuery(this).val());
				jQuery(this).change(function() {
					conditionalOption(jQuery(this).attr('id'), jQuery(this).val());
				});
		});
	}

	if(jQuery('#page_template').length){
		var pageTemplate = jQuery('#page_template').val();
		pageTemplateOption(pageTemplate);

		jQuery('#page_template').change(function() {
			pageTemplate = jQuery(this).val();
			pageTemplateOption(pageTemplate);
		});
	}

	function pageTemplateOption(pageTemplate){
		// Make simple
		if (pageTemplate == 'template-content-page.php'){
			jQuery('#my-custom-fields').slideUp();
		}
		else{
			jQuery('#my-custom-fields').slideDown();
		}
	}


	function conditionalOption(optionName, optionVar){

		// Make simple
		optionVar = optionVar.replace(/\s+/g, '-').toString().toLowerCase();

		//console.log(optionName, optionVar);

		jQuery('.option_hidden', '#my-custom-fields').each(function(){

				var showIf = jQuery(this).data('show-if');
				var show = jQuery(this).data('show');
				var scope = jQuery(this).data('scope');


				// if scope is index always show
				if(jQuery.inArray('index', scope) !== -1){
						show[optionName] = true;
						jQuery(this).data('show',show);
						jQuery(this).slideDown('fast');
				}

				if (showIf.hasOwnProperty(optionName))  {

					//console.log('has name check if has: '+optionVar+ " in:");
					// console.log(showIf[optionName]);

					if(jQuery.inArray(optionVar, showIf[optionName]) !== -1){
							show[optionName] = true;
					}
					else {
							show[optionName] = false;
					}

					// save
					jQuery(this).data('show',show);

					var nameOfOption = jQuery(this).attr('id');
					 //console.log('show: '+ nameOfOption);
					 //console.log(jQuery(this).data('show'));

					if(allTrue(show)){
						jQuery(this).slideDown('fast');
					}
					else{
						jQuery(this).slideUp('fast');
					}
				}
		});

		// Hide all options settings and show only the one with the section_type set
		//jQuery('.option_hidden', '#my-custom-fields').not('.'+optionVar).slideUp('fast');
		//jQuery('.option_hidden.'+optionVar, '#my-custom-fields').slideDown('fast');



		//
		if(optionName == '_rffw_sectiontype'){
			var showContent = true;

			// set for array
			$.each(rf.rffw_section_types, function(index, sectionType) {
				//

				if(optionVar == sectionType['slug'] && 'show-content' in sectionType){
					showContent = sectionType['show-content'];
				}
			});
			// List of sections types where page
			if (showContent){
				jQuery('.composer-switch').attr('style','display: inline-block !important');
				jQuery('#postdivrich').slideDown({
						complete: function(){
								// Fix postdivrich menu layout
								jQuery(window).resize();
						}
				});
			}
			else{
				jQuery('#postdivrich').slideUp();
				jQuery('.composer-switch').attr('style','display: none !important');
			}
		}
	}

});

function allTrue(obj){
	for(var o in obj)
			if(!obj[o]) return false;
	return true;
}
