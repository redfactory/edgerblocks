jQuery(document).ready(function($){
	"use strict";

// Live search sections
	var $spinnerSearchSearch =  jQuery('#spinnerSectionSearch');
	jQuery("input#section-search").on('keyup', function (e) {
			setTimeout(function() {
					$spinnerSearchSearch.addClass('is-active');
					jQuery('#section-add-list').empty();
					jQuery.ajax({
						type: "POST",
						url: ajaxurl,
						data: {
							'action'	:'rffw_section_search',
							's'				: jQuery('input#section-search').val(),
						},
						error: function(jqXHR, textStatus, errorThrown){
							console.error("The following error occured: " + textStatus, errorThrown);
							$spinnerSearchSearch.removeClass('is-active');
						},
						success: function(data) {
							jQuery('#section-add-list').empty().append(data).scrollTop(0);
							$spinnerSearchSearch.removeClass('is-active');
						}
					});
			}, 500);
			e.preventDefault();
	});

// // Prevent enter to
// 	$('#my-custom-fields').on('keyup keypress', function(e) {
// 	  var keyCode = e.keyCode || e.which;
// 	  if (keyCode === 13) {
// 	    e.preventDefault();
// 	    return false;
// 	  }
// 	});

	$("div.sortable-list").sortable({
	 	cursor: 'move',
	 	items: '.sortable',
   		update: function( event, ui ) {}
	});

	var oldContainer;
  var nestedSections = jQuery("ol.sort-section-list").sortableNested({
    group: 'nestedSections',
    afterMove: function (placeholder, container) {
      /* if(oldContainer != container){
        if(oldContainer)
          oldContainer.el.removeClass("active");
          container.el.addClass("active");

        oldContainer = container;
      }
      */
    },
    onDrop: function ($item, container, _super) {

      if(jQuery($item).parent().attr('id') == 'sectionsPage'){
        if (!jQuery($item).find('ol.sub-sections').length) {
            jQuery($item).append('<ol class="sub-sections"></ol>');
        }
      }
      else{
        jQuery($item).find('ol.sub-sections').remove();
      }

      buildSectionArray();

      _super($item, container);
    },

    // check if section van be child section & drop section can have child section
    isValidTarget: function ($item, container) {

      var dropSectionType = $.grep(rf.rffw_section_types, function(e){ return e.slug == container.el.parent('li').data('type'); });
      if (dropSectionType.length >= 1) {
        var dragSectionType = $.grep(rf.rffw_section_types, function(e){ return e.slug == $item.data('type'); });
        if (dragSectionType.length >= 1 ) {
          if(dropSectionType[0]['has-child']=== true && dragSectionType[0]['be-child'] === true ){
            return true;
          }
          else{
            return false;
          }
        }
        else{
          return false;
        }
      }
      else{
          return true;
      }
    }
  });


$("ol.sort-section-list").on("click", "a.remove-section", function(event) {
    event.preventDefault();

    var removeListItem = jQuery(this).parent().detach();
    $('ol#sectionsSort').append(removeListItem);

    buildSectionArray();
  });




  jQuery("input[type=checkbox]", "#sectionsPage").change(function() {
      if(this.checked) {
        jQuery(this).parent().data('visible', true);
        jQuery(this).parent().removeClass('off');

        if(this.id == 'section-content-visible') {
          jQuery('#postdivrich').slideDown();
          jQuery(window).trigger('resize');
        }
      }
      else{
        jQuery(this).parent().data('visible', false);
        jQuery(this).parent().addClass('off');

        if(this.id == 'section-content-visible') {
          jQuery('#postdivrich').slideUp();
          jQuery(window).trigger('resize');
        }
      }
      buildSectionArray();
  });

  //
  if(jQuery("input[type=checkbox]#section-content-visible", "#sectionsPage").length && !jQuery("input[type=checkbox]#section-content-visible", "#sectionsPage").prop('checked')){
    jQuery('#postdivrich').slideUp();
  }

  window.buildSectionArray = function(){
			//console.log("buildsection");
      var data = nestedSections.sortableNested("serialize").get();
      var jsonString = JSON.stringify(data[0], null, ' ');
      jQuery('input#_rffw_sections').val(jsonString);
  }

	$("#section-add-to-page").on("click", function(event) {
    //var selectedSection = [];
    $('input:checked', '#section-add-list').each(function() {

			// Add section to page
      jQuery("#sectionsPage")
			.append('<li data-visible="1" data-type="'+$(this).data('type')+'" data-id="'+$(this).data('id')+'" id="sortable-'+$(this).data('id')+'" class="sortable wp-menu-image dashicons-after '+$(this).data('icon')+'"><span class="section_name">'+$(this).data('name')+'</span><a class="thickbox" href="post.php?post='+$(this).data('id')+'&action=edit&iframe=1&#038;width=1000&#038;height=800&#038;TB_iframe=true&#038;hidemenu=true">'+$(this).data('title')+'</a><a class="remove-section" href="#">x</a><ol class="sub-sections"></ol></li>');

			// Animate just changed effect
			jQuery('#sortable-'+$(this).data('id')).css({backgroundColor: '#FFF896'}).animate({backgroundColor: '#F1F1F1'},2500);

			// remove check
      $(this).prop( "checked", false );
        //selectedSection.push(section);
    });

    //console.log(selectedSection);
    $("#show-existing").text($("#show-existing").data('titleopen'));
    $('.section-panels-box').animate({'margin-left': '0px'},300, function() {
      // Animation complete.
      $('.all-section-panel').hide();
    });

		jQuery(".sortable-list").sortableNested("refresh");
		buildSectionArray();
		event.preventDefault();
	});


	$("#show-existing").on("click", function(event) {
    //
    if (!$('.all-section-panel').is(':visible')) {
      $('.all-section-panel').show();
			$(this).text($(this).data('titleclose'));
      $('.section-panels-box').animate({'margin-left': '300px'},300, function() {
        // Animation complete.
      });

    }
    else{
      $(this).text($(this).data('titleopen'));
      $('.section-panels-box').animate({'margin-left': '0px'},300, function() {
        // Animation complete.
        $('.all-section-panel').hide();
      });
    }
		event.preventDefault();
	});
  $("#add-new-section").on("click", function(event) {
		event.preventDefault();
	});
});
