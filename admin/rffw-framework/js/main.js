jQuery(function(){
	"use strict";

	// build colorpickers
	jQuery('.cp_colorpicker').wpColorPicker();

	var $spinners =  jQuery('.select-cat-spinner');

	ajaxGetItems(jQuery('select#_rffw_query_post_type'));
	jQuery('select#_rffw_query_post_type').change(function() {
		ajaxGetItems(jQuery(this));
	});

	if(getParameterByName('iframe')){
		// Add iframe var to trash link
		jQuery('a.submitdelete').attr('href', function() {
				return this.href + '&iframe=1';
		});

	}


	jQuery( '.editinline' ).on( 'click', function() {
			var tag_id = jQuery( this ).parents( 'tr' ).attr( 'id' ),
		order  = jQuery( 'td.order', '#' + tag_id ).text();

			jQuery( ':input[name="order"]', '.inline-edit-row' ).val( order );
	} );


if(jQuery(".js-accordion").length){

	jQuery( ".js-accordion" ).accordion({
	  header: ".js-accordion-header",
		//active: false,
 		collapsible: true,
		icons :false,

		// classes: {
		// 	"ui-accordion-header": "rffw-header",
		//   "ui-accordion-header-collapsed": "rffw-option-section-open",
		//   "ui-accordion-content": "rffw-options-section",
		// }

	});
}

	// var $accordionHeader = jQuery('.js-accordion').find('.js-accordion-header');
	// var $accordionContent = jQuery('.accordion-content').find('.js-accordion-header');
  // $accordionHeader.click(function(){
	//
	// 	if(jQuery(this).data('open') == true){
	// 		jQuery(this).data('open', false);
	// 		//Expand or collapse this panel
	// 		jQuery(this).next().slideUp();
	// 		jQuery(this).find('span.dashicons').removeClass('dashicons-arrow-up-alt2').addClass('dashicons-arrow-down-alt2');
	// 	}
	// 	else{
	// 		jQuery(this).data('open', true);
	// 		//Expand or collapse this panel
	// 		jQuery(this).next().slideDown();
	// 		jQuery(this).find('span.dashicons').removeClass('dashicons-arrow-down-alt2').addClass('dashicons-arrow-up-alt2');
	//
	// 	}
	// 	//Hide the other panels
	// 	jQuery(".accordion-content").not(jQuery(this).next()).slideUp();
	// 	$accordionHeader.not(jQuery(this)).find('span.dashicons').removeClass('dashicons-arrow-up-alt2').addClass('dashicons-arrow-down-alt2');
	//
  // });

function ajaxGetItems(postType){
	$spinners.addClass('is-active');

	var checkedValues = jQuery('#_rffw_items_post_type_list input:checkbox:checked').map(function () {
		return this.value;
	}).toArray();

	jQuery.ajax({
			type: "POST",
			url: ajaxurl,
			data: {
				'action'		:'rffw_get_post_by_type',
				'post_ID'		: jQuery('input#post_ID').val(),
				'post_type'	: postType.val(),
				'selected_ids'	: checkedValues,

			},
			error: function(jqXHR, textStatus, errorThrown){
				console.error("The following error occured: " + textStatus, errorThrown);
				jQuery('.spinner').removeClass('is-active');
			},
			success: function(data) {
				jQuery('#_rffw_items_post_type_list').empty().append(data);
				jQuery('.spinner').removeClass('is-active');
			}
	});

}

	(function(){ var tb_show_temp = window.tb_show; window.tb_show = function(){ tb_show_temp.apply(null, arguments);
		var iframe = jQuery('#TB_iframeContent');
		iframe.load(function(){
			var iframeDoc = iframe[0].contentWindow.document;
			var iframeJQuery = iframe[0].contentWindow.jQuery;

			// of jQuery in iframe is loaded
			if(iframe[0].contentWindow.jQuery){

				// Add iframe to 'move to trash link'
				iframeJQuery('.submitdelete').attr('href', function() {
					return this.href + '&iframe=1';
				});
			}
		});
	 }})();

});

// Build event lisner
var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
var eventer = window[eventMethod];
var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";

// Listen to message from child window
eventer(messageEvent,function(e) {
    var key = e.message ? "message" : "data";
    var data = e[key];

	if(e.data.action=='closeUpdateIfame'){
		closeUpdateIfame(e.data);
	}
},false);


function closeUpdateIfame(post){
	// Remove thickbox iframe popup
	tb_remove();

	// Check if sortable item exsists
	if(jQuery('#sortable-'+post.id).length){
		if(post.post_status=='trash'){
			jQuery('#sortable-'+post.id)
				.slideUp('slow')
				.remove();
			jQuery(".sortable-list").sortableNested("refresh");
		}
		else{
			jQuery('#sortable-'+post.id)
				.data('type', post._rffw_sectiontype)
				.removeClass()
				.addClass('sortable dashicons-after wp-menu-image icon-'+post._rffw_sectiontype);
			jQuery('#sortable-'+post.id+'> .section_name').text(post._rffw_sectiontype);
			jQuery('#sortable-'+post.id+'> a.thickbox').text(post.post_title);
		}
		buildSectionArray();
	}
	// Add sortable item on button and refresh
	else{


		jQuery("#sectionsPage").append('<li data-visible="1" data-type="'+post.type+'" data-id="'+post.id+'" id="sortable-'+post.id+'" class="sortable dashicons-after wp-menu-image '+post.icon+'"><span class="section_name">'+post.name+'</span><a class="thickbox" href="post.php?post='+post.id+'&action=edit&iframe=1&#038;width=1000&#038;height=800&#038;TB_iframe=true&#038;hidemenu=true">'+post.post_title+'</a><a class="remove-section" href="#">x</a><ol class="sub-sections"></ol></li>');
		jQuery(".sortable-list").sortableNested("refresh");
		buildSectionArray();
	}

	// Animate just changed effect
	jQuery('#sortable-'+post.id).css({backgroundColor: '#FFF896'}).animate({backgroundColor: '#F1F1F1'},2500);
}

function getParameterByName(name, url) {
    if (!url) {
      url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
