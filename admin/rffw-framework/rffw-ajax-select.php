<?php
add_action( 'wp_ajax_rffw_get_post_by_type', 'rffw_get_post_by_type_ajax_handler' );
function rffw_get_post_by_type_ajax_handler() {

	$rffw_db_prefix = '_rffw_';

  // maybe check some permissions here, depending on your app
  if ( ! current_user_can( 'edit_posts' ) )   exit;

	$post_type 	= (isset($_POST['post_type']))? $_POST['post_type'] : '';
	$pages_checked_array = (isset($_POST['selected_ids']))? $_POST['selected_ids'] : array();

	$customField['wp_query_args']['post_type'] 	 = $post_type;

	if( isset($_POST['post_type']) && $_POST['post_type'] == 'attachment'){
		$customField['wp_query_args']['post_status'] = 'inherit';
		$customField['wp_query_args']['post_mime_type'] = 'image';
	}

	if (!empty($pages_checked_array)) {
		// Get checked posts
		$pages_checked = new WP_Query(array_merge($customField['wp_query_args'], array(
							'post__in'  		=> $pages_checked_array,
							'orderby' 			=> 'post__in',
							'posts_per_page' 	=> -1,
							'suppress_filters'=> 0
							)));
	}

	// Get other posts
	$pages_notchecked = new WP_Query(array_merge($customField['wp_query_args'], array(
						'post__not_in'  	=> $pages_checked_array,
						'posts_per_page' 	=> -1,
						'suppress_filters'=> 0
						)));


	// Merge Query
	$all_pages = new WP_Query();
	if(!empty($pages_checked_array)) {
			$all_pages->posts = array_merge( $pages_checked->posts, $pages_notchecked->posts );
	}
	else{
		$all_pages->posts = $pages_notchecked->posts;
	}

	$all_pages->post_count = count( $all_pages->posts );

	// Loop posts
	while ( $all_pages->have_posts() ) : $all_pages->the_post();

		// Set checked or don't
		$pagechecked = (in_array(get_the_ID(), $pages_checked_array)) ? 'checked' : '';

		if( isset($_POST['post_type']) && $_POST['post_type'] == 'attachment'){
		?>

		<li tabindex="0" role="checkbox" data-id="<?php the_ID(); ?>" class="attachment">
				<div class="attachment-preview js--select-attachment type-image subtype-jpeg landscape">
					<div class="thumbnail">
							<div class="centered">
								<img src="<?php echo wp_get_attachment_url(get_the_ID(), 'thumbnail' ); ?>" draggable="true" alt="">
							</div>
					</div>
				</div>
				<input class="media-checkbox" type="checkbox" <?php echo $pagechecked; ?> name="<?php echo  $rffw_db_prefix ; ?>items_post_type[]" id="<?php echo  $rffw_db_prefix ; ?>items_post_type" value="<?php the_ID(); ?>" />
		</li>

		<?php
		}	else{
			echo '<div class="sortable sortable-single">';
			echo '<input type="checkbox" '.$pagechecked.' name="' . $rffw_db_prefix . 'items_post_type[]" id="' . $rffw_db_prefix .'items_post_type" value="' . get_the_ID() .'" />' . get_the_title();
			echo '</div>';
		}

	endwhile;

	wp_die();
}

add_action( 'wp_ajax_rffw_section_search', 'rffw_section_search_ajax_handler' );
function rffw_section_search_ajax_handler() {
	global $rffw_section_types;

	 // maybe check some permissions here, depending on your app
  if ( ! current_user_can( 'edit_posts' ) )   exit;

	$sections = new WP_Query(
					array( 	'posts_per_page' 	=> -1,
									'post_type' 			=> 'section',
									's' 							=>	$_POST['s'],
									'suppress_filters'=> 0, // shows only the selected translation with this line
									'post_status' 		=> array('publish', 'pending', 'draft', 'future', 'private', 'inherit')
					));




		if ( $sections->have_posts() ):
			?>
			<span class="text"><?php esc_html_e('Found ', 'edgerblocks'); echo $sections->found_posts; esc_html_e(' section(s).', 'edgerblocks'); ?></span>
			<?php

			 while ( $sections->have_posts() ) : $sections->the_post();
			 $post = get_post();
			 // add meta to post object
			 $section_post = rffw_add_meta_to_post($post);

			 // section type key
			 if(isset($section_post->rffw_meta['sectiontype'])){
			 		$rffw_section_type_key = array_search($section_post->rffw_meta['sectiontype'], array_column($rffw_section_types, 'slug'));
		 		}
				else{
					$rffw_section_type_key = 0;
				}

			 // Set title to no tile if empty
			 if($section_post->post_title=='') $section_post->post_title = __('No title', 'edgerblocks');
			 ?>
			 <li>
				 <label class="menu-item-title">
					 <input type="checkbox" class="" name="all-sections-checked[]" data-id="<?php echo $section_post->ID; ?>" data-title='<?php echo $section_post->post_title; ?>' data-type='<?php echo sanitize_title($section_post->_rffw_sectiontype); ?>' data-name='<?php echo $rffw_section_types[$rffw_section_type_key]['name']; ?>' data-icon='<?php echo $rffw_section_types[$rffw_section_type_key]['icon']; ?>' > <?php echo $section_post->post_title; ?> </label>
					 <a class="thickbox edit dashicons-before <?php echo $rffw_section_types[$rffw_section_type_key]['icon']; ?>" href="<?php echo get_admin_url(); ?>post.php?post=<?php echo $section_post->ID; ?>&action=edit&iframe=1&#038;width=1000&#038;height=800&#038;TB_iframe=true&#038;hidemenu=true" title="<?php esc_html_e('Edit this section','edgerblocks'); ?>"></a>
			 </li>
			 <?php

    endwhile;
	else:
		?>
		<span class="text"><?php esc_html_e('No sections found. ', 'edgerblocks'); ?></span>
		<?php
 	endif;

 	wp_reset_postdata();

	wp_die();
}
