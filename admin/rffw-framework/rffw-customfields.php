<?php
// Load the rf custom fields class
if ( file_exists( dirname( __FILE__ ).'/class-rffw-customfields.php' ) ) {
    require_once dirname(__FILE__).'/class-rffw-customfields.php';
}
$rffw_CustomFields_var = new rffw_CustomFields($rffw_customFields, $rffw_post_types);

if ( file_exists( dirname( __FILE__ ).'/rffw-core.php' ) ) {
    require_once dirname(__FILE__).'/rffw-core.php';
}
if ( file_exists( dirname( __FILE__ ).'/rffw-ajax-select.php' ) ) {
    require_once dirname(__FILE__).'/rffw-ajax-select.php';
}
