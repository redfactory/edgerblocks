<?php
function rffw_add_meta_to_post( $post ) {
  //global $rffw_db_prefix;
  //$_metas = array();
	//$_metas = get_post_meta($post_obj->ID);
  $_metas = rffw_get_post_custom_single($post->ID);
/*
  if(get_post_meta($post_obj->ID)){
    foreach($_metas as $key => $value) {
      // Search for prefix to remove it
      if(substr($key, 0, strlen($rffw_db_prefix)) == $rffw_db_prefix){
          $new_key = substr($key, strlen($rffw_db_prefix));
          unset($_metas[$key]);

          if(sizeof($value) >= 1) {
            $_metas[$new_key] = $value[0];
          }
      }
      else{
        if(sizeof($value) >= 1) {
          $_metas[$key] = $value[0];
        }
      }
	  }
*/
  if($_metas){
    $_metas = rffw_build_style($_metas);
    $post->rffw_meta = $_metas;
  }
  return $post;
}
add_filter('the_post', 'rffw_add_meta_to_post');

function rffw_get_meta($rffw_meta_name, $post_object) {
  global $rffw_customFields;

  // handle like a rffw custom field
  if(array_search($rffw_meta_name, array_column($rffw_customFields, 'name'))){

    //Search customfield for this field(name) PHP > 5.5
    $key = array_search($rffw_meta_name, array_column($rffw_customFields, 'name'));

    // Check if the page is saved
    if(isset($post_object->rffw_meta['saved'])){
      // page is saved so is not set means empty or false
      $rffw_meta_var = (isset($post_object->rffw_meta[$rffw_meta_name]))? $post_object->rffw_meta[$rffw_meta_name] : '';
    }
    // Page not saved use default var if set
    else{
      // Set default or empty
      $rffw_meta_var = (isset($rffw_customFields[$key]['default']))? $rffw_customFields[$key]['default'] : '';
    }
  }
  else{
    $rffw_meta_var = (isset($post_object->rffw_meta[$rffw_meta_name]))? $post_object->rffw_meta[$rffw_meta_name] : '';
  }

  return $rffw_meta_var;
}

function rffw_the_meta($rffw_meta_name, $post_object) {
  echo rffw_get_meta($rffw_meta_name, $post_object);
}

function rffw_is_meta($rffw_meta_name, $post_object) {
  if(empty(rffw_get_meta($rffw_meta_name, $post_object ))){
    return false;
  }
  elseif(rffw_get_meta($rffw_meta_name, $post_object ) == '0x0'){
    return false;
  }
  elseif(rffw_get_meta($rffw_meta_name, $post_object )){
    return true;
  }

  return false;
}



function rffw_get_theme($theme_var_key, $theme_var_key2 = false){
  global $edgerblocks;
  if($theme_var_key2){
    $theme_var = isset( $edgerblocks[$theme_var_key][$theme_var_key2] ) ? $edgerblocks[$theme_var_key][$theme_var_key2] : '';
  }
  else{
  	$theme_var = isset( $edgerblocks[$theme_var_key] ) ? $edgerblocks[$theme_var_key] : '';
  }
  return $theme_var;
}
function rffw_the_theme($theme_var_key, $theme_var_key2 = false){
  echo rffw_get_theme($theme_var_key, $theme_var_key2);
}

function rffw_is_theme($theme_var_key, $theme_var_key2 = false){
  if(empty(rffw_get_theme($theme_var_key, $theme_var_key2)))
    return false;
  elseif(rffw_get_theme($theme_var_key, $theme_var_key2))
    return true;

  return false;
}

function rffw_is_index() {
    return ( is_archive() || is_author() || is_category() || is_home() || is_tag() || is_search());
}

// Create 'top' section and move that to the top
add_action('edit_form_after_title', function() {
  global $post, $wp_meta_boxes;
  do_meta_boxes(get_current_screen(), 'top', $post);
  unset($wp_meta_boxes[get_post_type($post)]['top']);
});


add_action('edit_form_after_editor', function() {
  global $post, $wp_meta_boxes;
  do_meta_boxes(get_current_screen(), 'after_editor', $post);
  unset($wp_meta_boxes[get_post_type($post)]['after_editor']);
});

//
add_filter('default_page_template_title', function() {
    return __('Section page Template (default)', 'edgerblocks');
});


/*
 * Get post custom Single
 */
function rffw_get_post_custom_single($post_id) {
  $rffw_db_prefix = '_rffw_';
  $metas = get_post_meta($post_id);

  if($metas){

	  foreach($metas as $key => $value) {
      // Search for prefix to remove it
      if(substr($key, 0, strlen($rffw_db_prefix)) == $rffw_db_prefix){
          $new_key = substr($key, strlen($rffw_db_prefix));
          unset($metas[$key]);

          if(sizeof($value) >= 1) {
            $metas[$new_key] = $value[0];
          }
      }
      else{
        if(sizeof($value) >= 1) {
          $metas[$key] = $value[0];
        }
      }
	  }
    $metas['ID'] = $post_id;

	  return $metas;
  }
  return false;
}

/*
 * Search in a array of objects
 */
function objArraySearch($array, $index, $value){
    foreach($array as $arrayInf) {
        if($arrayInf->{$index} == $value) {
            return $arrayInf;
        }
    }
    return null;
}



// Add category classes to body on single pages also for Woocommerce
add_filter('body_class','add_category_to_single');
function add_category_to_single($classes, $class = false) {

  if (is_single() ) {
    global $post;

    foreach((get_the_category($post->ID)) as $category) {
      // add category slug to the $classes array
      $classes[] = $category->category_nicename;
    }

	// add product_cat term
	$custom_terms = get_the_terms(0, 'product_cat');
    if ($custom_terms) {
      foreach ($custom_terms as $custom_term) {
        if (is_object($custom_term)){
          // Check if the parent category exists:
          if( $custom_term->parent > 0 ) {
              // Get the parent product category:
              $parent = get_term( $custom_term->parent, 'product_cat' );
              // Append the parent class:
              if ( ! is_wp_error( $parent ) )
                  $classes[] = 'product_parent_cat_' . $parent->slug;
          }

          $classes[] = 'product_cat_' . $custom_term->slug;
        }
      }
    }
  }
  // return the $classes array
  return $classes;
}

// hex to rgb converter
function rffw_hex2rgb($hex) {
   if (isset($hex)) {
     $hex = str_replace("#", "", $hex);

     if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
     } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
     }
     $rgb = array($r, $g, $b);

     return implode(",", $rgb);
   }
}

// Add featured image and inline style to meta
function rffw_build_style($rffw_meta, $featured_image_in_style = true){

	// Empty var's to prevent Undefined index
	$rffw_meta['featured_image_url']    = '';
	$rffw_meta['inline_style']          = '';
  $rffw_meta['inline_style_no_image'] = '';

  // add Featured image url to meta
  if ( isset($rffw_meta['_thumbnail_id']) ) {
    $rffw_meta['featured_image_url'] = wp_get_attachment_image_url($rffw_meta['_thumbnail_id'], 'fullwidth');
  }
  elseif (wp_get_attachment_url()) {
    $rffw_meta['featured_image_url'] = wp_get_attachment_url();
  }

  // Add background image to inline style
  if(!isset($rffw_meta['sectiontype'])){
    if (!empty($rffw_meta['featured_image_url']) && $featured_image_in_style) {
        $rffw_meta['inline_style'] .= "background-image: url('" . $rffw_meta['featured_image_url'] . "'); ";
    }
  }

  // Add background color to inline style
  if (isset($rffw_meta['sectionbgcolor']) &&  !empty($rffw_meta['sectionbgcolor']) ) {
  	$rffw_meta['inline_style'] .= "background-color:" .$rffw_meta['sectionbgcolor'] . "; ";
    $rffw_meta['inline_style_no_image'] .= "background-color:" . $rffw_meta['sectionbgcolor'] . "; ";
  }
  elseif(isset($rffw_meta['bg-color']) &&  !empty($rffw_meta['bg-color']) ) {
    $rffw_meta['inline_style'] .= "background-color:" . $rffw_meta['bg-color'] . "; ";
    $rffw_meta['inline_style_no_image'] .= "background-color:" . $rffw_meta['bg-color'] . "; ";
  }

  // Add top-padding to inline style
  if (isset($rffw_meta['padding-top']) &&  !empty($rffw_meta['padding-top']) ) {
  	$rffw_meta['inline_style'] .= "padding-top:" . intval($rffw_meta['padding-top']) . "px; ";
    $rffw_meta['inline_style_no_image'] .= "padding-top:" . intval($rffw_meta['padding-top']) . "px; ";
  }
  // Add bottom-padding to inline style
  if (isset($rffw_meta['padding-bottom']) &&  !empty($rffw_meta['padding-bottom']) ) {
  	$rffw_meta['inline_style'] .= "padding-bottom:" . intval($rffw_meta['padding-bottom']) . "px; ";
    $rffw_meta['inline_style_no_image'] .= "padding-bottom:" . intval($rffw_meta['padding-bottom']) . "px; ";
  }

  // Add text-align to inline style
  if (isset($rffw_meta['text-alignment']) &&  !empty($rffw_meta['text-alignment']) ) {
  	$rffw_meta['inline_style'] .= "text-align:" . $rffw_meta['text-alignment'] . "; ";
    $rffw_meta['inline_style_no_image'] .= "text-align:" . $rffw_meta['text-alignment'] . "; ";
  }

  return $rffw_meta;
}

// rf add sections
function rffw_edit_section($ID){
  // Output the edit button for this section
  if (current_user_can('edit_posts')){
    echo '<a class="editPopup button thickbox" href="'.get_admin_url().'post.php?post='.$ID.'&action=edit&iframe=1&#038;width=800&#038;height=800&#038;TB_iframe=true&#038;" title="'. esc_attr__('Edit Section', 'edgerblocks') .'"></a>';
  }
}
function rffw_edit_post(){
  // Output the edit button for this section
  if (current_user_can('edit_posts')){
    global $post;
    $class_edit_link ='dashicons-admin-page';

    if(isset($post->post_type)){
      $postType = get_post_type_object($post->post_type);
      if(!empty($postType->menu_icon))
        $class_edit_link = $postType->menu_icon;
    }

    echo '<a class="button editpage '.$class_edit_link.'" href="'.get_admin_url().'post.php?post='.get_the_ID().'&action=edit" title="'. esc_attr__('Edit page', 'edgerblocks') .'"></a>';
  }
}

function rffw_error($sting){
  if(current_user_can('edit_posts'))
    echo '<section><div class="container rf-error">'.$sting.'</div></section>';
}

function rffw_query_builder($rffw_section, $rffw_section_meta){

  // Set vars
  $slides_ids_array = array();
  $slides_post_type = 'section';

  // Get subsections id to build slider
  if (isset($rffw_section->children[0]) && count($rffw_section->children[0]) >= 1) {
    foreach ($rffw_section->children[0] as $sub_section_object) {
      $slides_ids_array[] = $sub_section_object->id;
    }
  }
  // Get selected items
  if(isset($rffw_section_meta['items_post_type']) && !empty($rffw_section_meta['items_post_type'])){
    if(empty($slides_ids_array)){
      $slides_ids_array = explode(",", $rffw_section_meta['items_post_type']);

      // ignore sticky posts, not set to top etc
      $rffw_query['ignore_sticky_posts'] = true;

      if(isset( $rffw_section_meta['query_post_type']) && !empty( $rffw_section_meta['query_post_type'])){
         $slides_post_type = $rffw_section_meta['query_post_type'];
      }
    }
  }
  // translate id's
  $slides_ids_array = my_translate_object_id( $slides_ids_array , 'section' );


  // if posttype isset not empty and there are no subsections change posttype
  if( isset($rffw_section_meta['query_post_type']) &&
      !empty($rffw_section_meta['query_post_type'] &&
      empty($slides_ids_array))
  ) {
    $slides_post_type = $rffw_section_meta['query_post_type'];
  }
  if( isset($rffw_section_meta['query_post_type']) && $rffw_section_meta['query_post_type'] == 'attachment'){
    $rffw_query['post_status'] = 'inherit';
  }

  if(isset($rffw_section_meta['max-amount-post']) && !empty($rffw_section_meta['max-amount-post'])){
    $rffw_query['posts_per_page'] = intval($rffw_section_meta['max-amount-post']);
  }
  else{
    $rffw_query['posts_per_page'] = 9;
  }


    if(!empty($slides_ids_array)){
      $rffw_query['post_type'] = $slides_post_type;
      $rffw_query['post__in']	= $slides_ids_array;
      $rffw_query['orderby'] 	= 'post__in';
    }
    else{
      $rffw_query['post_type'] =  $slides_post_type;
    }

    if( isset($rffw_section_meta['post-category'])  && !empty($rffw_section_meta['post-category'])){
      $rffw_query['tax_query'] = array(
          array(
          'taxonomy' => 'category',
          'field' => 'slug',
          'terms' => $rffw_section_meta['post-category']
           )
        );
    }else{
      $rffw_query['tax_query'] = array(
          array(
          'taxonomy' => 'category',
          'field' => 'slug',
          'terms' => 'presentaties',
          'operator' => 'NOT IN'
           )
        );
    }

    return new WP_Query($rffw_query);
}

//Makes post_thumbnail work for attachment media images
function post_thumbnail_fix_media( $html, $post_id, $post_thumbnail_id, $size, $attr  ) {
  if($html==''){
    $post = get_post($post_id);

    if($post->post_type == 'attachment'){

      $post_mime_type = array(
        'image/svg+xml',
        'image/jpeg',
        'image/gif',
        'image/png',
        'image/bmp',
        'image/tiff',
        'image/x-icon'
      );

      if(in_array($post->post_mime_type, $post_mime_type )){

        // Default the thumbnail fuctions
        do_action( 'begin_fetch_post_thumbnail_html', $post->ID, $post_id, $size );
        if ( in_the_loop() )
          update_post_thumbnail_cache();
        $html = wp_get_attachment_image( $post_id, $size, false, $attr );
        do_action( 'end_fetch_post_thumbnail_html', $post->ID, $post_id, $size );
      }
    }
  }
  return $html;
}
add_filter('post_thumbnail_html', 'post_thumbnail_fix_media', 10, 5 );


// If iframe is set in post or get Set again in url
add_filter('redirect_post_location', 'redirect_to_post_on_publish_or_save');
function redirect_to_post_on_publish_or_save($location){
	if(isset($_REQUEST['iframe']) && $_REQUEST['iframe'] == 1) {
		$location = add_query_arg( array('iframe' => 1), $location);
	}
	return $location;
}



add_action('save_post',     'rffw_close_update_ifame', 9999, 1 );
add_action('trashed_post',  'rffw_close_update_ifame', 9999, 1);
function rffw_close_update_ifame($ID){

  // Get post Add meta
  $post = rffw_add_meta_to_post(get_post($ID));

  // Check if post is in iframe and saved
  if(isset($_REQUEST['iframe']) && $_REQUEST['iframe'] == 1 && isset($post->rffw_meta['saved'])) {

      global $rffw_section_types;

      // section type key
      $rffw_section_type_key = array_search($post->rffw_meta['sectiontype'], array_column($rffw_section_types, 'slug'));


  		// If post title is empty show no title
  		if($post->post_title == '')$post->post_title =  __('No title', 'edgerblocks');

  		// Call javascript to close iframe
  		echo '<script>parent.postMessage({
        action:"closeUpdateIfame",
        post_title:"'.$post->post_title.'",
        id:'.$post->ID.',
        icon:"'.$rffw_section_types[$rffw_section_type_key]['icon'].'",
        name:"'.$rffw_section_types[$rffw_section_type_key]['name'].'",
        type:"'.sanitize_title($post->rffw_meta['sectiontype']).'",
        post_status:"'.$post->post_status.'"
      },"*");</script>';
  		exit();
  	}
}

function rffw_load_section($ID){
  // Set default options
  $section_object = new stdClass();

  $section_object->id = $ID;
  if (function_exists('icl_object_id')){
  	$section_object->id = icl_object_id($section_object->id, 'section', true, ICL_LANGUAGE_CODE);
  }
  $section_object->visible = true;
  $section_object->children = array();

  // Load section by ID
  include(get_template_directory().'/section.php');
}

// Default navigation menu before menu setup
function rffw_emptymenu() {
  echo "";
}

function rffw_section_in(){
  global $post;
  $rffw_db_prefix = '_rffw_';
  $query = new WP_Query(array(
      'post_type' => 'any',
      'post_status' => get_post_stati(), // any
      'meta_key' => $rffw_db_prefix.'sections',
      //'orderby' => '',
      //'order' => 'DESC',
      'meta_query' => array(
              array(
                  'key' => $rffw_db_prefix.'sections',
                  'value' => '"id": '.$post->ID,
                  'compare' => 'LIKE'
              )
          ),
  ));

  echo '<ul>';
  if ($query->have_posts()){
    while ( $query->have_posts() ) : $query->the_post();
      // If post title is empty show no title
      if($post->post_title == '')$post->post_title =  __('No title', 'edgerblocks');

      echo '<li><a href="'.get_admin_url().'post.php?post='.$post->ID.'&action=edit">'.$post->post_title.'</a> - '.$post->post_type.'</li>';
    endwhile;
  }
  else{
    echo '<li>'.esc_html_e('This Section is not used', 'edgerblocks').'</li>';
  }
  echo '</ul>';

  wp_reset_postdata();
}

add_action( 'admin_menu','rffw_custom_menu_class', 99);
function rffw_custom_menu_class() {
    global $menu, $submenu;

    if(isset($submenu['themes.php'])){
      foreach( $submenu['themes.php']as  $key => $value ){
          if( $value[2]     == 'edit.php?post_type=widget_area'){
            $submenu['themes.php'][$key][4] = " rffw-admin-menu-item-sub dashicons-before dashicons-schedule";
          }
          elseif($value[2]  == 'tgmpa-install-plugins'){
            $submenu['themes.php'][$key][4] = " rffw-admin-menu-item-sub dashicons-before dashicons-admin-plugins";
          }
          elseif($value[2]  == 'pt-one-click-demo-import'){
            $submenu['themes.php'][$key][4] = " rffw-admin-menu-item-sub dashicons-before dashicons-download";
          }
      }
    }


    foreach( $menu as $key => $value ){
        if('edit.php?post_type=section' == $value[2] ||
           'edit.php?post_type=event' == $value[2] ||
           'edit.php?post_type=omroep'    == $value[2] ||
           'edit.php?post_type=solution' == $value[2] ||
           'edit.php?post_type=block' == $value[2] ||
           'edit.php?post_type=client' == $value[2] ||
           'Stichting RPO' == $value[2]
         ){
            $menu[$key][4] .= " rffw-admin-menu-item";
         }

    }
}

function rffw_the_link($permalink, $post){
  if(rffw_is_meta('custom-url', $post)) {
    echo rffw_get_meta('custom-url', $post);
  }
  else{
    echo $permalink;
  }
}

function rffw_is_link($rffw_section, $post){
  if(rffw_is_meta('click-through-link', $rffw_section)){
    if (in_array(get_post_type(), array(
      'block',
      'client',
      'team',
      'section',
      'attachment',
    ))) {
      if(rffw_is_meta('custom-url', $post)) {
            return true;
      }else{
            return false;
      }
    }
    else{
      return true;
    }
  }
  else{
    return false;
  }
}

function rffw_the_grid_thumbnail($post, $size, $args = array(), $rffw_section = ''){
  if(!isset($args['style'])) $args['style'] = '';

  if(is_object($rffw_section)){
    if(rffw_is_meta('grid-image-width', $rffw_section)){
      $args['style'] .= ' max-width:'.rffw_get_meta('grid-image-width', $rffw_section);
    }
  }
  if (rffw_is_meta('overview-img', $post)) {
    echo wp_get_attachment_image(rffw_get_meta('overview-img', $post), $size, false, $args);
  }
  elseif (rffw_get_meta('_thumbnail_id', $post)){
    echo get_the_post_thumbnail($post, $size, $args );
  }
  //TODO mabye get_post_thumbnail_id() try
}


function rffw_the_grid_thumbnail_url($post, $size){
  if (rffw_is_meta('overview-img', $post)) {
    echo wp_get_attachment_image_url(rffw_get_meta('overview-img', $post), $size);
  }
  else{
    echo get_the_post_thumbnail_url($post, $size);
  }
}

function rffw_the_header_image_url($header_post){

  $header_background_url = get_the_post_thumbnail_url($header_post, 'fullwidth');

  if (!rffw_is_meta('header-bg-color', $header_post) && rffw_is_theme('header-image')) {
    $header_background_url = wp_get_attachment_image_url(rffw_get_theme('header-image')['id'], 'fullwidth');
  }

  echo $header_background_url;
}
