<?php

if ( !class_exists('rffw_CustomFields') ) {
	class rffw_CustomFields {
		/**
		* @var  string  $prefix  The prefix for storing custom fields in the postmeta table
		*/
		var $prefix = '_rffw_';

		/**
		* @var  array  $customFields  Defines the custom fields available
		*/
		var $customFields = array();
		var $post_types = array();

		// relpace content with index
		var $index_page_ids = array();

		/**
		* PHP 5 Constructor
		*/
		function __construct($customFieldsArray , $rffw_post_typesArray) {
			// Set var array in object
			$this->customFields = $customFieldsArray;
			$this->post_types = $rffw_post_typesArray;

			add_action( 'admin_menu', array( &$this, 'createCustomFields' ) );
			add_action( 'post_updated', array( &$this, 'saveCustomFields' ), 1, 2 );

			// Comment this line out if you want to keep default custom fields meta box
			add_action( 'do_meta_boxes', array( &$this, 'removeDefaultCustomFields' ), 10, 3 );
		}

		/**
		* Remove the default Custom Fields meta box
		*/
		function removeDefaultCustomFields( $type, $context, $post ) {

			foreach ( array( 'normal', 'advanced', 'side' ) as $context ) {
				foreach ($this->post_types as $post_type => $post_type_info) {
					remove_meta_box( 'postcustom', $post_type, $context );
				}
			}
		}

		/**
		* Create the new Custom Fields meta box
		*/
		function createCustomFields() {

			if ( function_exists( 'add_meta_box' ) ){

					foreach ($this->post_types as $post_type => $post_type_info) {

							add_meta_box(
									'my-custom-fields', // Unique ID
									RFFW_NAME.' '.$post_type_info['name'].' Options',
									array( &$this, 'displayCustomFields' ),
									$post_type,   // Post type
									'normal',
									'high'
							);

						}
			}
		}

		/**
		* Display the new Custom Fields meta box
		*/
		function displayCustomFields() {
			global $post, $theme_options;

			$this->index_page_ids = get_page_for_index_ids();

			?>
			<div class="form-wrap">
					<input type="hidden" name="rffw_hidden_flag" value="true" />
					<div class="grid grid-pad js-accordion">

					<?php
					wp_nonce_field('my-custom-fields', 'my-custom-fields_wpnonce', false, true);

					// Set no menu in post if set
					if(isset($_REQUEST['iframe']) && $_REQUEST['iframe'] == 1) {
						echo '<input type="hidden" name="iframe" value="1" />';
					}

					foreach ( $this->customFields as $customField ){

						if($customField['type']=='options-section'){

							// relpace title of content for index pages
							if( in_array($post->ID, $this->index_page_ids) && $customField['name'] == 'page-content'){
									$customField['title'] 		=  __("Index Settings", 'edgerblocks');
									$customField['subtitle'] 	=  __("Here you can find the settings for your index.", 'edgerblocks');
							}

							// Check scope
							$scope = $customField['scope'];

							$is_index = false;
							$output = false;
							foreach ( $scope as $scopeItem ) {
									if ($scopeItem == $post->post_type || ($scopeItem == 'index' && in_array($post->ID, $this->index_page_ids)) ){
										$output = true;
										$customField['scope'] = array($scopeItem);
										if($scopeItem == 'index' && in_array($post->ID, $this->index_page_ids)) $is_index = true;
										break;
									}
							}

							// Check capability
							if ( !current_user_can( $customField['capability'], $post->ID ) )
								$output = false;

							// Output if allowed
							if ( $output ){
								$data_attributes = '';
								$option_class = '';


								if(isset($customField['conditional'])){
										$data_attributes .= 'data-conditional =\''.$customField['conditional'].'\' ';
										$option_class .= 'option-conditional ';
								}

								if(isset($customField['show-if']) && !$is_index){

									foreach ($customField['show-if'] as $key => $value) {
										// Move var to add prefix
										$customField['show-if'][$this->prefix.$key] = $customField['show-if'][$key];
										unset($customField['show-if'][$key]);
									}

									$data_attributes .= 'data-scope =\''.json_encode($customField['scope']).'\' ';
									$data_attributes .= 'data-show-if =\''.json_encode($customField['show-if']).'\' ';
									$data_attributes .= 'data-show =\''.json_encode($customField['show-if']).'\' ';
									$option_class .= 'option_hidden ';
								}


								?>
								<div id="div-<?php echo $customField[ 'name' ]; ?>" class="col-1-1 js-accordion-header <?php echo $option_class; ?>" <?php echo $data_attributes; ?>>
									<div class="rffw_option_header">
									  <span class="rffw_option_title"><?php echo $customField[ 'title' ]; ?></span>
									  <span class="rffw_option_subtitle"><?php echo $customField[ 'subtitle' ]; ?></span>
										<span class="dashicons dashicons-arrow-down-alt2"></span>
									</div>
								</div>
								<div class="accordion-content" id="fold-<?php echo $customField[ 'name' ]; ?>">

									<?php
									foreach ( $customField['fields'] as $customFieldInSection ){
										$this->buildcustomField($customFieldInSection);
									}
									?>

								</div>

								<?php
							}
						}
						else{
							$this->buildcustomField($customField);
						}
					}

					?>

        </div>
			</div>
			<?php
		}
		function buildcustomField($customField){
			global $post, $theme_options;
			// Check scope
			$scope = $customField['scope'];
			$output = false;
			$is_index = false;
			foreach ( $scope as $scopeItem ) {
					if ($scopeItem == $post->post_type || ($scopeItem == 'index' && in_array($post->ID, $this->index_page_ids)) ){
						$output = true;
						$customField['scope'] = array($scopeItem);
						if($scopeItem == 'index' && in_array($post->ID, $this->index_page_ids)) $is_index = true;
						break;
					}
			}

			// Check capability
			if ( !current_user_can( $customField['capability'], $post->ID ) )
				$output = false;

			// Output if allowed
			if ( $output ){


				$data_attributes = '';
				$option_class = '';


				if(isset($customField['conditional'])){
						$data_attributes .= 'data-conditional =\''.$customField['conditional'].'\' ';
						$option_class .= 'option-conditional ';
				}

				if(isset($customField['show-if'])&& !$is_index){

					foreach ($customField['show-if'] as $key => $value) {
						// Move var to add prefix
						$customField['show-if'][$this->prefix.$key] = $customField['show-if'][$key];
						unset($customField['show-if'][$key]);
					}

					$data_attributes .= 'data-scope =\''.json_encode($customField['scope']).'\' ';
					$data_attributes .= 'data-show-if =\''.json_encode($customField['show-if']).'\' ';
					$data_attributes .= 'data-show =\''.json_encode($customField['show-if']).'\' ';
					$option_class .= 'option_hidden ';
				}


				if(isset($customField['responsive'])){
					echo '<div id="div-'.$this->prefix . $customField[ 'name' ].'" class="col-'.$customField['responsive'].' '.$option_class.'" '.$data_attributes.'>';
				}
				else{
					echo '<div id="div-'.$this->prefix . $customField[ 'name' ].'" class="col-1-2 '.$option_class.'" '.$data_attributes.'>';
				}

				?>
				<div class="rffw-framework form-field form-required">

					<?php
					if(!@include(get_template_directory().'/admin/rffw-framework/customfields/'.sanitize_title($customField['type']).'.php'))
					throw new Exception('Failed to include customfield: '.sanitize_title($customField['type']).'.php');
					?>
					<?php if ( isset($customField[ 'description' ]) ) echo '<p class="description">' . $customField[ 'description' ] . '</p>'; ?>
				</div>
				<?php
				echo '</div>';
			}
		}
		/**
		* Save the new Custom Fields values
		*/
		function saveCustomFields( $post_id, $post ) {
			if (!isset($_POST['rffw_hidden_flag']))
				return;
			if ( isset($_POST[ 'my-custom-fields_wpnonce' ]) && !wp_verify_nonce( $_POST[ 'my-custom-fields_wpnonce' ], 'my-custom-fields' ) )
				return;
			if ( !current_user_can( 'edit_post', $post_id ) )
				return;

			// Set save
			update_post_meta( $post_id, $this->prefix .'saved', true );

			foreach ( $this->customFields as $customField ) {
				if($customField['type']=='options-section'){
					foreach ( $customField['fields'] as $customFieldInSection ){
						$this->saveCustomField($post_id, $post, $customFieldInSection);
					}
				}
				else{
						$this->saveCustomField($post_id, $post, $customField);
				}
			}
		}

	function saveCustomField($post_id, $post, $customField){

		if ( current_user_can( $customField['capability'], $post_id ) && isset($customField[ 'name' ]) ){
			// Fix for saving 0 to database cast to integer or replace on on return
			if(isset( $_POST[ $this->prefix . $customField['name'] ] ) && $_POST[$this->prefix.$customField['name']] == '0')	$_POST[$this->prefix . $customField['name'] ] = '0x0';

			if ( isset( $_POST[ $this->prefix . $customField['name'] ] ) && trim((is_array($_POST[$this->prefix.$customField['name']]) ? implode(",",$_POST[$this->prefix.$customField['name']]) : $_POST[$this->prefix.$customField['name']]))) {
				update_post_meta( $post_id, $this->prefix.$customField[ 'name' ], (is_array($_POST[$this->prefix.$customField['name']]) ? implode(",",$_POST[$this->prefix.$customField['name']]) : $_POST[$this->prefix.$customField['name']]) );
			} else {
				delete_post_meta( $post_id, $this->prefix . $customField[ 'name' ] );
			}
		}
	}
}	 // End Class
} // End if class exists statement
