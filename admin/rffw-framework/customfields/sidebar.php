<?php
  // Dropdown field
  echo '<label for="' . $this->prefix . $customField[ 'name' ] .'"><b>' . $customField[ 'title' ] . '</b></label>';

  echo '<select name="' . $this->prefix . $customField[ 'name' ] .'" id="' . $this->prefix . $customField[ 'name' ] .'" class="rffw-dropdown">';
  foreach ($GLOBALS['wp_registered_sidebars'] as $sidebar) {
    if ($sidebar['id']) {
      $selected = '';
      if (get_post_meta($post->ID, $this->prefix.$customField['name'], true) == $sidebar['id']) {
        $selected = 'selected="selected"';
      }
      echo '<option '.$selected.' value="'.$sidebar['id'].'">'.$sidebar['name'].'</option>';
    }
  }
  echo '</select>';
?>
