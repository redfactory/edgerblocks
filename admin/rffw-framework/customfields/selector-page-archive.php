<?php
//save main post
$main_post = $post;

// Dropdown field
echo '<label for="' . $this->prefix . $customField[ 'name' ] .'"><b>' . $customField[ 'title' ] . '</b></label>';
echo '<select name="' . $this->prefix . $customField[ 'name' ] .'" id="' . $this->prefix . $customField[ 'name' ] .'" class="widefat">';

  // Get checked id's and make array
  $selected_page = get_post_meta( $post->ID, $this->prefix.$customField['name'], true);


  echo '<option value="" selected="selected">Select a ' . $customField[ 'title' ] . '</option>';

  // add achive page post type to link to in list
  $exclude_post_types = array('attachment', 'page', 'section');
  foreach ( get_post_types( array('public'  => true ), 'objects' ) as $post_type ) {
    if(in_array($post_type->name, $exclude_post_types))  continue;

    // Set checked or don't
    $selected = ($post_type->name == $selected_page)? 'selected="selected"' : '';
    echo '<option '.$selected.' value="'.$post_type->name.'">'.esc_html__('Archive', 'edgerblocks').': '.$post_type->labels->name.'</option>';
  }


  // Get other posts
  $all_posts = new WP_Query(array_merge($customField['wp_query_args'], array(
    'posts_per_page' 	=> -1,
    'suppress_filters'=> 0
  )));

  // loop
  while ( $all_posts->have_posts() ) : $all_posts->the_post();

    $_title = (empty(get_the_title()))?	__('No title', 'edgerblocks'): get_the_title();
    // Set checked or don't
    $selected = (get_the_ID() == $selected_page)? 'selected="selected"' : '';
    echo '<option '.$selected.' value="'.get_the_ID().'">'.$_title.'</option>';

  endwhile;
  wp_reset_postdata();

  // wp_reset_query working alternative
  $post = $main_post;

  echo '<option value="" >No ' . $customField[ 'title' ] . '</option>';
echo '</select>';
?>
