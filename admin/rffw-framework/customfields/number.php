<?php
// Fix for saving 0 to database cast to integer or replace on on return
$number = get_post_meta( $post->ID, $this->prefix . $customField[ 'name' ], true );
if($number == '0x0') $number = '0';

echo '<label for="' . $this->prefix . $customField[ 'name' ] .'"><b>' . $customField[ 'title' ] . '</b></label>';
echo '<input class="rffw-text" type="number"
  max="' . $this->prefix . $customField[ 'max' ] . '"
  min="' . $this->prefix . $customField[ 'min' ] . '"
  placeholder="' . $customField[ 'placeholder' ] . '"
  name="' .$this->prefix.$customField['name'].'"
  id="'.$this->prefix.$customField['name'].'"
  value="'.$number.'" />';
?>
