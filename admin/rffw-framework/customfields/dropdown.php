<?php
// Dropdown field
echo '<label for="' . $this->prefix . $customField[ 'name' ] .'"><b>' . $customField[ 'title' ] . '</b></label>';
echo '<select name="' . $this->prefix . $customField[ 'name' ] .'" id="' . $this->prefix . $customField[ 'name' ] .'" class="rffw-dropdown">';

// options can be array or sting with ,
if(isset($customField['options'])){
  if(is_array($customField['options'])){
    $options = $customField['options'];
  }
  else{
    $options = explode(',',$customField['options']);
  }

  if(isset($customField['options_value']) && is_array($customField['options_value'])){
    $options_value = true;
  }
}
else{
  echo '<pre>No options found.<pre>';
}

foreach ($options as $index => $option) {
  $selected = '';

  if (!empty(get_post_meta($post->ID, $this->prefix.$customField['name'], true)) && get_post_meta($post->ID, $this->prefix.$customField['name'], true) == $option ||
      !empty(get_post_meta($post->ID, $this->prefix.$customField['name'], true)) && get_post_meta($post->ID, $this->prefix.$customField['name'], true) == $customField['options_value'][$index] ||
      isset($_GET[$this->prefix.$customField['name']]) && $_GET[$this->prefix.$customField['name']] == $option  ||
      isset($_GET[$this->prefix.$customField['name']]) && $_GET[$this->prefix.$customField['name']] == $customField['options_value'][$index]){
      // Set seleced
      $selected = 'selected="selected"';
  }

  if($options_value)  echo '<option '.$selected.' value="'.$customField['options_value'][$index].'">'.$option.'</option>';
  else                echo '<option '.$selected.' value="'.$option.'">'.$option.'</option>';
}
echo '</select>';
?>
