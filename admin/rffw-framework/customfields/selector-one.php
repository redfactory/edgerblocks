<?php
//save main post
$main_post = $post;

// Dropdown field
echo '<label for="' . $this->prefix . $customField[ 'name' ] .'"><b>' . $customField[ 'title' ] . '</b></label>';
echo '<select name="' . $this->prefix . $customField[ 'name' ] .'" id="' . $this->prefix . $customField[ 'name' ] .'" class="widefat">';

  // Get checked id's and make array
  $selected_post_id = get_post_meta( $post->ID, $this->prefix.$customField['name'], true);

  // Get other posts
  $all_posts = new WP_Query(array_merge($customField['wp_query_args'], array(
    'posts_per_page' 	=> -1,
    'suppress_filters'=> 0
  )));

  echo '<option value="" selected="selected" disabled >Select a ' . $customField[ 'title' ] . '</option>';
  echo '<option value=""> - '.esc_html__('Select none', 'edgerblocks').' - </option>';
  // loop
  while ( $all_posts->have_posts() ) : $all_posts->the_post();

    $_title = (empty(get_the_title()))?	__('No title', 'edgerblocks'): get_the_title();
    // Set checked or don't
    $selected = (get_the_ID() == intval($selected_post_id))? 'selected="selected"' : '';
    echo '<option '.$selected.' value="'.get_the_ID().'">'.$_title.'</option>';

  endwhile;
  wp_reset_postdata();

  // wp_reset_query working alternative
  $post = $main_post;

  echo '<option value="" >No ' . $customField[ 'title' ] . '</option>';
echo '</select>';
?>
