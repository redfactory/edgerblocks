<?php
// Dropdown field
echo '<label for="' . $this->prefix . $customField[ 'name' ] .'"><b>' . $customField[ 'title' ] . '</b></label>';
echo '<select name="' . $this->prefix . $customField[ 'name' ] .'" id="' . $this->prefix . $customField[ 'name' ] .'" class="widefat rffw-dropdown">';

// options can be array or sting with ,
if(isset($customField['options'])){
  if(is_array($customField['options'])){
    $options = $customField['options'];
  }
}
else{
  echo '<pre>No options found.<pre>';
}

foreach ($options as $option) {
  $selected = '';


  if (!empty(get_post_meta($post->ID, $this->prefix.$customField['name'], true)) &&
      get_post_meta($post->ID, $this->prefix.$customField['name'], true) == $option['slug'] ||
      isset($_GET[$this->prefix.$customField['name']]) && $_GET[$this->prefix.$customField['name']] == $option['slug']){
      // Set seleced
      $selected = 'selected="selected"';
  }

  echo '<option '.$selected.' value="'.$option['slug'].'">'.$option['name'].'</option>';

}
echo '</select>';
?>
