<?php
  echo '<label for="' . $this->prefix . $customField[ 'name' ] .'"><b>' . $customField[ 'title' ] . '</b></label>';
  echo '<div class="sortable-list">';

  $category_array = get_post_meta( $post->ID, $this->prefix . $customField['name'], true );
  $temp_array = array();
  if ($category_array) {
    $temp_array = explode(',',$category_array);
    foreach ($temp_array as $catid) {
      echo '<div class="sortable">';
      echo '<input type="checkbox" checked name="' . $this->prefix . $customField['name'] . '[]" id="' . $this->prefix . $customField['name'] . '" value="' . $catid .'" />' . get_the_title($catid);
      echo '</div>';
    }
  }

  $args = array( 'posts_per_page' => -1, 'post_type' => 'portfolio', 'order' => 'DESC');
  $sections = get_posts( $args );

  foreach ( $sections as $section ) :
    if (!in_array($section->ID, $temp_array)) {
      echo '<div class="sortable">';
      echo '<input type="checkbox" name="' . $this->prefix . $customField['name'] . '[]" id="' . $this->prefix . $customField['name'] . '" value="' . $section->ID .'" />' . $section->post_title;
      echo '</div>';
    }
  endforeach;
  
  wp_reset_postdata();
  echo '</div>';
?>
