<?php

if ( get_post_meta( $post->ID, $this->prefix . $customField['name'], true ) == true ||
    (
      empty(get_post_meta( $post->ID, $this->prefix . $customField['name'], true )) &&
      empty(get_post_meta( $post->ID, $this->prefix . 'saved', true )) &&
      $customField[ 'default' ] == true
    )
  ){
    $checked =' checked="checked"';
    $value = 1;
  }
  else{
    $checked ='';
    $value = 0;
  }

  echo '<label for="' . $this->prefix . $customField[ 'name' ] .'"><b>'. $customField[ 'title' ] .'</b></label>';
  echo '<div class="switch"><input class="cmn-toggle cmn-toggle-round" type="checkbox" name="' . $this->prefix . $customField['name'] . '" id="' . $this->prefix . $customField['name'] . '" value="'.$value.'"';
  echo $checked;
  echo '" style="width: auto;" onclick="jQuery(this).val(this.checked ? 1 : 0)" />';
  echo '<label for="' . $this->prefix . $customField[ 'name' ] .'"></label></div>';
?>
