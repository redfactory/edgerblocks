<?php
// add special sections to section array
global $rffw_post_types, $rffw_section_types;

// Add thickbox for iframe popup links
add_thickbox();

?>

<div class="grid grid-pad">
	<div class="page-sections">

		<div class="add-section">

			<a id="show-existing" data-titleclose="<?php esc_html_e('Close Existing Section', 'edgerblocks'); ?>" data-titleopen="<?php esc_html_e('Add Existing Section', 'edgerblocks') ?>" class="add-existing" href="#"><?php esc_html_e('Add Existing Section', 'edgerblocks') ?></a>

			<div class="new-section-drop">
				<a id="add-new-section" class="button button-primary button-large dashicons-before dashicons-arrow-down" href="#"><?php esc_html_e('Add New Section', 'edgerblocks') ?></a>
			  <ul id="loop-section-types">
					<?php	foreach ($rffw_section_types as $section_type) { ?>
						<li><a class="thickbox dashicons-before <?php echo $section_type['icon']; ?>" href="<?php echo get_admin_url(); ?>post-new.php?post_type=section&iframe=1&#038;<?php echo $this->prefix; ?>sectiontype=<?php echo $section_type['slug']; ?>&#038;width=1000&#038;height=800&#038;TB_iframe=true"><?php echo $section_type['name']; ?></a></li>
					<?php } ?>
			  </ul>
			</div>
		<span class="section_text"><?php esc_html_e('Add a new section or select an existing section to create this page. You can reorder them by drag and drop. Don\'t forget to update this page to save your changes.', 'edgerblocks'); ?></span>

		</div>

		<div class="field-header">
			<label for="<?php echo $this->prefix.$customField[ 'name' ]; ?>">
				<b><?php echo $customField[ 'title' ]; ?></b>
			</label>
		</div>

		<div class="section-panels-box">
			<div class="all-section-panel">
				<div class="search-section-header">
					<input type="text" placeholder="<?php esc_attr_e('Type To Search Sections...', 'edgerblocks'); ?>" id="section-search" value="">
					<span id="spinnerSectionSearch" class="spinner"></span>
				</div>
				<ol id="section-add-list">
					<span class="text"><?php esc_html_e('Last 20 sections', 'edgerblocks') ?></span>
		<?php
		// Get all section objects
		$sections = get_posts(
						array( 	'posts_per_page' 	=> 20,
										'post_type' 			=> 'section',
										//'order' 					=> 'asc', //DESC or ASC
										//'orderby'				=> 'title',
										'post__not_in' 		=> $sections_ids_on_this_page,  // Dont get sections that are already on this page.
										'suppress_filters'=> 0, 													// shows only the selected translation with this line
										'post_status' 		=> array('publish', 'pending', 'draft', 'future', 'private', 'inherit')
						));

		// Loop all sections
		foreach ( $sections as $section_post ){

				// add meta to post object
				$section_post = rffw_add_meta_to_post($section_post);

				// section type key
				$rffw_section_type_key = array_search($section_post->rffw_meta['sectiontype'], array_column($rffw_section_types, 'slug'));

				// Set title to no tile if empty
				if($section_post->post_title=='') $section_post->post_title = __('No title', 'edgerblocks');
				?>
				<li>
					<label class="menu-item-title">
						<input type="checkbox" class="" name="all-sections-checked[]" data-id="<?php echo $section_post->ID; ?>" data-title='<?php echo $section_post->post_title; ?>' data-type='<?php echo sanitize_title($section_post->_rffw_sectiontype); ?>' data-name='<?php echo $rffw_section_types[$rffw_section_type_key]['name']; ?>' data-icon='<?php echo $rffw_section_types[$rffw_section_type_key]['icon']; ?>' > <?php echo $section_post->post_title; ?> </label>
						<a class="thickbox edit dashicons-before <?php echo $rffw_section_types[$rffw_section_type_key]['icon']; ?>" href="<?php echo get_admin_url(); ?>post.php?post=<?php echo $section_post->ID; ?>&action=edit&iframe=1&#038;width=1000&#038;height=800&#038;TB_iframe=true&#038;hidemenu=true" title="<?php esc_html_e('Edit this section','edgerblocks'); ?>"></a>
				</li>
			<?php
		}

		?>

		</ol>

		<div class="button-controls">
			<a id="section-add-to-page" class="button" href="#">Add to this page</a>
		</div>
	</div>

	<div class="page-section-panel">
		<ol id="sectionsPage" class="sort-section-list sortable-list">

<?php
// Get Section array string and build array
$sections_json = get_post_meta( $post->ID, $this->prefix . $customField['name'], true );
$sections_array = json_decode($sections_json);

$specialsections = $rffw_post_types[get_post_type($post->ID)]['default-sections'];


//If content is not in array add on top
foreach($specialsections as $key => $value){

	// relpace content with index
	if( in_array($post->ID, $this->index_page_ids) && $key == 'content'){
		$value = __('The page index', 'edgerblocks');
		$specialsections[$key] = $value;
	}

	if(isset($sections_array) && !empty($sections_array)){

		$section_content_object = objArraySearch($sections_array, 'id', $key);
		if(isset($section_content_object) && $section_content_object->id == $key){
			// Do noting already in array
		}
		else{
			// Add object to array
			$sections_array[] = (object) array(
								'id'        => $key,
								'name'   		=> $value,
								'visible'   => 'true'
							);
		}
	}
	else{
		// Add object to array
		$sections_array[] = (object) array(
		          'id'        => $key,
							'name'   		=> $value,
		          'visible'   => 'true'
		        );
	}
}

// Build empty array
$sections_ids_on_this_page = array();

// Loop array of sections and content on page
foreach ($sections_array as $section_object){

	// filter custom sections
	if (!is_numeric($section_object->id)){

			// Add section name if not set
			if(!isset($section_object->name)){
					$section_object->name = $specialsections[$section_object->id];
			}

			// add checked or class
			if($section_object->visible){
				$class = '';
				$checked = 'checked';
			}else{
				$class = 'off';
				$checked = '';
			}

			// Build page special sortable item
			echo '<li data-visible="'.$section_object->visible.'" data-id="'.$section_object->id.'" id="sortable-'.$section_object->id.'" class="sortable-content sortable dashicons-after wp-menu-image icon-pageContent '.$class.'">';
				echo $section_object->name;
				echo '<input class="section-visible-check" type="checkbox" '.$checked.' name="visible" id="section-'.$section_object->id.'-visible" value="true" />';
			echo '</li>';

		}
		else{
			// Replace ID if translation exists (WPML)
			if (function_exists('icl_object_id')){
				$section_object->id = icl_object_id($section_object->id, 'section', true, ICL_LANGUAGE_CODE);
			}
			elseif (has_filter('wpml_object_id')){
				$section_object->id = apply_filters( 'wpml_object_id', $section_object->id, 'section', true);
			}

		  // add section id to array
			$sections_ids_on_this_page[] = $section_object->id;

			// Get section
			$section_post = get_post($section_object->id);

			// add meta to post object
			$section_post = rffw_add_meta_to_post($section_post);

			if($section_post && $section_post->post_status != 'trash'){
					// Set title to no tile if empty
					if($section_post->post_title=='')	$section_post->post_title = __('No title', 'edgerblocks');

					// Get section type key to get icon and mabye other section type info
					$rffw_section_type_key = array_search($section_post->rffw_meta['sectiontype'], array_column($rffw_section_types, 'slug'));

					// Build section sortable item
					echo '<li data-visible="'.$section_object->visible.'"  data-type="'.sanitize_title($section_post->_rffw_sectiontype).'" data-id="'.$section_post->ID.'" id="sortable-'.$section_post->ID.'" class="sortable wp-menu-image dashicons-before '.$rffw_section_types[$rffw_section_type_key]['icon'].'">';
						echo '<span class="section_name">'.$rffw_section_types[$rffw_section_type_key]['name'].'</span>';
						echo '<a class="thickbox" href="'.get_admin_url().'post.php?post='.$section_post->ID.'&action=edit&iframe=1&#038;width=1000&#038;height=800&#038;TB_iframe=true&#038;hidemenu=true">' .$section_post->post_title ."</a>";
						echo '<a class="remove-section" href="#">x</a><ol class="sub-sections">';

								if(isset($section_object->children[0]) && count($section_object->children[0]) >= 1){
									foreach($section_object->children[0] as $sub_section_object){
										// Replace ID if translation exists (WPML)
										if (function_exists('icl_object_id')){
											$sub_section_object->id = icl_object_id($sub_section_object->id, 'section', true, ICL_LANGUAGE_CODE);
										}
										elseif (has_filter('wpml_object_id')){
											$sub_section_object->id = apply_filters( 'wpml_object_id', $sub_section_object->id, 'section', true);
										}

										// Get section
										$sub_section_post = get_post($sub_section_object->id);

										if($sub_section_post && $sub_section_post->post_status != 'trash'){
												// Set title to no tile if empty
												if($sub_section_post->post_title=='')	$sub_section_post->post_title = __('No title', 'edgerblocks');

												echo '<li data-visible="'.$sub_section_object->visible.'" data-type="'.sanitize_title($sub_section_post->_rffw_sectiontype).'" data-id="'.$sub_section_post->ID.'" id="sortable-'.$sub_section_post->ID.'" class="sortable dashicons-after wp-menu-image icon-'.sanitize_title($sub_section_post->_rffw_sectiontype).'">';
													echo '<span class="section_name">'.$sub_section_post->_rffw_sectiontype.'</span>';
													echo '<a class="thickbox" href="'.get_admin_url().'post.php?post='.$sub_section_post->ID.'&action=edit&iframe=1&#038;width=1000&#038;height=800&#038;TB_iframe=true&#038;hidemenu=true">' .$sub_section_post->post_title ."</a>";
													echo '<a class="remove-section" href="#">x</a>';
												echo '</li>';
										}
									}
								}
						echo '</ol>';
					echo '</li>';
			}
	}
}
wp_reset_postdata();
?>
				</ol>
			</div>
		</div>
		<input type="text" name="<?php echo $this->prefix.$customField[ 'name' ]; ?>" id="<?php echo $this->prefix.$customField[ 'name' ]; ?>" value="<?php echo htmlspecialchars( get_post_meta( $post->ID, $this->prefix . $customField[ 'name' ], true ) ); ?>" style="display:none" />
	</div>
</div>
