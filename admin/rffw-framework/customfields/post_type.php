<?php
// Dropdown with postypes
echo '<label for="' . $this->prefix . $customField[ 'name' ] .'"><b>' . $customField[ 'title' ] . '</b></label>'; ?>
  <select id="<?php echo $this->prefix . $customField[ 'name' ]; ?>" name="<?php echo $this->prefix . $customField[ 'name' ]; ?>" class="rffw-dropdown">
      <?php foreach ( get_post_types( array(   'public'   => true ), 'objects' ) as $post_type ) { ?>
         <option <?php selected( get_post_meta($post->ID, $this->prefix.$customField['name'], true), $post_type->name); ?> value="<?php echo $post_type->name; ?>"><?php echo $post_type->label; ?></option>
      <?php }	?>
  </select>
