<?php
  echo '<label for="' . $this->prefix . $customField[ 'name' ] .'"><b>' . $customField[ 'title' ] . '</b></label>';

  wp_editor(
    get_post_meta( $post->ID, $this->prefix . $customField[ 'name' ], true ), $this->prefix . $customField[ 'name' ], $settings = array()
  );
?>
