<?php
  echo '<label for="' . $this->prefix . $customField[ 'name' ] .'"><b>' . $customField[ 'title' ] . '</b></label>';
  echo '<div class="sortable-list">';

  $category_array = get_post_meta( $post->ID, $this->prefix . $customField['name'], true );
  $temp_array = array();
  if ($category_array) {
    $temp_array = explode(',',$category_array);
    foreach ($temp_array as $catid) {
      $cat = get_term( $catid, 'portfoliocat' );
      echo '<div class="sortable">';
      echo '<input type="checkbox" checked name="' . $this->prefix . $customField['name'] . '[]" id="' . $this->prefix . $customField['name'] . '" value="' . $catid .'" />' . $cat->name;
      echo '</div>';
    }
  }

  $args = array(
    'orderby' => 'name',
    'order' => 'ASC',
    'taxonomy' => 'portfoliocat'
  );
  $categories = get_categories($args);

  foreach ( $categories as $cat ) :
    if (!in_array($cat->term_id, $temp_array)) {
      echo '<div class="sortable">';
      echo '<input type="checkbox" name="' . $this->prefix . $customField['name'] . '[]" id="' . $this->prefix . $customField['name'] . '" value="' . $cat->term_id .'" />' . $cat->name;
      echo '</div>';
    }
  endforeach;
  
  wp_reset_postdata();
  echo '</div>';
?>
