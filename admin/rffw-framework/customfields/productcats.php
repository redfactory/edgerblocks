<?php
  // Category list
  echo '<label for="' . $this->prefix . $customField[ 'name' ] .'"><b>' . $customField[ 'title' ] . '</b></label>';
  echo '<select name="' . $this->prefix . $customField[ 'name' ] .'" id="' . $this->prefix . $customField[ 'name' ] .'">';
  echo '<option value="">'.esc_html__('Select category', 'edgerblocks').'</option>';

  $all_categories = get_categories(array(
       'taxonomy'     => 'product_cat',
       'orderby'      => 'name',
       'show_count'   => 0,
       'pad_counts'   => 0,
       'hierarchical' => 1,
       'title_li'     => '',
       'hide_empty'   => 0
   ));

  foreach ($all_categories as $category) {
    $selected = '';
    if (get_post_meta($post->ID, $this->prefix.$customField['name'], true) == $category->slug) {
      $selected = 'selected="selected"';
    }
    echo '<option '.$selected.' value="'.$category->slug.'">'.$category->name.'</option>';
  }
  echo '</select>';
?>
