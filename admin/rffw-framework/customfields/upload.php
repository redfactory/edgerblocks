<?php
$attachment_id = (!empty(get_post_meta( $post->ID, $this->prefix . $customField[ 'name' ], true )))? get_post_meta( $post->ID, $this->prefix . $customField[ 'name' ], true ) :0;

wp_enqueue_media();

// Striped from dash for js
$field =str_replace('-', '_', $this->prefix . $customField[ 'name' ]);
?>
<label for="<?php echo $field; ?>"><b><?php echo  $customField[ 'title' ] ?></b></label>
<input type='hidden' name='<?php echo $this->prefix . $customField[ 'name' ] ?>' id='<?php echo $this->prefix . $customField[ 'name' ] ?>' value='<?php echo $attachment_id ?>'>
<a href="#" id="" class='image-preview-wrapper upload_image_<?php echo $field; ?>'>
	<img <?php if($attachment_id == 0){ echo 'style="display:none;"'; } ?> id='image_<?php echo $field; ?>' src='<?php echo wp_get_attachment_url($attachment_id ); ?>' height='100'>
</a>
<a <?php if($attachment_id >  0){ echo 'style="display:none;"'; } ?> href="#" id="upload_image_button_<?php echo $field; ?>" class='upload_image_<?php echo $field; ?>'><?php esc_html_e( 'Set Image', 'edgerblocks'); ?></a>
<a <?php if($attachment_id == 0){ echo 'style="display:none;"'; } ?> href="#" id="remove_image_button_<?php echo $field; ?>"><?php esc_html_e( 'Remove image', 'edgerblocks'); ?></a>


<script>
jQuery( document ).ready( function( $ ) {
	// Uploading files
	var file_frame_<?php echo $field; ?>;
	var wp_media_post_id = wp.media.model.settings.post.id; // Store the old id
	var set_to_post_id_<?php echo $field; ?> = <?php echo $attachment_id; ?>; // Set this

	jQuery('.upload_image_<?php echo $field; ?>').on('click', function( event ){
		event.preventDefault();

		// If the media frame already exists, reopen it.
		if ( file_frame_<?php echo $field; ?> ) {

			// Set the post ID to what we want
			file_frame_<?php echo $field; ?>.uploader.uploader.param( 'post_id', set_to_post_id_<?php echo $field; ?> );
			// Open frame
			file_frame_<?php echo $field; ?>.open();
			return;
		} else {
			// Set the wp.media post id so the uploader grabs the ID we want when initialised
			wp.media.model.settings.post.id = set_to_post_id_<?php echo $field; ?>;
		}
		// Create the media frame.
		file_frame_<?php echo $field; ?> = wp.media.frames.file_frame_<?php echo $field; ?> = wp.media({
			title: 'Select a image to upload',
			button: {
				text: 'Use this image',
			},
			multiple: false	// Set to true to allow multiple files to be selected
		});

		// When an image is selected, run a callback.
		file_frame_<?php echo $field; ?>.on( 'select', function() {
			// We set multiple to false so only get one image from the uploader
			attachment = file_frame_<?php echo $field; ?>.state().get('selection').first().toJSON();
			// Do something with attachment.id and/or attachment.url here
			$( '#image_<?php echo $field; ?>' ).attr( 'src', attachment.url ).css( 'width', 'auto' ).show();
			$( '#<?php echo $this->prefix . $customField[ 'name' ] ?>' ).val( attachment.id );
			// Restore the main post ID
			wp.media.model.settings.post.id = wp_media_post_id;
			jQuery("a#upload_image_button_<?php echo $field; ?>").hide();
			jQuery("a#remove_image_button_<?php echo $field; ?>").show();
		});

		file_frame_<?php echo $field; ?>.on('open',function() {
				// We set multiple to false so only get one image from the uploader
				var selection = file_frame_<?php echo $field; ?>.state().get('selection');
				id = jQuery('#<?php echo $this->prefix . $customField[ 'name' ] ?>').val();

				if(id){
					attachment = wp.media.attachment(id);
					attachment.fetch();
					selection.add( attachment ? [ attachment ] : [] );
				}
				else{
					selection.add( [] );
				}
		 });


			// Finally, open the modal
			file_frame_<?php echo $field; ?>.open();
	});
	// Restore the main ID when the add media button is pressed
	jQuery("a.add_media").on( 'click', function() {
		wp.media.model.settings.post.id = wp_media_post_id;
	});
	// Restore the main ID when the add media button is pressed
	jQuery("a#remove_image_button_<?php echo $field; ?>").on( 'click', function() {
		set_to_post_id = 0;

		//var selection = file_frame_<?php echo $field; ?>.state().get('selection');
		//selection.reset([]);
		//selection.reset( selected ? [ wp.media.attachment( selected ) ] : [] );

		$('#<?php echo $this->prefix . $customField[ 'name' ] ?>' ).val('');
		jQuery('#image_<?php echo $field; ?>').removeAttr('src').hide();
		jQuery(this).hide();
		jQuery("a#upload_image_button_<?php echo $field; ?>").show();
		event.preventDefault();
	});
});
</script>
<?php
