<?php

  //Label for customfield
  echo '<label for="' . $this->prefix . $customField[ 'name' ] .'"><b>' . $customField[ 'title' ] . '</b></label>';

  //Generate list for customfield
  echo '<div class="sortable-list">';
  $category_array = get_post_meta( $post->ID, $this->prefix . $customField['name'], true );
  $temp_array = array();
  if ($category_array) {
    $temp_array = explode(',',$category_array);
    foreach ($temp_array as $catid) {
      echo '<div class="sortable">';
      echo '<input type="checkbox" checked name="' . $this->prefix . $customField['name'] . '[]" id="' . $this->prefix . $customField['name'] . '" value="' . $catid .'" />' . get_cat_name($catid);
      echo '</div>';
    }
  }
  $categories = get_categories('order_by=name&order=asc&hide_empty=0');
  foreach ($categories as $cat) {
    if ($cat->category_parent == 0 && !in_array($cat->term_id, $temp_array)) {
      echo '<div class="sortable">';
      echo '<input type="checkbox" name="' . $this->prefix . $customField['name'] . '[]" id="' . $this->prefix . $customField['name'] . '" value="' . $cat->term_id .'" />' . $cat->name;
      echo '</div>';
    }
  }
  echo '</div>';
?>
