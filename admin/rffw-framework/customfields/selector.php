<?php
  //save main post
  $main_post = $post;

  // Get checked id's and make array
  $pages_checked = get_post_meta( $post->ID, $this->prefix.$customField['name'], true);
  $pages_checked_array = explode(",", $pages_checked);

  echo '<label for="' . $this->prefix . $customField[ 'name' ] .'"><b>' . $customField[ 'title' ] . '</b><span class="spinner select-cat-spinner"></span></label>';
  echo '<div id="' . $this->prefix . $customField[ 'name' ] .'_list" class="sortable-list sortable-single">';

  // Get post meta to from other custom field
  // if($product_cat = get_post_meta($post->ID, $this->prefix.'productcats', true)){
  //  $customField['wp_query_args']['product_cat'] = $product_cat;
  // }



  // Get checked posts
  $pages_checked = new WP_Query( array(
    'post__in'  		    => $pages_checked_array,
    'orderby' 			    => 'post__in',
    'posts_per_page' 	  => -1,
    'suppress_filters'  => 0,
    'post_type'         => get_post_types( array(   'public'   => true ), 'names' ),
    'post_status' => array('publish', 'pending', 'draft', 'future', 'inherit')

    ));

  // Get other posts
  $pages_notchecked = new WP_Query(array_merge($customField['wp_query_args'], array(
    'post__not_in'  	=> $pages_checked_array,
    'posts_per_page' 	=> -1,
    'suppress_filters'=> 0,
    'post_status' => array('publish', 'pending', 'draft', 'future', 'inherit')
    )));

  // Merge Query
  $all_pages = new WP_Query();
  $all_pages->posts = array_merge( $pages_checked->posts, $pages_notchecked->posts );
  $all_pages->post_count = count( $all_pages->posts );



  // Loop posts
  while ( $all_pages->have_posts() ) : $all_pages->the_post();

    // Set checked or don't
    $pagechecked = (in_array(get_the_ID(), $pages_checked_array)) ? 'checked' : '';
      echo '<div class="sortable sortable-single">';
      echo '<input type="checkbox" '.$pagechecked.' name="' . $this->prefix . $customField['name'] . '[]" id="' . $this->prefix . $customField['name'] . '" value="' . get_the_ID() .'" />' . get_the_title();
      echo '</div>';
  endwhile;

  // wp_reset_query working alternative
  $post = $main_post;

  echo '</div>';
?>
