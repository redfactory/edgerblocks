		</div><!-- #content -->

		<footer class="site-footer">
			<div class="hidden-xs">
				<?php if (is_active_sidebar('footer-widgets')) { ?>
					<div class="container">
						<div class="row">
							<?php rffw_the_widget_builder('footer-widgets') ?>
						</div>
					</div>
				<?php } ?>

				<div id="footer-text" class="container <?php if (is_active_sidebar('footer-bottom-widgets')) echo 'has-widgets'; ?>">
					<div class="row">
						<?php rffw_the_widget_builder('footer-bottom-widgets') ?>
					</div>
				</div>
			</div>
			<div class="visible-xs">
				<div class="container footer-mobile">
					<?php dynamic_sidebar('footer-mobile-widgets') ?>
				</div>
			</div>

		</footer>

	</div><!-- #container -->

	<?php wp_footer(); ?>
	</body>
</html>
