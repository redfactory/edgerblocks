<?php
/* Template Name: Evenement */

// add meta to post object
$post = rffw_add_meta_to_post($post);

// Get all customfields in array
$rffw_post_meta = rffw_get_post_custom_single(get_the_ID());

// Build ['featured_image_url'] & ['inline_style'] etc
$rffw_post_meta = rffw_build_style($rffw_post_meta, true);

get_header();

require_once(get_template_directory().'/sections.php');

get_footer();
