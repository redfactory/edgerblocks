<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="theme-color" content="<?php rffw_the_theme('theme-primary-color', 'color'); ?>">
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  <?php wp_head(); ?>
</head>

<?php global $post, $index_page_post;
if (isset($index_page_post) && is_object($index_page_post))  $post =  $index_page_post;

?>

<body <?php body_class(); ?>>
  <div id="container">

    <header class="<?php if(rffw_is_meta('menubar-transparent', $post)) echo 'transparent'; ?> <?php if(rffw_get_theme('navbar-bottom-background-color') != 'transparent') echo 'sticky-shadow'; ?> <?php if(!rffw_is_theme('menubar-sticky')) echo 'static'; ?>">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-9">
            <div class='site-logo'>
              <a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'>
                  <h1><?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?></h1>
              </a>
            </div>
          </div>

          <?php if (!rffw_is_theme('menu-always-mobile')) { ?>
          <div class="hidden-xs col-sm-8 col-lg-8 col-md-8">
            <nav id="mainMenu">
              <?php if(rffw_is_theme('search-button')) { ?>
                <form method="get" class="searchform" action="<?php echo get_home_url(); ?>">
                  <input type="text" name="s" class="s" placeholder="" value="<?php echo get_search_query(); ?>">
                  <input class="button" type="submit" value="Search">
                </form>
                <i class="glyphicon glyphicon-search site-search-open"></i>
              <?php } ?>

                <?php
                wp_nav_menu(array(
                  'theme_location' => 'main-menu',
                  'fallback_cb'    => 'rffw_emptymenu'
                ));
                ?>
            </nav>
          </div>
          <?php } ?>

          <div class="col-xs-3 <?php if (rffw_is_theme('menu-always-mobile')) echo 'col-sm-8 col-lg-9 col-md-8'; else echo 'hidden-sm hidden-md hidden-lg'; ?>">

              <div class="menu-button-container">
                <?php if(rffw_is_theme('search-button')) { ?>
                  <i class="glyphicon glyphicon-search site-search-open"></i>
                <?php } ?>

                <a class="menu-button mobile-open">
                  <div class="bar"></div>
                  <div class="bar"></div>
                  <div class="bar"></div>
                </a>
              </div>

              <nav class="wp_nav_menu_mobile <?php if (rffw_is_theme('menu-mobile-style')) rffw_the_theme('menu-mobile-style'); ?>">

                <div class="wp_nav_menu_mobile-container">

                  <div class="site-logo-mobile">
                    <h1><?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?></h1>
                  </div>
                  <a class="menu-button mobile-close">
                    <div class="bar"></div>
                    <div class="bar"></div>
                    <div class="bar"></div>
                  </a>

                  <?php wp_nav_menu(array(
                    'container_class' => 'dl-menuwrapper',
                    'container_id'    => 'dl-menu',
                    'menu_class'      => 'dl-menu dl-menuopen',
                    'theme_location'  => 'mobile-menu',
                    'fallback_cb'     => 'rffw_emptymenu'
                  )); ?>
                </div>
              </nav>
          </div>

        </div>
      </div>
      <form method="get" class="searchform mobile visible-xs" action="<?php echo get_home_url(); ?>">
        <input type="text" name="s" class="s" placeholder="" value="<?php echo get_search_query(); ?>">
        <input class="button" type="submit" value="Search">
      </form>
    </header>

    <div id="content" class="site-content">
