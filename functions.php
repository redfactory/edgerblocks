<?php
/**
* Stichting RPO functions and definitions.
*
* @link https://developer.wordpress.org/themes/basics/theme-functions/
*
* @package Stichting RPO
*/

$rffw_theme = wp_get_theme();
define('RFFW_NAME', 'Stichting RPO');
define('RFFW_SLUG', 'edgerblocks'); // will be the var name of redux settings
define('RFFW_VERSION', $rffw_theme->get('Version'));


$edgerblocks = array (
  'last_tab' => '1',
  'layout-type' => 'bodyFullwidth',
  'theme-primary-color' =>
  array (
    'color' => '#4A6CFA',
    'alpha' => '1',
    'rgba' => 'rgba(74,108,250,1)',
  ),
  'theme-background-color' =>
  array (
    'color' => '#ffffff',
    'alpha' => '1',
    'rgba' => 'rgba(255,255,255,1)',
  ),
  'background-image' =>
  array (
    'url' => '',
    'id' => '',
    'height' => '',
    'width' => '',
    'thumbnail' => '',
    'title' => '',
    'caption' => '',
    'alt' => '',
    'description' => '',
  ),
  'index-background-color' =>
  array (
    'color' => '#F6F6F6',
    'alpha' => '1',
    'rgba' => 'rgba(246,246,246,1)',
  ),
  'theme-gradient' => '',
  'theme-gradient-color1' =>
  array (
    'color' => '',
    'alpha' => '1',
    'rgba' => '',
  ),
  'theme-gradient-color2' =>
  array (
    'color' => '#c6c6c6',
    'alpha' => '1',
    'rgba' => 'rgba(198,198,198,1)',
  ),
  'theme-gradient-direction' => '',
  'theme-gradient-speed' => '30',
  'post-sidebar' => 'no-sidebar',
  'page-sidebar' => 'no-sidebar',
  'solution-sidebar' => 'no-sidebar',
  'index-sidebar' => 'no-sidebar',
  'logo-image' =>
  array (
    'url' => '',
    'id' => '',
    'height' => '',
    'width' => '',
    'thumbnail' => '',
    'title' => '',
    'caption' => '',
    'alt' => '',
    'description' => '',
  ),
  'logo-image-transparent' =>
  array (
    'url' => '',
    'id' => '',
    'height' => '',
    'width' => '',
    'thumbnail' => '',
    'title' => '',
    'caption' => '',
    'alt' => '',
    'description' => '',
  ),
  'logo-image-sticky' =>
  array (
    'url' => '',
    'id' => '',
    'height' => '',
    'width' => '',
    'thumbnail' => '',
    'title' => '',
    'caption' => '',
    'alt' => '',
    'description' => '',
  ),
  'logo-height' => '30',
  'logo-top' => '24',
  'logo-bottom' => '24',
  'navbar-bottom-background-color' =>
  array (
    'color' => '#ffffff',
    'alpha' => '1',
    'rgba' => 'rgba(255,255,255,1)',
  ),
  'navbar-bottom' =>
  array (
    'font-family' => 'Roboto',
    'font-options' => '',
    'google' => '1',
    'font-weight' => '500',
    'font-style' => '',
    'text-transform' => 'none',
    'font-size' => '14px',
    'line-height' => '16px',
  ),
  'navbar-bottom-link-color' =>
  array (
    'regular' => '#ffffff',
    'hover' => '#0a1f44',
    'active' => '#0a1f44',
  ),
  'sticky-navbar-bottom-link-color' =>
  array (
    'regular' => '#041e47',
    'hover' => '#f56a16',
    'active' => '#f56a16',
  ),
  'menubar-sticky' => '1',
  'menu-always-mobile' => '',
  'menu-mobile-style' => 'menu-mobile-fullscreen',
  'mobilemenu-background-color' =>
  array (
    'color' => '#ed7d42',
    'alpha' => '1',
    'rgba' => 'rgba(255,255,255,1)',
  ),
  'mobilemenu-font' =>
  array (
    'font-family' => 'Roboto',
    'font-options' => '',
    'google' => '1',
    'font-weight' => '400',
    'font-style' => '',
    'text-transform' => 'none',
    'font-size' => '16px',
    'line-height' => '16px',
  ),
  'mobilemenu-link-color' =>
  array (
    'regular' => '#ffffff',
    'hover' => '#0a1f44',
    'active' => '#0a1f44',
  ),
  'search-button' => '1',
  'search-on-type' => '0',
  'search-background' =>
  array (
    'color' => '#f56a16',
    'alpha' => '1',
    'rgba' => 'rgba(245,106,22,1)',
  ),
  'search-color' =>
  array (
    'color' => '#ffffff',
    'alpha' => '1',
    'rgba' => 'rgba(255,255,255,1)',
  ),
  'header-breadcrumb' => '1',
  'header-height' => '200',
  'header-image' =>
  array (
    'url' => get_stylesheet_directory_uri().'/images/header_bg.jpg',
    'id' => '',
    'height' => '',
    'width' => '',
    'thumbnail' => '',
    'title' => '',
    'caption' => '',
    'alt' => '',
    'description' => '',
  ),
  'header-background-color' =>
  array (
    'color' => '#ffffff',
    'alpha' => '1',
    'rgba' => 'rgba(255,255,255,1)',
  ),
  'header-text-style' => 'light-text',
  'header-parallax' => 'none',
  'cta-border-radius' => '10',
  'theme-primary' =>
  array (
    'font-family' => 'Roboto',
    'font-options' => '',
    'google' => '1',
  ),
  'theme-secondary' =>
  array (
    'font-family' => 'Roboto',
    'font-options' => '',
    'google' => '1',
  ),
  'title-dark' =>
  array (
    'font-weight' => '700',
    'font-style' => '',
    'text-transform' => 'uppercase',
    'font-size' => '16px',
    'color' => '#0a1f44',
  ),
  'subtitle-dark' =>
  array (
    'font-weight' => '900',
    'font-style' => '',
    'text-transform' => 'none',
    'font-size' => '32px',
    'color' => '#0a1f44',
  ),
  'page-subtitle-dark' =>
  array (
    'font-weight' => '400',
    'font-style' => '',
    'text-transform' => 'none',
    'font-size' => '14px',
    'color' => '#0a1f44',
  ),
  'body-dark' =>
  array (
    'font-weight' => '400',
    'font-style' => '',
    'text-transform' => 'none',
    'font-size' => '14px',
    'color' => '#53627c',
  ),
  'metadata-dark' =>
  array (
    'font-weight' => '400',
    'font-style' => '',
    'text-transform' => 'none',
    'font-size' => '12px',
    'color' => '#A2A2A2',
  ),
  'heading1-dark' =>
  array (
    'font-weight' => '700',
    'font-style' => '',
    'text-transform' => 'none',
    'font-size' => '38px',
    'color' => '#0a1f44',
  ),
  'heading2-dark' =>
  array (
    'font-weight' => '900',
    'font-style' => '',
    'text-transform' => 'none',
    'font-size' => '32px',
    'color' => '#0a1f44;',
  ),
  'heading3-dark' =>
  array (
    'font-weight' => '500',
    'font-style' => '',
    'text-transform' => 'none',
    'font-size' => '26px',
    'color' => '#0a1f44',
  ),
  'heading4-dark' =>
  array (
    'font-weight' => '700',
    'font-style' => '',
    'text-transform' => 'uppercase',
    'font-size' => '22px',
    'color' => '#0a1f44',
  ),
  'heading5-dark' =>
  array (
    'font-weight' => '400',
    'font-style' => '',
    'text-transform' => 'uppercase',
    'font-size' => '18px',
    'color' => '#0a1f44',
  ),
  'heading6-dark' =>
  array (
    'font-weight' => '',
    'font-style' => '',
    'text-transform' => 'none',
    'font-size' => '16px',
    'color' => '#0a1f44',
  ),
  'link-color-dark' =>
  array (
    'regular' => '#f56a16',
    'hover' => '#0a1f44',
    'active' => '#0a1f44',
  ),
  'sidebar-widget-title-dark' =>
  array (
    'font-weight' => '400',
    'font-style' => '',
    'text-transform' => 'uppercase',
    'font-size' => '18px',
    'color' => '#bcc1c4',
  ),
  'sidebar-widget-content-dark' =>
  array (
    'font-weight' => '400',
    'font-style' => '',
    'text-transform' => 'none',
    'font-size' => '16px',
    'color' => '#4a6cfa',
  ),
  'title-light' =>
  array (
    'color' => '#FFFFFF',
  ),
  'subtitle-light' =>
  array (
    'color' => '#FFFFFF',
  ),
  'page-subtitle-light' =>
  array (
    'color' => '#FFFFFF',
  ),
  'body-light' =>
  array (
    'color' => '#FFFFFF',
  ),
  'metadata-light' =>
  array (
    'color' => '#ffffff',
  ),
  'heading1-light' =>
  array (
    'color' => '#ffffff',
  ),
  'heading2-light' =>
  array (
    'color' => '#ffffff',
  ),
  'heading3-light' =>
  array (
    'color' => '#ffffff',
  ),
  'heading4-light' =>
  array (
    'color' => '#ffffff',
  ),
  'heading5-light' =>
  array (
    'color' => '#ffffff',
  ),
  'heading6-light' =>
  array (
    'color' => '#ffffff',
  ),
  'link-color-light' =>
  array (
    'regular' => '#ffffff',
    'hover' => '#ffffff',
    'active' => '#ffffff',
  ),
  'sidebar-widget-title-light' =>
  array (
    'color' => '#ffffff',
  ),
  'sidebar-widget-content-light' =>
  array (
    'color' => '#ffffff',
  ),
  'button-font' =>
  array (
    'font-family' => 'Roboto',
    'font-options' => '',
    'google' => '1',
    'font-weight' => '400',
    'font-style' => '',
    'text-transform' => 'none',
    'font-size' => '14px',
    'line-height' => '20px',
  ),
  'button-bg-color-dark' =>
  array (
    'regular' => '#f56a16',
    'hover' => '#FF9800',
    'active' => '#FF9800',
  ),
  'button-fg-color-dark' =>
  array (
    'regular' => '#ffffff',
    'hover' => '#ffffff',
    'active' => '#ffffff',
  ),
  'button-bg-color-light' =>
  array (
    'regular' => '#182c4f',
    'hover' => '#283e65',
    'active' => '#283e65',
  ),
  'button-fg-color-light' =>
  array (
    'regular' => '#ffffff',
    'hover' => '#ffffff',
    'active' => '#ffffff',
  ),
  'button-border-radius' => '24',
  'button' =>
  array (
    'padding-top' => '14',
    'padding-right' => '22',
    'padding-bottom' => '14',
    'padding-left' => '22',
  ),
  'button-raise' => '0',
  'footer-background-color' => '#f7f7f7',
  'footer-widget-title' =>
  array (
    'font-weight' => '400',
    'font-style' => '',
    'text-transform' => 'uppercase',
    'font-size' => '18px',
    'color' => '#4A6CFA',
  ),
  'footer-widget-content' =>
  array (
    'font-weight' => '400',
    'font-style' => '',
    'text-transform' => 'none',
    'font-size' => '16px',
    'color' => '#4a6cfa',
  ),
  'footer-link-color' =>
  array (
    'regular' => '#ffffff',
    'hover' => '#ffffff',
    'active' => '#ffffff',
  ),
  'footer-text-color' =>
  array (
    'font-weight' => '400',
    'font-style' => '',
    'text-transform' => 'none',
    'font-size' => '10px',
    'color' => '#ffffff'
    ),

  'footer-text' => '© 2018 | All rights reserved',
  'custom-js' => '',
  '404-title' => '404 - Pagina niet gevonden',
  '404-content' => 'Deze pagina kon niet worden gevonden.',
  'envato-username' => '',
  'envato-purchase-key' => '',
  'REDUX_last_saved' => 1541516699,
  'REDUX_LAST_SAVE' => 1541516699,
);

require get_template_directory() .'/admin/rffw-framework/class/page_for_post_type.php';
require get_template_directory() .'/admin/rffw_scss/rffw_scss.php';



if ( ! function_exists( 'edgerblocks_setup' ) ) :
	/**
	* Sets up theme defaults and registers support for various WordPress features.
	*
	* Note that this function is hooked into the after_setup_theme hook, which
	* runs before the init hook. The init hook is too late for some features, such
	* as indicating support for post thumbnails.
	*/
	function edgerblocks_setup() {


    add_theme_support( 'align-wide' );

		/*
		* Make theme available for translation.
		* Translations can be filed in the /languages/ directory.
		* If you're building a theme based on Stichting RPO, use a find and replace
		* to change 'edgerblocks' to the name of your theme in all the template files.
		*/
		load_theme_textdomain( 'edgerblocks', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		* Let WordPress manage the document title.
		* By adding theme support, we declare that this theme does not use a
		* hard-coded <title> tag in the document head, and expect WordPress to
		* provide it for us.
		*/
		add_theme_support( 'title-tag' );

		add_theme_support( 'woocommerce');

		/*
		* Enable support for Post Thumbnails on posts and pages.
		*
		* @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		*/
		add_theme_support( 'post-thumbnails' );
		add_image_size( 'fullwidth', 1920, 9999 );
		add_image_size( 'container', 1200, 9999 );

		/*register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'edgerblocks' ),
		) );*/

		// Register the wp3 menu
		register_nav_menus(
			array(
				'main-menu' => esc_html__( 'Main menu', 'edgerblocks' ),
				'top-menu' => esc_html__( 'Top menu', 'edgerblocks' ),
				'mobile-menu' => esc_html__( 'Mobile menu', 'edgerblocks' ),
				)
			);

			/*
			* Switch default core markup for search form, comment form, and comments
			* to output valid HTML5.
			*/
			add_theme_support( 'html5', array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			) );

      add_post_type_support( 'page', 'excerpt' );
			/*
			* Enable support for Post Formats.
			* See https://developer.wordpress.org/themes/functionality/post-formats/
			*/
			// Needs style
			// add_theme_support( 'post-formats', array(
			// 	'aside',
			// 	'image',
			// 	'video',
			// 	'quote',
			// 	'link',
			// ) );

			if ( ! isset( $content_width ) ) $content_width = 1200;
		}
	endif; // edgerblocks_setup
	add_action( 'after_setup_theme', 'edgerblocks_setup' );

function rffw_excerpt_length( $length ) {
    return 18;
}
add_filter( 'excerpt_length', 'rffw_excerpt_length', 999 );

function rffw_theme_excerpt_more( $more ) {
   return '';
}
add_filter( 'excerpt_more', 'rffw_theme_excerpt_more' );


//ignore sticky posts in count for posts per page
add_action( 'pre_get_posts', 'rffw_ignore_sticky');
function rffw_ignore_sticky($q) {
  if ( $q->is_main_query() && $q->is_home() ) {
    $count_stickies = count( get_option( 'sticky_posts' ) );
    $ppp = get_option( 'posts_per_page' );
    $offset = ( $count_stickies <= $ppp ) ? ( $ppp - ( $ppp - $count_stickies ) ) : $ppp;

    if (!$q->is_paged()) {
      $q->set('posts_per_page', ( $ppp - $offset ));
    } else {
      $offset = ( ($q->query_vars['paged']-1) * $ppp ) - $offset;
      $q->set('posts_per_page',$ppp);
      $q->set('offset',$offset);
    }
  }
}

add_filter( 'found_posts', 'rffw_ignore_sticky_found', 10, 2 );
function rffw_ignore_sticky_found( $found_posts, $q ) {
  if( $q->is_main_query() && $q->is_home() ) {

    $count_stickies = count( get_option( 'sticky_posts' ) );
    $ppp = get_option( 'posts_per_page' );
    $offset = ( $count_stickies <= $ppp ) ? ( $ppp - ( $ppp - $count_stickies ) ) : $ppp;

    $found_posts = $found_posts + $offset;
  }
  return $found_posts;
};



define('RFFW_SCSS_ALWAYS_RECOMPILE', true);
//add_action ('redux/options/'.RFFW_SLUG.'/saved', 'rffw_scss_compile');

// Load settings arrays
require_once(get_template_directory().'/admin/rffw-post-types-extras.php');
require_once(get_template_directory().'/admin/rffw-sections.php');
require_once(get_template_directory().'/admin/rffw-fields.php');
require_once(get_template_directory().'/admin/rffw-framework/rffw-customfields.php');

// functions/loader.php
require_once(get_template_directory().'/functions/loader.php');


// load blocks
require_once(get_template_directory().'/blocks/index.php');
