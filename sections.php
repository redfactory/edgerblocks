<div id="sections">

<?php
// Get sections ID's in order and the content position
if(isset($rffw_post_meta['sections']) && !empty($rffw_post_meta['sections'])){

    // Build array with sections on this page
    $sections_array = json_decode($rffw_post_meta['sections']);

    // Add thickbox for iframe popup links
    if (current_user_can('edit_posts')) add_thickbox();

    // Loop sections including page content
    foreach ($sections_array as $section_object){

      // skip hidden sections
      if($section_object->visible){

        // filter custom sections
        if (is_numeric($section_object->id)){
          // Load section by ID
          include(get_template_directory().'/section.php');
        }
        else{
          // if page has index load in no content
          if( $section_object->id == 'content' && rffw_is_index() ){
             get_template_part( '/sections/index', get_post_type() );
          // load default page section
        }elseif( $section_object->id == 'content' && get_page_template_slug() == 'page-event.php' ){
            //Don't show content section on event page
          }else{
            get_template_part( '/sections/'.$section_object->id, get_post_type() );
          }
        }
      }
    }
 }
 else{

   // Get the default sections by post type
   if(isset($rffw_post_types[get_post_type()]['default-sections'])){
     $default_sections =  $rffw_post_types[get_post_type()]['default-sections'];
   }
   else{
     // if no section where set for this posttype
     $default_sections = array(
       'header' 	=> __('The page header', 'edgerblocks'),
       'content' 	=> __('The page content', 'edgerblocks'),
     );

     // don't show default content on 404
     if ( is_404() ) unset($default_sections['content']);
   }

    // Load all default sections for this post type
    foreach ($default_sections as $section_type => $section_name) {

        // if page has index load it instead of content
        if( $section_type == 'content' && rffw_is_index()){
          if(is_search()){
            // Show Load sections for post type
            get_template_part( '/sections/index', 'search' );
          }
          else{
            get_template_part( '/sections/index', get_post_type() );
          }

        }
        else{
          if(is_search()){
            // Show Load sections for post type
            get_template_part( '/sections/'.$section_type, 'search' );
          }
          else{
            // Show Load sections for post type
            get_template_part( '/sections/'.$section_type, get_post_type() );
          }

         }
    }

    // Show 404 section
    if ( is_404() ) get_template_part( '/sections/content', '404' );
 }

 // Load footer section if set in theme settings
 if(rffw_is_theme('featured_section_footer')) rffw_load_section(rffw_get_theme('featured_section_footer'));

?>

</div>
