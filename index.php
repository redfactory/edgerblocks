<?php

global $wp_query;
$index_page_ids = get_page_for_index_ids(false);


if(is_home() && get_option( 'page_for_posts' )){
  $index_page_post = get_post(get_option( 'page_for_posts' ));

  // add meta to post object
  $index_page_post = rffw_add_meta_to_post($index_page_post);

  // Get all customfields in array
  $rffw_post_meta = rffw_get_post_custom_single(get_the_ID());

  // Build ['featured_image_url'] & ['inline_style'] etc
  $rffw_post_meta = rffw_build_style($rffw_post_meta, true);


  // Set sidebar on/off based on post setting or else theme settings
  $rffw_sidebar = (rffw_is_meta('sidebar', $index_page_post))? rffw_get_meta('sidebar', $index_page_post) : rffw_get_theme('index-sidebar');
}
elseif( is_post_type_archive() && isset($index_page_ids[get_post_type()])){


  $index_page_post = get_post($index_page_ids[get_post_type()]);

  // add meta to post object
  $index_page_post = rffw_add_meta_to_post($index_page_post);

  // Get all customfields in array
  $rffw_post_meta = rffw_get_post_custom_single(get_the_ID());

  // Build ['featured_image_url'] & ['inline_style'] etc
  $rffw_post_meta = rffw_build_style($rffw_post_meta, true);

  $rffw_sidebar = rffw_get_theme('index-sidebar');
}
else{
  $rffw_sidebar = rffw_get_theme('index-sidebar');
}


get_header();

require_once(get_template_directory().'/sections.php');

get_footer();
