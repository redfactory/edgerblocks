<?php
// Replace ID if translation exists (WPML)
if (function_exists('icl_object_id')){
	$section_object->id = icl_object_id($section_object->id, 'section', true, ICL_LANGUAGE_CODE);
}
elseif (has_filter('wpml_object_id')){
	$section_object->id = apply_filters( 'wpml_object_id', $section_object->id, 'section', true);
}

// Get section
$rffw_section = get_post($section_object->id);

if(is_object($rffw_section)){
	if($rffw_section->post_status != 'trash'){

		// force the rffw_add_meta_to_post filter to the section object
		$rffw_section = rffw_add_meta_to_post($rffw_section);

		// Add section & subsection info to section object
		$rffw_section->visible 	= $section_object->visible;
		$rffw_section->children = $section_object->children;
		$rffw_section->real 		= true;

		// Get all customfields in array
		$rffw_section_meta = rffw_get_post_custom_single($rffw_section->ID);

		// Build ['featured_image_url'] & ['inline_style']
		$rffw_section_meta = rffw_build_style($rffw_section_meta);


		// Set id_attribute with section id or if set section id meta
		$rffw_section->id_attribute = (rffw_is_meta('section-id', $rffw_section))? 'section-'.rffw_get_meta('section-id', $rffw_section) : 'section-'.$rffw_section->ID;

		// include section {sectiontype}.php
		if(file_exists(get_template_directory().'/sections/'.sanitize_title($rffw_section_meta['sectiontype']).'.php')){
			include(get_template_directory().'/sections/'.sanitize_title($rffw_section_meta['sectiontype']).'.php');
		}
		else{
			rffw_error('Section type "'.sanitize_title($rffw_section_meta['sectiontype']).'" template file is not found.');
			rffw_edit_section($rffw_section->ID);
		}
	}
}

unset($rffw_section);
