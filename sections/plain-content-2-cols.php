
<section id="<?php echo $rffw_section->id_attribute; ?>" style="<?php rffw_the_meta('inline_style', $rffw_section); ?>" class="plain-content <?php rffw_the_meta('text-style', $rffw_section); ?> <?php rffw_the_meta('section-class', $rffw_section); ?> <?php if(rffw_is_meta('section-parallax', $rffw_section)) echo 'parallax'; ?>">
	<?php rffw_top_section_border($rffw_section); ?>

	<div class="container <?php if(rffw_is_meta('full-width', $rffw_section)) echo 'full-width'; ?>">
  <?php if(rffw_is_meta('show-title', $rffw_section)): ?>
      <h2 class="section-title subtitle">
        <?php echo apply_filters('the_title', $rffw_section->post_title); ?>
      </h2>
  <?php endif; ?>

	<div class="row">
		<div class="col-sm-6">
		  <div class="post-content body">
	      <?php echo apply_filters('the_content', $rffw_section->post_content); ?>
	    </div>
		</div>
		<div class="col-sm-6">
			<div class="post-content body">
				<?php if(rffw_is_meta('second-column', $rffw_section)) : ?>
						<?php rffw_the_meta('second-column', $rffw_section); ?>
				<?php endif; ?>
			</div>
		</div>

</div>
	<?php rffw_edit_section($rffw_section->ID); ?>

  <?php rffw_bottom_section_border($rffw_section); ?>
</section>
