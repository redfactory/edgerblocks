<?php
global $wp_query, $index_page_post, $rffw_section;

// Set section if this in index page
if(isset($rffw_section) && is_object($rffw_section)){

	// Build query from subsections or section options Get Cards posts
	$rffw_cards = rffw_query_builder($rffw_section, $rffw_section_meta);
}
else{
	// create section to create index
 	$rffw_section = rffw_create_section_for_index($index_page_post);

	// use default query for cards
	$rffw_cards = $wp_query;
}
?>

<section id="<?php echo $rffw_section->id_attribute; ?>" class="grid image-grid <?php rffw_the_meta('text-style', $rffw_section); ?> <?php rffw_the_meta('section-class', $rffw_section); ?>" style="<?php rffw_the_meta('inline_style', $rffw_section); ?>">

	<?php if($rffw_section->real) rffw_top_section_border($rffw_section); ?>

	<?php if(rffw_is_meta('boxed', $rffw_section)): ?><div class="container"><?php endif; ?>

	<div id="grid-<?php echo $rffw_section->id_attribute; ?>" class="image-grid-js grid-parent <?php if(rffw_is_meta('boxed', $rffw_section)) echo 'boxed'; ?> container">
		<div class="row">

			<?php if ( $rffw_cards->have_posts() ) : ?>

			<?php $push = ($rffw_section->sidebar == 'left-sidebar')? 'col-md-push-4	col-lg-push-4	': ''; ?>
			<?php $col = ($rffw_section->sidebar == 'left-sidebar' || $rffw_section->sidebar == 'right-sidebar' )? 'col-md-8 col-lg-8 ': 'col-md-12 col-lg-12 '; ?>

			<div class="col-xs-12 col-sm-12 <?php echo $col.$push; ?>">
				<div class="row">
			<?php
			if (rffw_is_meta('grid-columns', $rffw_section)){
				switch (rffw_get_meta('grid-columns', $rffw_section)) {
				    case 2:
				      	$grid_columns = 'col-xs-12 col-sm-6 col-md-6 col-lg-6';
				        break;
				    case 3:
				      	$grid_columns = 'col-xs-12 col-sm-6 col-md-4 col-lg-4';
				        break;
				    case 6:
				      	$grid_columns = 'col-xs-6 col-sm-4 col-md-3 col-lg-2';
				        break;
				    default:
								$grid_columns = 'col-xs-12 col-sm-6 col-md-4 col-lg-3';
				}
			}
			else{
					$grid_columns = 'col-xs-12 col-sm-6 col-md-4 col-lg-3';
			}

			while ( $rffw_cards->have_posts() ) : $rffw_cards->the_post();
				$post = get_post();
				?>

				<div class="grid-item grid-columns-<?php rffw_the_meta('grid-columns', $rffw_section) ?> <?php echo $grid_columns; ?>">
					<div class="grid-item-container <?php rffw_the_meta('grid-hover', $rffw_section) ?> <?php if (rffw_is_meta('show-content', $rffw_section) || rffw_is_link($rffw_section, $post)) echo 'has-hover'; ?>" style="background-image:url('<?php rffw_the_grid_thumbnail_url($post, 'large'); ?>');">
						<div class="grid-content-overlay" style="background-color:<?php rffw_the_meta('bg-color', $rffw_section); ?>;"></div>
			    	<div class="grid-item-content">
			        	<?php if(rffw_is_meta('show-content', $rffw_section)) { ?>
			        		<h3><?php the_title(); ?></h3>

									<?php if(rffw_is_meta('show-meta', $rffw_section)){ ?>
										<?php get_template_part( 'template-parts/info', get_post_type() ); ?>
									<?php } ?>

			        	<?php } ?>

			        	<?php if(rffw_is_link($rffw_section, $post)) { ?>
			        	<a class="btn" href="<?php rffw_the_link(get_permalink(), $post); ?>">
			        		<?php (rffw_is_meta('click-through-text', $rffw_section))? esc_html_e(''.rffw_get_meta('click-through-text', $rffw_section),'edgerblocks') : esc_html_e('Read more','edgerblocks'); ?>
			        		<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
			        	</a>
			        	<?php } ?>
			    	</div>
		    	</div>
			</div>

			<?php	endwhile;	?>

			</div>

			<?php if(rffw_is_meta('pagination', $rffw_section)): ?>
			<div class="pagination">
				<?php
				if($rffw_cards->found_posts == 1){
					echo $rffw_cards->found_posts." ". esc_html__('result found.','edgerblocks');
				}
				else{
					echo $rffw_cards->found_posts." ". esc_html__('results found.','edgerblocks');
				}
				?>
				<?php
				the_posts_pagination( array(
					'mid_size'  => 5,
					'prev_text' => '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>',
					'next_text' => '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>',
				) );
				?>
			</div>
			<?php endif; ?>

		<?php else: ?>
			<?php get_template_part( 'template-parts/content', 'none' ); ?>
		<?php endif; ?>

			</div>


			<?php	if($rffw_section->sidebar == 'left-sidebar' ||$rffw_section->sidebar == 'right-sidebar' ): ?>
				<?php	$pull = ($rffw_section->sidebar == 'left-sidebar')? 'col-md-pull-8	col-lg-pull-8	': 'col-md-offset-1 col-lg-offset-1'; ?>

				<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 <?php echo $pull; ?>">
					<?php get_sidebar('index'); ?>
				</div>

			<?php endif; ?>

		</div>
	</div>

	<?php if(rffw_is_meta('boxed', $rffw_section)): ?></div><?php endif; ?>

	<?php wp_reset_postdata();  if(isset($index_page_post)) $post = $index_page_post; ?>
	<?php if($rffw_section->real) rffw_edit_section($rffw_section->ID); ?>
	<?php if($rffw_section->real) rffw_bottom_section_border($rffw_section); ?>
</section>
