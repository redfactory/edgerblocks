<?php
  $section_style = rffw_get_meta('section-plain-style', $rffw_section);
  $section_class ='';

  $section_class = $section_style;
?>

<section id="<?php echo $rffw_section->id_attribute; ?>" class="slider index image <?php rffw_the_meta('text-style', $rffw_section); ?> <?php rffw_the_meta('section-class', $rffw_section); ?>" style="<?php rffw_the_meta('inline_style', $rffw_section); ?>">
  	<?php rffw_top_section_border($rffw_section); ?>

	<?php
		// Build query from subsections or section options Get Cards posts
		$rffw_cards = rffw_query_builder($rffw_section, $rffw_section_meta);
	?>
		<div class="container">
      <?php if(rffw_is_meta('show-title', $rffw_section)): ?>
        <h2 class="section-title subtitle">
          <?php echo apply_filters('the_title', $rffw_section->post_title); ?>
        </h2>
      <?php endif; ?>
      <div class="swiper-content">
  			<div class="swiper-container" data-id="<?php echo $rffw_section->ID; ?>" id="swiper-<?php echo $rffw_section->ID; ?>">
  				<div class="swiper-wrapper">

          <?php while ( $rffw_cards->have_posts() ) : $rffw_cards->the_post(); ?>
            <?php $post = get_post(); ?>

  					<div id="slide-<?php the_ID(); ?>" class="swiper-slide slide" data-title="<?php the_title(); ?>">
              <div class="grid-item">
                <?php
                  if(rffw_is_meta('omroep-url', $post)) {
                    $link = rffw_get_meta('omroep-url', $post);
                  }else{
                    $link = get_permalink();
                  }
                ?>
                <a class="grid-item-container" href="<?php echo $link ?>" target="_blank">
                	<div class="grid-item-content" style="background-image:url(<?php rffw_the_grid_thumbnail_url($post, 'medium'); ?>);">
                	</div>
                </a>
              </div>
  					</div>

  					<?php
  					endwhile;
  					wp_reset_postdata();
  					?>
  				</div>

  				<?php //Removed from options. Can be put back if needed (might need styling)
  				if(rffw_is_meta('pagination', $rffw_section) && false): ?>
  				<div id="swiper-pagination-<?php echo $rffw_section->ID; ?>" class="swiper-pagination"></div>
  				<?php endif; ?>
  				</div>

          <?php if(rffw_is_meta('arrows', $rffw_section)): ?>
          <div class="slider-arrows">

              <div class="swiper-button-prev prev" id="swiper-prev-<?php echo $rffw_section->ID; ?>"></div>
              <div class="swiper-button-next next" id="swiper-next-<?php echo $rffw_section->ID; ?>"></div>
          </div>
          <?php endif; ?>
        </div>
			</div>




	<?php
	$slidesPerView 	= (rffw_is_meta('slides-per-view', $rffw_section))? 			rffw_get_meta('slides-per-view', $rffw_section) : 3;
	$spacebetween 	= (rffw_is_meta('slides-space-between', $rffw_section))? 	rffw_get_meta('slides-space-between', $rffw_section) : 0;
  $slidesTotal 	= (rffw_is_meta('max-amount-post', $rffw_section))? 			rffw_get_meta('max-amount-post', $rffw_section) : 10;

	$pagination_swiper_line =  (rffw_is_meta('pagination', $rffw_section)) 	? 	'pagination: \'#swiper-pagination-'.$rffw_section->ID.'\',' : '';
	$prev_next_swiper_line 	=  (rffw_is_meta('arrows', $rffw_section)) 			? 'prevButton: \'#swiper-prev-'.$rffw_section->ID.'\', nextButton: \'#swiper-next-'.$rffw_section->ID.'\',' : '';

	wp_add_inline_script( 'rffw_main', 'jQuery(function($){	"use strict";
			if(jQuery(\'#swiper-'.esc_js($rffw_section->ID).'\').length > 0){
				var swiper'.esc_js($rffw_section->ID).' = new Swiper(\'#swiper-'.esc_js($rffw_section->ID).'\', {
					'.$pagination_swiper_line.'
					'.$prev_next_swiper_line.'

					slidesPerView: '.esc_js($slidesTotal).',
					spaceBetween: '.esc_js($spacebetween).',

					paginationClickable: true,
					loop: true,
					breakpoints: {
						// when window width is <= 320px
						320: {
							slidesPerView: 1,
              spaceBetween: 5,
						},
						// when window width is <= 768px
						768: {
							slidesPerView: '.esc_js($slidesPerView).',
              spaceBetween: 5,
						},
						// when window width is <= 992px
						992: {
							slidesPerView: '.esc_js($slidesPerView).',
						}
					}
				});
			}
		});');

	?>

	<?php rffw_edit_section($rffw_section->ID); ?>
	<?php rffw_top_section_border($rffw_section); ?>
</section>
