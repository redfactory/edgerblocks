<?php
global $wp_query, $index_page_post, $rffw_section;

// Set section if this in index page
if(isset($rffw_section) && is_object($rffw_section)){
	// Build query from subsections or section options Get Cards posts
	$rffw_cards = rffw_query_builder($rffw_section, $rffw_section_meta);
}
else{
	// create section to create index
 	$rffw_section = rffw_create_section_for_index($index_page_post);

	// use default query for cards
	$rffw_cards = $wp_query;
}
?>

<section id="<?php echo $rffw_section->id_attribute; ?>" class="list <?php rffw_the_meta('text-style', $rffw_section); ?> <?php echo is_search() ? 'search-results' : rffw_the_meta('section-class', $rffw_section); ?>" style="<?php rffw_the_meta('inline_style', $rffw_section); ?>">

	<?php if($rffw_section->real) rffw_top_section_border($rffw_section); ?>
	<div class="container">

	<div class="row">
		<div class="col-xs-12 col-sm-12 <?php echo $col.$push; ?>">
			<div id="list-<?php echo $rffw_section->id_attribute; ?>" class="list-parent">

				<?php if ( $rffw_cards->have_posts() ) : ?>

					<?php
					while ( $rffw_cards->have_posts() ) : $rffw_cards->the_post();
			        $post = get_post();

						$grid_img_url = '';
						if(rffw_is_meta('show-image', $rffw_section)) {
							if (rffw_is_meta('overview-img', $post)) {
								$grid_img_url = wp_get_attachment_image_url( rffw_get_meta('overview-img', $post), 'medium_large' );
							} else {
								$grid_img_url = rffw_get_meta('featured_image_url', $post);
								if (empty($grid_img_url)) {
									$image_src = wp_get_attachment_image_src( get_post_thumbnail_id(),'index' );
									$grid_img_url = $image_src[0];
									if (empty($image_src)) {
										$grid_img_url = '';
									}
								}

							}
						}
						?>

						<div class="list-item">
							<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
								<?php if (isset($grid_img_url) && $grid_img_url != '') { ?>
								<div class="featured-image" style="background-image:url('<?php echo $grid_img_url; ?>');"></div>
								<?php } ?>

								<div class="post-content">
									<?php if (rffw_is_meta('show-content', $rffw_section)) { ?>
										<?php if (rffw_is_link($rffw_section, $post)) {
											the_title( sprintf( '<h3 class="post-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' );
										} else { ?>
											<h3 class="post-title"><?php the_title(); ?></h3>
										<?php } ?>
										<?php
											$posttype = get_post_type();
											if($posttype == 'post'){
												$posttype = 'nieuws';
											}elseif($posttype == 'page'){
												$posttype = 'pagina';
											}
										?>
										<?php if(is_search() && isset($posttype)) : ?>
											<div class="meta"><?php echo $posttype; ?></div>
										<?php endif; ?>

										<?php the_excerpt(''); ?>

										<?php if (rffw_is_link($rffw_section, $post)) { ?>
										<a class="btn" href="<?php rffw_the_link(get_permalink(), $post); ?>">
											<?php (rffw_is_meta('click-through-text', $rffw_section))? esc_html_e(''.rffw_get_meta('click-through-text', $rffw_section),'edgerblocks') : esc_html_e('Lees meer','edgerblocks'); ?>
											<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
										</a>
										<?php } ?>

										<?php if(rffw_is_meta('show-meta', $rffw_section)){ ?>
											<?php get_template_part( 'template-parts/info', get_post_type() ); ?>
										<?php } ?>

									<?php } ?>
								</div>
							</article>
						</div>

					<?php endwhile; ?>

					<?php if(rffw_is_meta('pagination', $rffw_section)): ?>
					<div class="pagination">
						<?php
						/*
						if($rffw_cards->found_posts == 1){
							echo $rffw_cards->found_posts." ". esc_html__('result found.','edgerblocks');
						}
						else{
							echo $rffw_cards->found_posts." ". esc_html__('results found.','edgerblocks');
						}
						*/

						the_posts_pagination( array(
							'mid_size'  => 5,
							'prev_text' => '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>',
							'next_text' => '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>',
						) );
						?>
					</div>
					<?php endif; ?>

				<?php else: ?>
					<?php get_template_part( 'template-parts/content', 'none' ); ?>
				<?php endif; ?>

			</div>
		</div>
	</div>
</div>
	<?php wp_reset_postdata();  if(isset($index_page_post)) $post = $index_page_post; ?>
	<?php if($rffw_section->real) rffw_edit_section($rffw_section->ID); ?>
	<?php if($rffw_section->real) rffw_bottom_section_border($rffw_section); ?>
</section>
