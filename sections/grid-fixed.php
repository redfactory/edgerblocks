<?php
global $wp_query, $index_page_post, $rffw_section;

// Set section if this in index page
if(isset($rffw_section) && is_object($rffw_section)){

	// Build query from subsections or section options Get Cards posts
	$rffw_cards = rffw_query_builder($rffw_section, $rffw_section_meta);
}
else{
	// create section to create index
 	$rffw_section = rffw_create_section_for_index($index_page_post);

	// use default query for cards
	$rffw_cards = $wp_query;
}

$post_type = rffw_get_meta('query_post_type', $rffw_section);

?>

<section id="<?php echo $rffw_section->id_attribute; ?>" class="grid fixed-grid <?php rffw_the_meta('text-style', $rffw_section); ?> <?php rffw_the_meta('section-class', $rffw_section); ?>" style="<?php rffw_the_meta('inline_style', $rffw_section); ?>">

	<?php if($rffw_section->real) rffw_top_section_border($rffw_section); ?>

	<div class="container">
	<div id="grid-<?php echo $rffw_section->id_attribute; ?>" class="fixed-grid-js grid-parent">
		<div class="row">
			<?php if ( $rffw_cards->have_posts() ) : ?>
			<?php
			if (rffw_is_meta('grid-columns', $rffw_section)){
				switch (rffw_get_meta('grid-columns', $rffw_section)) {
				    case 2:
				      	$grid_columns = 'col-xs-12 col-sm-6 col-md-6 col-lg-6';
				        break;
				    case 3:
				      	$grid_columns = 'col-xs-12 col-sm-6 col-md-4 col-lg-4';
				        break;
				    case 6:
				      	$grid_columns = 'col-xs-6 col-sm-4 col-md-3 col-lg-2';
				        break;
						case 4:
				    default:
								$grid_columns = 'col-xs-12 col-sm-6 col-md-3 col-lg-3';
				}
				$cols = rffw_get_meta('grid-columns', $rffw_section);
			}
			else{
				$grid_columns = 'col-xs-12 col-sm-6 col-md-4 col-lg-4';
				$cols = 3;
			}

			$last_col_centered = false;
			$amount = $rffw_cards->post_count;
			$divide = round($amount / $cols);
			$rest = $amount - ($cols * $divide);
			if($rest == 1){
				$last_col_centered = true;
			}

			global $rffw_section;
			if (rffw_is_meta('showdate', $rffw_section)){
				$show_date = rffw_get_meta('showdate', $rffw_section);
			}elseif(is_home()){
				$show_date = true;
			}else{
				$show_date = false;
			}

			$c = 0;
			while ( $rffw_cards->have_posts() ) : $rffw_cards->the_post();
				$post = get_post();
				$c++;
				?>

				<div class="grid-item grid-columns-<?php rffw_the_meta('grid-columns', $rffw_section) ?> <?php echo $grid_columns; ?> <?php echo !empty($post_type) ? 'grid-item-'.$post_type : ''; ?> <?php echo ($last_col_centered && $c == $amount) ? 'col-centered' : ''; ?> ">
					<?php if(!empty($post_type)) : ?>
						<?php include(get_template_directory().'/template-parts/card-'.$post_type.'.php'); ?>
					<?php else : ?>
						<?php include(get_template_directory().'/template-parts/card-post.php'); ?>
					<?php endif; ?>
				</div>

			<?php	endwhile;	?>

			</div>
			<?php if(rffw_is_meta('showreadmore', $rffw_section)): ?>
					<div class="read-more">
						<?php
							$post_type = get_post_type();
							$link = get_post_type_archive_link( $post_type );
							$link_text= 'Lees meer';
							if($post_type == 'post'){
								$link_text = 'Lees meer nieuws';
							}
						?>
						<a href="<?php echo $link; ?>" class="btn"><?php _e($link_text, 'edgerblocks'); ?></a>
					</div>
			<?php endif;?>
			<?php if(rffw_is_meta('pagination', $rffw_section) || is_home()): ?>
				<div class="pagination">
					<?php
					/*
					if($rffw_cards->found_posts == 1){
						echo $rffw_cards->found_posts." ". esc_html__('result found.','edgerblocks');
					}
					else{
						echo $rffw_cards->found_posts." ". esc_html__('results found.','edgerblocks');
					}
					*/
					?>
					<?php
					the_posts_pagination( array(
						'mid_size'  => 5,
						'prev_text' => '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>',
						'next_text' => '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>',
					) );
					?>
				</div>
		<?php endif; ?>

		<?php else: ?>
			<?php get_template_part( 'template-parts/content', 'none' ); ?>
		<?php endif; ?>
	</div>

</div>
	<?php wp_reset_postdata(); ?>
	<?php if($rffw_section->real) rffw_edit_section($rffw_section->ID); ?>
	<?php if($rffw_section->real) rffw_bottom_section_border($rffw_section); ?>
</section>
