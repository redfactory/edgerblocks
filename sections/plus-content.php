<section id="<?php echo $rffw_section->id_attribute; ?>" style="<?php rffw_the_meta('inline_style_no_image', $rffw_section); ?>" class="plus-content <?php rffw_the_meta('section-class', $rffw_section); ?>">
	<?php rffw_top_section_border($rffw_section); ?>

  <?php $push = (rffw_get_meta('media-position', $rffw_section) == 'right')? 'col-sm-push-6	': ''; ?>
  <?php	$pull = (rffw_get_meta('media-position', $rffw_section) == 'right')? 'col-sm-pull-6	': ''; ?>

	<?php if(!rffw_is_meta('full-width', $rffw_section)): ?>
	  <div class="container">
	<?php endif; ?>

    <div class="row">

			<div class="col-xs-12 col-sm-6 <?php echo $push; ?>">
					<div class="feat_image align-center">
						<?php echo get_the_post_thumbnail($rffw_section->ID); ?>
					</div>
			</div>

      <div class="col-xs-12 col-sm-6 <?php echo $pull; ?>">
        <div class="post-content body">

          <?php if(rffw_is_meta('show-title', $rffw_section) ): ?>
            <h2 class="section-title subtitle">
              <?php echo apply_filters('the_title', $rffw_section->post_title); ?>
            </h2>
      	  <?php endif; ?>

          <?php echo apply_filters('the_content', $rffw_section->post_content); ?>
        </div>
      </div>

    </div>

		<?php if(!rffw_is_meta('full-width', $rffw_section)): ?>
		  </div>
		<?php endif; ?>

	<?php rffw_edit_section($rffw_section->ID); ?>

  <?php rffw_bottom_section_border($rffw_section); ?>
</section>
