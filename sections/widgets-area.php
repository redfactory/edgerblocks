<section id="<?php echo $rffw_section->id_attribute; ?>" style="<?php rffw_the_meta('inline_style', $rffw_section); ?>" class="widgets-area <?php rffw_the_meta('text-style', $rffw_section); ?>  <?php rffw_the_meta('section-class', $rffw_section); ?>">
	<?php rffw_top_section_border($rffw_section); ?>

		<?php if(rffw_is_meta('section-parallax', $rffw_section)): ?>
	    <div class="section-background-container">
	      <div class="section-background" style="<?php rffw_the_meta('inline_style', $rffw_section); ?>"></div>
	    </div>
	  <?php endif; ?>

		<?php if(rffw_is_meta('show-title', $rffw_section)): ?>
	    <div class="container">
	      <h2 class="section-title subtitle">
	        <?php echo apply_filters('the_title', $rffw_section->post_title); ?>
	      </h2>
	    </div>
	  <?php endif; ?>

	<div class="container">
		<div class="row">

			<?php
			if (is_active_sidebar(rffw_get_meta('widgets-area', $rffw_section))){
				rffw_the_widget_builder(rffw_get_meta('widgets-area', $rffw_section));
			}
			?>

		</div>
	</div>

	<?php rffw_edit_section($rffw_section->ID); ?>

	<?php rffw_bottom_section_border($rffw_section); ?>
</section>
