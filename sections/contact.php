<?php
	$style_bg = "background-image: url('" . $rffw_section->rffw_meta['featured_image_url'] . "'); ";

	$show_img_in_section_bg = true;
	$section_style = rffw_get_meta('section-plain-style', $rffw_section);
	if($section_style == 'image-left' || $section_style == 'image-right' || $section_style == 'big-header') {
		$show_img_in_section_bg = false;
	}
?>

<section id="<?php echo $rffw_section->id_attribute; ?>" style="<?php rffw_the_meta('inline_style', $rffw_section); ?> <?php echo $show_img_in_section_bg ? $style_bg : ''; ?>" class="contact" <?php rffw_the_meta('text-style', $rffw_section); ?> <?php rffw_the_meta('section-class', $rffw_section); ?> <?php if(rffw_is_meta('section-parallax', $rffw_section)) echo 'parallax'; ?>">
	<?php rffw_top_section_border($rffw_section); ?>

	<div class="container">
		<?php if(rffw_is_meta('show-title', $rffw_section)): ?>
			<h2 class="section-title subtitle">
				<?php echo apply_filters('the_title', $rffw_section->post_title); ?>
			</h2>
		<?php endif; ?>

		<div class="row">
			<div class="col-md-8">
				<div class="post-content body">
					<?php if(rffw_is_meta('contact-form', $rffw_section)): ?>
						<?php
							$form_id = rffw_get_meta('contact-form', $rffw_section);
							echo do_shortcode ('[gravityform id="'.$form_id.'" title="false" description="false" ajax="true"]');
						?>
					<?php endif; ?>
				</div>
			</div>
			<div class="col-md-4">
				<div class="post-content body">
					<?php echo apply_filters('the_content', $rffw_section->post_content); ?>
				</div>
			</div>
		</div>
</div>
	<?php rffw_edit_section($rffw_section->ID); ?>

  <?php rffw_bottom_section_border($rffw_section); ?>
</section>
