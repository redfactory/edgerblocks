<?php

global $post, $index_page_post;

$page_event = false;
if(is_page_template('page-event.php')){
	$page_event = true;
}

if(rffw_get_meta('header-background-image', $post)) {
	$header_background_url = wp_get_attachment_image_url(rffw_get_meta('header-background-image', $post), 'fullwidth');
}

if($page_event){
	$header_background_url = wp_get_attachment_image_url(get_post_thumbnail_id(), 'fullwidth');
	$accent_color = rffw_get_meta('accent-color', $post);
	$accent_color_rgb = rffw_hex2rgb($accent_color);
}

if(isset($accent_color)){
	$bg_color = $accent_color;
}elseif(rffw_is_meta('header-bg-color', $post)){
	$bg_color = rffw_get_meta('header-bg-color', $post);
}

if (isset($index_page_post) && is_object($index_page_post))  $post = $index_page_post;
?>
<?php if($page_event) : ?>
	<style>
	.page-header::before {
		<?php if (isset($header_background_url) && !empty($header_background_url)){ ?>background-image:url(<?php echo $header_background_url;  ?>);<?php } ?>
	}
	<?php if(isset($accent_color)) : ?>
		.btn:not(.btn-primary):not(.btn-success):not(.btn-info):not(.btn-danger):not(.btn-link) {
				background-color:<?php echo $accent_color; ?>;
		}
	<?php endif; ?>
	</style>
<?php endif; ?>
<section id="page-header-<?php the_ID(); ?>"
	class="page-header <?php echo $page_event ? 'page-header-event' : ''; ?> <?php if (rffw_is_meta('text-style-header', $post)) rffw_the_meta('text-style-header', $post); else rffw_the_theme('header-text-style'); ?> <?php if(rffw_is_meta('featured_image_url', $post)) echo rffw_get_theme('header-parallax') . '-wrapper'; ?>"
	style="<?php echo isset($bg_color) ? 'background-color:'.$bg_color.';' : ''; ?>">

	<?php if( rffw_is_meta('featured_image_url', $post) && rffw_get_theme('header-parallax') == 'parallax' ) { ?>

		<div class="parallax-window" data-parallax="scroll" data-image-src="<?php rffw_the_meta('featured_image_url', $post); ?>" data-speed="0.4" data-iosFix="true"></div>

	<?php } elseif( rffw_is_meta('featured_image_url', $post) && rffw_get_theme('header-parallax') == 'parallax-fixed' ) { ?>

		<div class="parallax-window" data-parallax="scroll" data-image-src="<?php rffw_the_meta('featured_image_url', $post); ?>" data-speed="0.0" data-iosFix="true"></div>

	<?php } ?>

	<div class="container <?php echo $page_event ? '' : 'wide';?>">
		<div class="row <?php echo $page_event ? 'row-eq-height' : '';?>">
			<div class="col-xs-12 <?php echo $page_event ? 'col-sm-6' : 'col-sm-7';?>">
				<?php if(!$page_event) : ?>
					<div class="breadcrumbs">
						<?php if(rffw_get_theme('header-breadcrumb')){ echo the_rffw_breadcrumbs(); } ?>
					</div>
				<?php endif; ?>
				<div class="header-title">
		 			<?php echo get_rffw_complete_title($post, $index_page_post); ?>
				</div>
 			</div>

 			<div class="col-xs-12 <?php echo $page_event ? 'col-sm-6' : 'col-sm-5';?>">
 				<div class="header-content">
					<?php
					if($page_event){
						the_content();
					}elseif(is_single() || is_singular()){
						echo '<p>'. strip_tags(get_the_excerpt()) . '</p>';
					}	else if(is_category()){
						echo category_description();
					}
					?>
 				</div>
 			</div>
 		</div>
	</div>

	<?php if(!rffw_is_index()) rffw_edit_post(); ?>

	<?php rffw_bottom_section_border($post); ?>
	<?php if($page_event) : ?>
		</div>
	<?php endif; ?>
</section>

<?php
	$custom_menu = rffw_get_meta('custom-menu', $post);
?>
<?php if($page_event && isset($custom_menu)) : ?>
	<section id="fixed-submenu" class="event-menu">
		<div class="container">
			<div class="submenu-mobile">
				<a data-toggle="collapse" data-target="#anchor-links-wrapper" id="anchor-button" class="visible-xs anchor-chevron collapsed"> <span class="first-menu-item"></span> <i class="fa fa-angle-down"> </i> </a>
			</div>
			<div id="anchor-links-wrapper" class="anchor-links-wrapper collapse">
				<div id="anchor-menu">
					<a data-toggle="collapse" data-target="#anchor-links-wrapper" id="anchor-close-button" class="anchor-chevron"> <i class="fa fa-angle-up"> </i> </a>
					<?php
					wp_nav_menu(array(
						'menu' => $custom_menu,
						'fallback_cb'    => 'rffw_emptymenu'
					));
					?>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>
