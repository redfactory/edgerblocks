<?php
$page_event = false;
if(is_page_template('page-event.php')){
	$page_event = true;
}

if($page_event){
	$accent_color = rffw_get_meta('accent-color', $post);
}
?>

<section id="<?php echo $rffw_section->id_attribute; ?>" style="<?php rffw_the_meta('inline_style_no_image', $rffw_section); ?>" class="nominaties event<?php rffw_the_meta('section-class', $rffw_section); ?>">
	<?php if($accent_color) : ?>
 		<style>
			section.event .js-tabs .navbar-tabs .navbar-collapse ul.tabs li:hover a,
			section.event .js-tabs .navbar-tabs .navbar-collapse ul.tabs li.ui-state-active a {
				background-color: <?php echo $accent_color; ?>;
			}
		</style>
	<?php endif; ?>
	<?php rffw_top_section_border($rffw_section); ?>

	<?php if(!rffw_is_meta('full-width', $rffw_section)): ?>
	  <div class="container">
	<?php endif; ?>

    <div class="row">

      <div class="col-xs-12">
        <div class="nominaties-content body">
						<div class="js-tabs">
							<div class="navbar-tabs">
								<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#tabs-navbar-<?php echo $event_dag_term->term_id; ?>" aria-expanded="false">
									<span><?php _e('Kies tijd', 'edgerblocks'); ?></span> <i class="fa fa-angle-down"> </i>
							 </button>
							 <div class="navbar-collapse collapse" id="tabs-navbar-<?php echo $event_dag_term->term_id; ?>">
								 <ul class="tabs">
								<?php
									$all_nominaties = new WP_Query( array(
									'post_type' => 'nominatie',
									'fields ' 	=> 'ids',
									'posts_per_page' => -1
									));
									$array_post_ids = wp_list_pluck( $all_nominaties->posts, 'ID' );

									$terms = wp_get_object_terms( $array_post_ids,  'cat-nominatie', array(
										'orderby'    => 'order',
										'order'      => 'ASC',
										'hide_empty' => true,
									));

									if ( !empty($terms) && !is_wp_error($terms)) {
											foreach( $terms as $term ) { ?>
												<li><a class="term_select" href="#nominatie-<?php echo $term->slug ; ?>"><?php echo $term->name; ?></a></li>
										 <?php }
									}
								?>
							</ul>
						</div>
				 </div>
						<?php
						if ( !empty($terms) && !is_wp_error($terms)) {
								foreach( $terms as $term ) { ?>

											<div id="nominatie-<?php echo $term->slug; ?>" class="tab" >
												<div class="js-accordion">

													<?php
													$nominaties = new WP_Query( array(
													'post_type' => 'nominatie',
													'posts_per_page' => -1,
													'tax_query' => array(
															array(
															'taxonomy' => 'cat-nominatie',
															'field' => 'term_id',
															'terms' => $term->term_id
															 )
														)
													));
													?>
													<?php if ( $nominaties->have_posts() ) : ?>
														<?php while ( $nominaties->have_posts() ) : $nominaties->the_post(); ?>
															<h4 class="accordion-header"><?php the_title(); ?></h4>
															<div class="content">
																<?php echo apply_filters('the_content', $post->post_content); ?>
															</div>

											<?php endwhile; ?>

											<?php wp_reset_postdata(); ?>

										<?php else : ?>
											<p><?php esc_html_e( 'Sorry, geen nominaties gevonden.' ); ?></p>
										<?php endif; ?>
									</div>
				        </div>
							 <?php }
						}
						?>

      </div>

    </div>

		<?php if(!rffw_is_meta('full-width', $rffw_section)): ?>
		  </div>
		<?php endif; ?>

	<?php rffw_edit_section($rffw_section->ID); ?>

  <?php rffw_bottom_section_border($rffw_section); ?>
</section>
