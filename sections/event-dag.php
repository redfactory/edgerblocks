<?php
$page_event = false;
if(is_page_template('page-event.php')){
	$page_event = true;
}

if($page_event){
	$accent_color = rffw_get_meta('accent-color', $post);
}
?>

<section id="<?php echo $rffw_section->id_attribute; ?>" style="<?php rffw_the_meta('inline_style_no_image', $rffw_section); ?>" class="event-dag event <?php rffw_the_meta('section-class', $rffw_section); ?>">
	<?php rffw_top_section_border($rffw_section); ?>

	<?php if(!rffw_is_meta('full-width', $rffw_section)): ?>
	  <div class="container">
	<?php endif; ?>

    <div class="row">
			<?php $event_dag_term = get_term_by('slug', rffw_get_meta('taxonomy-event-dag', $rffw_section), 'event-dag');?>

      <div class="col-xs-12">
        <div class="event-dag-content body">
            <h3 class="event-subtitle"><?php echo $event_dag_term->name ?></h3>

						<div class="js-tabs">
							<div class="navbar-tabs">
								<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#tabs-navbar-<?php echo $event_dag_term->term_id; ?>" aria-expanded="false">
									<span><?php _e('Kies tijd', 'edgerblocks'); ?></span> <i class="fa fa-angle-down"> </i>
							 </button>
								<div class="navbar-collapse collapse" id="tabs-navbar-<?php echo $event_dag_term->term_id; ?>">
									<ul class="tabs">
									<?php
										$all_programma_datas = new WP_Query( array(
										'post_type' => 'programma-data',
										'fields ' 	=> 'ids',
										'posts_per_page' => -1,
										'tax_query' => array(
										    array(
										    'taxonomy' => 'event-dag',
										    'field' => 'term_id',
										    'terms' => $event_dag_term->term_id
										     )
										  )
										));
										$array_post_ids = wp_list_pluck( $all_programma_datas->posts, 'ID' );

										$terms = wp_get_object_terms( $array_post_ids,  'tijd', array(
											'orderby'    => 'order',
											'order'      => 'ASC',
											'hide_empty' => true,
										));

										if ( !empty($terms) && !is_wp_error($terms)) {
											foreach( $terms as $term ) { ?>
												<li><a class="term_select" href="#tijd-<?php echo $term->slug ; ?>"><?php echo $term->name; ?></a></li>
											<?php }
										}
									?>
								</ul>
							</div>
					 </div>

						<?php
						if ( !empty($terms) && !is_wp_error($terms)) {
								foreach( $terms as $term ) { ?>

									<div id="tijd-<?php echo $term->slug; ?>" class="tab">
										<div class="js-accordion">

											<?php
												$programma_datas = new WP_Query( array(
												'post_type' => 'programma-data',
												'fields ' 	=> 'ID',
												'posts_per_page' => -1,
												'tax_query' => array(
													'relation' => 'AND',
												    array(
												    'taxonomy' => 'event-dag',
												    'field' => 'term_id',
												    'terms' => $event_dag_term->term_id
														),
														array(
														'taxonomy' => 'tijd',
														'field' => 'term_id',
														'terms' => $term->term_id
														 )
												  )
												));
											 if ( $programma_datas->have_posts() ) : ?>
												<?php while ( $programma_datas->have_posts() ) : $programma_datas->the_post(); ?>

													<h4 class="accordion-header"><span class="titel"><?php the_title(); ?></span>
														<span class="tijd"><?php rffw_the_meta('programma-data-tijd', $post); ?></span>
														<span class="locatie" <?php echo isset($accent_color) ? 'style="background-color:'.$accent_color.'"' : ''; ?>><?php	$locaties = get_the_terms( $post->ID, 'locatie' );
																echo $locaties[0]->name;
															?></span>
													</h4>

													<div class="content">
														<?php echo apply_filters('the_content', $post->post_content); ?>
													</div>

												<?php endwhile; ?>

												<?php wp_reset_postdata(); ?>

											<?php else : ?>
												<p><?php esc_html_e( 'Sorry, geen programma data gevonden voor deze dag.' ); ?></p>
											<?php endif; ?>


											</div>
										</div>


									<?php }
							}
							?>
					</div>

        </div>
      </div>

    </div>

		<?php if(!rffw_is_meta('full-width', $rffw_section)): ?>
		  </div>
		<?php endif; ?>

	<?php rffw_edit_section($rffw_section->ID); ?>

  <?php rffw_bottom_section_border($rffw_section); ?>
</section>
