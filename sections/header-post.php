<?php

global $post, $index_page_post;

?>

<?php if(is_single()) : ?>
	<section id="page-header-<?php the_ID(); ?>"class="page-header-post">
		<div class="container">
			<div class="breadcrumbs">
				<?php if(rffw_get_theme('header-breadcrumb')){ echo the_rffw_breadcrumbs(); } ?>
			</div>
		</div>
		<?php if(!rffw_is_index()) rffw_edit_post(); ?>
	</section>
<?php else : ?>

<?php
	if(rffw_get_meta('header-background-image', $post)) {
		$header_background_url = wp_get_attachment_image_url(rffw_get_meta('header-background-image', $post), 'fullwidth');
	}

	if (isset($index_page_post) && is_object($index_page_post))  $post = $index_page_post;
	?>

	<section id="page-header-<?php the_ID(); ?>"
		class="page-header <?php if (rffw_is_meta('text-style-header', $post)) rffw_the_meta('text-style-header', $post); else rffw_the_theme('header-text-style'); ?> <?php if(rffw_is_meta('featured_image_url', $post)) echo rffw_get_theme('header-parallax') . '-wrapper'; ?>"
		style="<?php if (rffw_is_meta('header-bg-color', $post)){ ?>background-color:<?php rffw_the_meta('header-bg-color', $post); ?>; <?php } ?><?php if (isset($header_background_url) && !empty($header_background_url)){ ?>background-image:url(<?php echo $header_background_url;  ?>);<?php } ?>">

		<?php if( rffw_is_meta('featured_image_url', $post) && rffw_get_theme('header-parallax') == 'parallax' ) { ?>

			<div class="parallax-window" data-parallax="scroll" data-image-src="<?php rffw_the_meta('featured_image_url', $post); ?>" data-speed="0.4" data-iosFix="true"></div>

		<?php } elseif( rffw_is_meta('featured_image_url', $post) && rffw_get_theme('header-parallax') == 'parallax-fixed' ) { ?>

			<div class="parallax-window" data-parallax="scroll" data-image-src="<?php rffw_the_meta('featured_image_url', $post); ?>" data-speed="0.0" data-iosFix="true"></div>

		<?php } ?>

		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-7">
						<div class="breadcrumbs">
							<?php if(rffw_get_theme('header-breadcrumb')){ echo the_rffw_breadcrumbs(); } ?>
						</div>
					<div class="header-title">
			 			<?php echo get_rffw_complete_title($post, $index_page_post); ?>
					</div>
	 			</div>

	 			<div class="col-xs-12 col-sm-5">
	 				<div class="header-content">
	 				</div>
	 			</div>
	 		</div>
		</div>

		<?php rffw_bottom_section_border($post); ?>
	</section>

<?php endif; ?>
