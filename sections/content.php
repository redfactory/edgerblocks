<?php
global $post;
$section_class = '';

?>

<section id="page-content-<?php the_ID(); ?>" <?php post_class('page-content '.$section_class.' '.rffw_get_meta('text-style', $post));?> style="<?php rffw_the_meta('inline_style_no_image', $post); ?>" >
	<div class="container page-content-container">
				<div class="post-content">

					<?php echo apply_filters('the_content', $post->post_content); ?>

				</div>
	</div>

	<?php rffw_edit_post(); ?>

</section>
