<?php
global $cookie_preference, $cookie_statistics, $cookie_marketing;
require_once(get_template_directory().'/class/TwitterTextFormatter.php');
use Netgloo\TwitterTextFormatter;

if ($cookie_statistics == true && $cookie_marketing == true ) {
?>

<section id="<?php echo $rffw_section->id_attribute; ?>" style="<?php rffw_the_meta('inline_style_no_image', $rffw_section); ?>" class="plain-content tweets hidden-xs <?php rffw_the_meta('section-class', $rffw_section); ?>">
	<?php rffw_top_section_border($rffw_section); ?>


	<?php if(!rffw_is_meta('full-width', $rffw_section)): ?>
	  <div class="container">
	<?php endif; ?>

    <div class="row">

      <div class="col-xs-12">
        <div class="post-content body">

          <?php if(rffw_is_meta('show-title', $rffw_section) ): ?>
            <h2 class="section-title subtitle">
              <?php echo apply_filters('the_title', $rffw_section->post_title); ?>
            </h2>
      	  <?php endif; ?>


				<div class="js-swiper-tweets">
	          <div class="swiper-wrapper">

					 <?php


						 if(rffw_is_meta('twitter_username', $rffw_section)){

								$tweets = rffw_get_tweets(rffw_get_meta('twitter_username', $rffw_section));


								if(is_array($tweets)) {
									foreach($tweets as $tweet) {
										?>

										<div class="swiper-slide">
											<blockquote class="twitter-tweet" data-lang="en">
													<p lang="nl" dir="ltr">
														<?php echo TwitterTextFormatter::format_text($tweet); ?>
													</p>
													&mdash; <?php echo $tweet->user->name; ?> (@<?php echo $tweet->user->screen_name; ?>)
													<a href="https://twitter.com/<?php echo $tweet->user->screen_name; ?>/status/<?php echo $tweet->id; ?>"><?php echo date("F j, Y", strtotime($tweet->created_at)); ?></a>
												</blockquote>
												<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
										</div>

									<?php	}
								}
							}
							else {
								_e('Enter twitter username in this section', 'edgerblocks');
							}
					 ?>
					 </div>
					 <div class="swiper-pagination"></div>
				</div>

				<script>
				   var swiper = new Swiper('.js-swiper-tweets', {
				    slidesPerView: 3,
				     spaceBetween: 30,
				     pagination: {
				       el: '.swiper-pagination',
				       clickable: true,
				     },
						 breakpoints: {
							 991: {
								 slidesPerView: 2,
		 				     spaceBetween: 15,
							 },
							 767: {
								 slidesPerView: 1,
		 				     spaceBetween: 15,
							 }
						 }
				   });
				 </script>



        </div>
      </div>

    </div>

		<?php if(!rffw_is_meta('full-width', $rffw_section)): ?>
		  </div>
		<?php endif; ?>

	<?php rffw_edit_section($rffw_section->ID); ?>

  <?php rffw_bottom_section_border($rffw_section); ?>
</section>
<?php } ?>
