<section id="<?php echo $rffw_section->id_attribute; ?>" class="slider slider-home <?php rffw_the_meta('text-style', $rffw_section); ?> <?php rffw_the_meta('section-class', $rffw_section); ?>" style="<?php rffw_the_meta('inline_style', $rffw_section); ?>">
  <?php rffw_top_section_border($rffw_section); ?>
  <svg height="100%" width="100%" class="section-circle">
    <circle class="circle-bottom" cx="50%" cy="120%" r="65%" fill="white" />
    <circle class="circle-right" cx="100%" cy="10%" r="65%" fill="white" />
  </svg>

	<?php
		// Build query from subsections or section options Get Cards posts
		$rffw_cards =  rffw_query_builder($rffw_section, $rffw_section_meta);
	?>
		<div class="swiper-container" data-id="<?php echo $rffw_section->ID; ?>" id="swiper-<?php echo $rffw_section->ID; ?>">
			<div class="swiper-wrapper">

				<?php while ( $rffw_cards->have_posts() ) : $rffw_cards->the_post(); ?>
          <?php $post = get_post(); ?>

  				<div id="slide-<?php the_ID(); ?>" class="swiper-slide slide" data-title="<?php the_title(); ?>">
  					<div class="container">
	  					<article class="post-content">
	  						<div class="col-left">
  						    <div class="card-content">
                    	<div class="card-image showcase-image visible-xs" style="background-image: url(<?php rffw_the_grid_thumbnail_url($post, 'large'); ?>);"></div>
				    	      	<div class="slider-title">
				    	        	<?php the_title();?>
				    	      	</div>
                      <div class="slide-nav">
                        <div class="inslide-button-prev prev inslide-btn">
                    		</div>
                    		<div class="inslide-button-next next inslide-btn">
                    		</div>
                      </div>
	  							</div>
	  						</div>
	  						<div class="col-right showcase-image" style="background-image: url(<?php rffw_the_grid_thumbnail_url($post, 'large'); ?>);"></div>
	  					</article>
  					</div>
  				</div>

				<?php
				endwhile;
				wp_reset_postdata();
				?>
			</div>
      <div class="slider-nav">
        <div class="slider-button-prev prev inslide-btn">
        </div>
        <div class="slider-button-next next inslide-btn">
        </div>
      </div>
		</div>

	<?php rffw_edit_section($rffw_section->ID); ?>

	<?php
	wp_add_inline_script( 'rffw_main', 'jQuery(function($){	"use strict";

		if(jQuery(\'#swiper-'.esc_js($rffw_section->ID).'\').length > 0){
			var swiper'.esc_js($rffw_section->ID).' = new Swiper(\'#swiper-'.esc_js($rffw_section->ID).'\', {

				prevButton: \'.slider-button-prev\',
				nextButton: \'.slider-button-next\',

				slidesPerView: 1,
				spaceBetween: 0,

				loop: true,
        loopedSlides: 10,

				breakpoints: {
					// when window width is <= 320px
					320: {
					},
					// when window width is <= 768px
					768: {
					},
					// when window width is <= 992px
					992: {
					}
				}
			});
		}
    swiper'.esc_js($rffw_section->ID).'.on("transitionEnd", function() {
      	$(".inslide-button-next").on("click", function(){
      		$(".slider-nav .slider-button-next").trigger("click");
      	});
        $(".inslide-button-prev").on("click", function(){
      		$(".slider-nav .slider-button-prev").trigger("click");
      	});
    });

	}); ');
	?>

	<?php rffw_bottom_section_border($rffw_section); ?>
</section>
