<?php
	global $post;

	$args = array(
		'category__in'   => wp_get_post_categories($post->ID),
		'posts_per_page' => 3,
		'post__not_in'   => array($post->ID)
	);

	$related = new WP_Query($args);
	$show_related = is_single();
?>

<?php if ( $show_related && $related->have_posts() ) : ?>
	<section id="post-related-<?php the_ID(); ?>" <?php post_class('post-related'.rffw_get_meta('text-style', $post));?> >
		<div class="container">
			<h2><?php _e('Relevante artikelen', 'edgerblocks'); ?></h2>
			<div class="row"><?php
				$show_date = true;
				while ( $related->have_posts() ) : $related->the_post();
					$post = get_post();
					?>
					<div class="col-md-4 col-sm-6">
						<div class="grid-item">
							<?php include(get_template_directory().'/template-parts/card-post.php'); ?>
						</div>
					</div>
			<?php	endwhile;	?>
			<?php wp_reset_postdata(); ?>
			</div>
		</div>
	</section>
<?php endif; ?>
