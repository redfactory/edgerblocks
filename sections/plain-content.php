<?php
	$section_style = rffw_get_meta('section-plain-style', $rffw_section);
	$feat_image = get_the_post_thumbnail($rffw_section->ID, 'large');
	$style_bg = "background-image: url('" . $rffw_section->rffw_meta['featured_image_url'] . "'); ";
	$section_class ='';

	$show_img_in_section_bg = true;
	$section_style = rffw_get_meta('section-plain-style', $rffw_section);
	if($section_style == 'image-left' || $section_style == 'image-right' || $section_style == 'big-header' || $section_style == 'winnaar') {
		$show_img_in_section_bg = false;
	}

	$section_class = $section_style;
?>

<section id="<?php echo $rffw_section->id_attribute; ?>" style="<?php rffw_the_meta('inline_style', $rffw_section); ?> <?php echo $show_img_in_section_bg ? $style_bg : ''; ?>" class="plain-content <?php rffw_the_meta('text-style', $rffw_section); ?> <?php rffw_the_meta('section-class', $rffw_section); ?> <?php if(rffw_is_meta('section-parallax', $rffw_section)) echo 'parallax'; ?> <?php echo $section_class; ?>">
	<?php rffw_top_section_border($rffw_section); ?>

  <?php if(rffw_is_meta('section-video', $rffw_section)): ?>
  <div class="section-video">
    <video playsinline autoplay muted loop onclick="this.paused ? this.play() : this.pause();">
      <source src="<?php rffw_the_meta('section-video', $rffw_section); ?>" type="video/mp4">
    </video>
  </div>
  <?php endif; ?>

	<div class="container <?php if(rffw_is_meta('full-width', $rffw_section)) echo 'full-width'; ?>">

	<?php if($section_style == 'image-left'|| $section_style == 'image-right') : ?>
		<div class="row row-eq-height">
			<div class="col-xs-12 col-sm-8 <?php echo $section_style == 'image-left' ? 'col-sm-push-4' : ''; ?>">
				<div class="content-vertical-centered">
					<div class="post-content body">
						<?php if(rffw_is_meta('show-title', $rffw_section)): ?>
				      <h2 class="section-title subtitle">
				        <?php echo apply_filters('the_title', $rffw_section->post_title); ?>
				      </h2>
					  <?php endif; ?>
			      <?php echo apply_filters('the_content', $rffw_section->post_content); ?>
			    </div>
				</div>
			</div>
			<div class="hidden-xs col-sm-4 <?php echo $section_style == 'image-left' ? 'col-sm-pull-8 image-left' : ''; ?>">
				<?php echo $feat_image; ?>
			</div>
		</div>
	<?php elseif($section_style == 'big-header') : ?>
		<div class="header big-header" style="<?php echo $style_bg; ?>">
			<?php if(rffw_is_meta('show-title', $rffw_section)): ?>
				<?php if(isset($feat_image)) : ?>
					<div class="image visible-xs">
						<?php echo $feat_image; ?>
					</div>
				<?php endif; ?>
				<div class="title">
					<h2 class="section-title subtitle">
						<?php echo apply_filters('the_title', $rffw_section->post_title); ?>
					</h2>
				</div>
			<?php endif; ?>
		</div>
		<div class="post-content body">
			<?php echo apply_filters('the_content', $rffw_section->post_content); ?>
		</div>
	<?php elseif($section_style == 'winnaar') : ?>
		<div class="row">
			<div class="col-sm-6 winnaar-col">
				<?php if(rffw_is_meta('show-title', $rffw_section)): ?>
					<h2 class="section-title subtitle">
						<?php echo apply_filters('the_title', $rffw_section->post_title); ?>
					</h2>
				<?php endif; ?>
				<?php echo $feat_image; ?>
			</div>
			<div class="col-sm-6 winnaar-col">
				<div class="post-content body">
					<?php echo apply_filters('the_content', $rffw_section->post_content); ?>
				</div>
			</div>
		</div>
	<?php else : ?>
		<?php if($section_style == 'boxed') : ?>
			<div class="boxed">
		<?php endif; ?>
			<?php if(rffw_is_meta('show-title', $rffw_section)): ?>
				<h2 class="section-title subtitle">
					<?php echo apply_filters('the_title', $rffw_section->post_title); ?>
				</h2>
			<?php endif; ?>
			<div class="post-content body">
				<?php echo apply_filters('the_content', $rffw_section->post_content); ?>
			</div>
		<?php if($section_style == 'boxed') : ?>
			</div>
		<?php endif; ?>

	<?php endif; ?>
</div>
	<?php rffw_edit_section($rffw_section->ID); ?>

  <?php rffw_bottom_section_border($rffw_section); ?>
</section>
