<section id="page-content-<?php the_ID(); ?>" <?php post_class('error404'); ?> >
	<div class="container">

		<?php if(rffw_is_theme('404-content')){ ?>
			<?php rffw_the_theme('404-content'); ?>
		<?php } else{ ?>
			<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try the navigation menu or a search?', 'edgerblocks' ); ?></p>
		<?php } ?>

	</div>
</section>
