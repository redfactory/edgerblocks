<section id="<?php echo $rffw_section->id_attribute; ?>" class="slider full-width <?php rffw_the_meta('text-style', $rffw_section); ?> <?php rffw_the_meta('section-class', $rffw_section); ?>" style="<?php rffw_the_meta('inline_style', $rffw_section); ?>">

  	<?php rffw_top_section_border($rffw_section); ?>

	<?php
		// Build query from subsections or section options
		$rffw_cards = rffw_query_builder($rffw_section, $rffw_section_meta);
	?>

    <?php if(rffw_is_meta('boxed', $rffw_section)): ?><div class="container"><?php endif; ?>
			<div class="swiper-container" data-id="<?php echo $rffw_section->ID; ?>" id="swiper-<?php echo $rffw_section->ID; ?>">
				<div class="swiper-wrapper">

				<?php	$slides_height = (rffw_is_meta('slides-height', $rffw_section))? rffw_get_meta('slides-height', $rffw_section) : 400; ?>

				<?php while ( $rffw_cards->have_posts() ) : $rffw_cards->the_post(); ?>
          <?php $post = get_post(); ?>

					<div id="slide-<?php the_ID(); ?>" class="swiper-slide slide" data-title="<?php the_title(); ?>">

						<?php	if(rffw_is_meta('click-through-link', $rffw_section)) :?>
							 <a href="<?php rffw_the_link(get_permalink(), $post); ?>">
						<?php endif; ?>

						  	<article class="post-content" style="<?php rffw_the_meta('inline_style_no_image', $post); ?> height:<?php echo $slides_height; ?>px; background-image: url(<?php rffw_the_grid_thumbnail_url($post, 'fullwidth'); ?>);">

								<?php if(rffw_is_meta('show-content', $rffw_section)): ?>

							    <div class="post-content body">
							    	<div class="container">
							          	<h3><?php the_title(); ?></h3>

                          <?php if(rffw_is_meta('show-meta', $rffw_section)){ ?>
                            <?php get_template_part( 'template-parts/info', get_post_type() ); ?>
                          <?php } ?>
							    	</div>
						    	</div>

								<?php endif; ?>

							</article>

							<?php if(rffw_is_meta('click-through-link', $rffw_section)) :?>
								</a>
							<?php endif; ?>

					</div>

					<?php
					endwhile;
					wp_reset_postdata();
					?>
				</div>

				<?php if(rffw_is_meta('pagination', $rffw_section)): ?>
				<div id="swiper-pagination-<?php echo $rffw_section->ID; ?>" class="swiper-pagination"></div>
				<?php endif; ?>

				<?php if(rffw_is_meta('arrows', $rffw_section)): ?>
				<div class="slider-arrows-container">
					<div class="slider-arrows">
						<div class="swiper-button-prev prev" id="swiper-prev-<?php echo $rffw_section->ID; ?>">
							<span class="glyphicon glyphicon-play icon-flipped"></span>
						</div>
						<div class="swiper-button-next next" id="swiper-next-<?php echo $rffw_section->ID; ?>">
							<span class="glyphicon glyphicon-play"></span>
						</div>
					</div>
				</div>
				<?php endif; ?>


			</div>
      <?php if(rffw_is_meta('boxed', $rffw_section)): ?></div><?php endif; ?>


	<?php
	// Build swiper js
	$pagination_swiper_line =  (rffw_is_meta('pagination', $rffw_section)) ? 'pagination: \'#swiper-pagination-'.$rffw_section->ID.'\',' : '';
	$prev_next_swiper_line =  (rffw_is_meta('arrows', $rffw_section)) ? 'prevButton: \'#swiper-prev-'.$rffw_section->ID.'\', nextButton: \'#swiper-next-'.$rffw_section->ID.'>\',' : '';

	wp_add_inline_script( 'rffw_main', 'jQuery(function($){	"use strict";
			if(jQuery(\'#swiper-'.esc_js($rffw_section->ID).'\').length > 0){
				var swiper'.esc_js($rffw_section->ID).' = new Swiper(\'#swiper-'.esc_js($rffw_section->ID).'\', {
					'.$pagination_swiper_line.'
					'.$prev_next_swiper_line.'
					slidesPerView: 1,
					spaceBetween: 0,
					paginationClickable: true,
					loop: true,
					breakpoints: {
						320: {
						},
						768: {
						},
						992: {
						}
					}
				});
			}
		});');

	?>

	<?php rffw_edit_section($rffw_section->ID); ?>
	<?php rffw_top_section_border($rffw_section); ?>
</section>
