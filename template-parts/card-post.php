<?php
	if(!isset($show_date)) {
		$show_date = false;
	}
?>
<a class="grid-item-container" href="<?php rffw_the_link(get_permalink(), $post); ?>">
	<div class="grid-item-content">
		<div class="image" style="background-image: url(<?php rffw_the_grid_thumbnail_url($post, 'large'); ?>);"></div>
		<div class="content">
			<h3><?php the_title(); ?></h3>
			<div class="date"><?php echo $show_date ? get_the_time('d-m-Y') : ''; ?></div>
			<p>
				<?php
					$excerpt = get_the_excerpt();
					echo str_replace(['<p>', '</p>'], '', $excerpt);
				?>
			</p>
		</div>
	</div>
</a>
