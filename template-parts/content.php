<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Stichting RPO
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="post-content">

		<?php if(has_post_thumbnail()) { ?>
			<div class="featured-image">
				<?php the_post_thumbnail('container'); ?>
			</div>
		<?php } ?>

		<?php the_title( sprintf( '<h3 class="post-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' ); ?>

		<?php the_excerpt(''); ?>

		<?php if ( 'post' === get_post_type() ) : ?>
		<div class="post-meta metadata">
			<?php echo get_avatar( get_the_author_meta( 'ID' ), 60 ); ?>
			<div class="post-meta-author">
				<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><?php the_author(); ?></a>
			</div>
			<div class="post-meta-date">
				<?php the_date('M jS Y') ?>
			</div>
		</div>
		<?php endif; ?>

		<!-- <div class="post-readmore">
			<a href="<?php the_permalink(); ?>" rel="bookmark"> Read more&hellip;</a>
		</div> -->

	</div>

</article>
