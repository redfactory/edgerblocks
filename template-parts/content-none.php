<?php
/**
 * Template part for displaying a message that posts cannot be found.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Stichting RPO
 */

?>

<div class="list-item">
	<article class="content-none">
		<div class="post-content">
			<h3 class="post-title"><?php esc_html_e( 'Nothing Found', 'edgerblocks' ); ?></h3>

			<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

				<p>
				<?php printf( wp_kses( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'edgerblocks' ), array( 'a' => array( 'href' => array() ) ) ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

			<?php elseif ( is_search() ) : ?>

				<p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'edgerblocks' ); ?></p>

			<?php else : ?>

				<p><?php esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'edgerblocks' ); ?></p>
				<?php get_search_form(); ?>

			<?php endif; ?>
		</div>
	</article>
</div>