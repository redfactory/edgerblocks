<div class="attachments">
	<h3><?php _e('Bijlages voor deze pagina', 'edgerblocks'); ?></h3>
	<ul>
		<?php
		foreach ( $attachments as $attachment ) {
			$class = "post-attachment mime-" . sanitize_title( $attachment->post_mime_type );
			$thumbimg = wp_get_attachment_link( $attachment->ID, 'thumbnail-size', true );
			echo '<li class="' . $class . ' data-design-thumbnail" target="_blank">' . $thumbimg . '</li>';
		}
		?>
	</ul>
</div>
