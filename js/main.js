jQuery(function($){
	"use strict";

	//jQuery('section.page-header').css('padding-top', jQuery('header').outerHeight());

	jQuery( ".js-accordion" ).accordion();
	jQuery( ".js-tabs" ).tabs({
    create: function( event, ui ) {setMenuItem();}
	});

	$('.inslide-button-next').on('click', function(){
		$('.slider-nav .slider-button-next').trigger('click');
	});

	$('.inslide-button-prev').on('click', function(){
		$('.slider-nav .slider-button-prev').trigger('click');
	});

	// Mobile menu button
	var $menuButton = jQuery('.menu-button');
	$menuButton.on('click', function(event){
		$menuButton.toggleClass('cross');
		jQuery('.wp_nav_menu_mobile').toggleClass('showMenu');
		event.preventDefault();
	});

// trigger close
jQuery('nav.wp_nav_menu_mobile').on('click', function(event){
	if (event.target !== this)
		return;

	$menuButton.toggleClass('cross');
	jQuery('.wp_nav_menu_mobile').toggleClass('showMenu');
	event.preventDefault();
});

//Event menu make sticky to top
if($("section.page-header-event").length){
	var menuTop = ($("section.page-header-event").offset().top + $("section.page-header-event").height()) - $("#container > header").outerHeight();
	$('#fixed-submenu').affix({
		offset: {
			top: menuTop}
	});
}

if($('.submenu-mobile .first-menu-item').length > 0){
	$('.submenu-mobile .first-menu-item').text($('#anchor-menu ul.menu li:first-child a').text());
}
$('#anchor-close-button').detach().appendTo('#anchor-menu ul.menu li:first-child');

	//Mobile menu
	jQuery('#dl-menu ul.sub-menu').each(function(){
		jQuery(this).addClass('dl-submenu');
	});
	jQuery( '#dl-menu' ).dlmenu({
		animationClasses : { classin : 'dl-animate-in-1', classout : 'dl-animate-out-1' }
	});


	// Put mainmenu items in dropdown when there's too much of 'em
	var $menuMenu = $('#mainMenu').find('ul.menu');

	function hideMenuOverflow() {
		var $obj_dropdown = $('<ul class="sub-menu">');

		var rffw_menu_length = $menuMenu.outerWidth();
		var rffw_li_total_length = 130;
		var rffw_has_dropdown = false;

		$menuMenu.find('li.extra-menu-items').remove();

		$menuMenu.children('li').each(function(){
			$(this).removeClass('hidden');

			var rffw_li_length = $(this).outerWidth();
			rffw_li_total_length = rffw_li_total_length + rffw_li_length;

			if ( rffw_li_total_length > rffw_menu_length ) {
				rffw_has_dropdown = true;
				$obj_dropdown.append($(this).clone());
				$(this).addClass('hidden');
			}
		});

		if (rffw_has_dropdown == true) {
			var $obj_dropdown_complete = $('<li class="menu-item menu-item-has-children extra-menu-items">').append($obj_dropdown);
			$obj_dropdown_complete.prepend('<a href="#"><span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></a>');
			$menuMenu.append($obj_dropdown_complete);
		}
	}

function subMenuLeftOrRight(){
	jQuery('ul.sub-menu', '#mainMenu').each(function( index ) {
		var rightPos = $(this).offset().left + $(this).outerWidth();
		var windowWidth = $( window ).width();

		if(rightPos > windowWidth){
			$(this).addClass('move');
		}
		else{
			$(this).removeClass('move');
		}
	});

}


	jQuery( window ).resize(function() {
		hideMenuOverflow();
		subMenuLeftOrRight();
	});

	jQuery(window).bind("load", function() {
		hideMenuOverflow();
		subMenuLeftOrRight();
	});

	//Grid block content animation
	/*jQuery('section.blocks').find('.grid-item-container').click(function(){
		jQuery('section.blocks').find('.grid-content-popup').removeClass('active');
		jQuery(this).parents('.grid-item').find('.grid-content-popup').addClass('active');
		return false;
	});
	jQuery('section.blocks').find('.grid-content-popup').find('.glyphicon').click(function(){
		jQuery(this).parent().removeClass('active');
		return false;
	});*/

	// Stick header
	jQuery(window).scroll(function() {

		var header = jQuery('#container > header:not(.static)');
		var scrollspace = jQuery(window).scrollTop();

		if (scrollspace > 0) {
			if (!header.hasClass('sticky')) {
				header.addClass('sticky no-sticky-hover');
			}
		} else {
			if (header.hasClass('sticky')) {
				header.removeClass('sticky no-sticky-hover');
			}
		}
	});

	jQuery('#container > header').hover(
		function(){
			if (!jQuery(this).hasClass('no-sticky-hover')) {
				jQuery(this).addClass('sticky');
			}
		},
		function(){
			if (!jQuery(this).hasClass('no-sticky-hover')) {
				if(!jQuery('form.searchform.mobile').hasClass('show-form')){
					jQuery(this).removeClass('sticky');
				}
			}
		}
	);

	// Masonry
	var masonryGrids = [];
	jQuery('.masonry-grid-js').each( function( i, container ) {
		var $grid = jQuery(this).masonry({
			"itemSelector"	: ".grid-item",
			"gutter"				: 0
		});
		masonryGrids.push($grid);
	});
	jQuery.each(masonryGrids, function( i, value ) {
		masonryGrids[i].imagesLoaded().progress( function() {
			masonryGrids[i].masonry('layout');
		});
	});


    jQuery('.site-search-open').on('click', function(event){
    	jQuery('form.searchform').toggleClass('show-form');
			if(jQuery('form.searchform').is(':visible')){
				jQuery('form.searchform input').focus();
			}
			event.preventDefault();
		});

    var $svgObjs = jQuery('#sections').find('section').find('svg');
    jQuery(window).load(function(){
    	$svgObjs.each(function(index, svg){
    		var s = Snap(svg);
    		var pathSet = s.selectAll( 'path' );

    		if (jQuery(this).hasClass('loop')) {
    			var animType = 'loop';
    		} else {
    			var animType = 'scroll';
    		}

    		if ( jQuery(this).offset().top < (jQuery(window).scrollTop() + jQuery(window).height()) ) {
    			pathSet.forEach( function( path ) {
    				if (!path.hasClass('started')) {
						animatePath(path, animType);
					}
    			});
    		}
    	});
    }).scroll(function() {
    	$svgObjs.each(function(index, svg){
    		var s = Snap(svg);
    		var pathSet = s.selectAll( 'path' );

    		if (jQuery(this).hasClass('loop')) {
    			var animType = 'loop';
    		} else {
    			var animType = 'scroll';
    		}

    		if ( jQuery(this).offset().top < (jQuery(window).scrollTop() + jQuery(window).height() - 10) ) {
    			pathSet.forEach( function( path ) {
					if (!path.hasClass('started')) {
						if (animType == 'loop') {
							path.addClass('started');
						}
						animatePath(path, animType);
					}
				});
    		} else {
    			if (animType == 'scroll') {
	    			pathSet.forEach( function( path ) {
	    				if (!path.hasClass('started')) {
							resetPath(path, animType);
						}
	    			});
	    		}
    		}
    	});
    });

	function animatePath(path, animType){
		if (path.attr('data-path-loop')){

			if(path.attr('data-easing')){
				 var easing = eval(path.attr('data-easing'));
			}
			else{
				var easing = mina.easeinout;
			}

			if(path.attr('data-speed')){
				var speed = eval(path.attr('data-speed'));
			}
			else{
				if (animType == 'loop') {
					var speed = 4000;
				} else {
					var speed = 1000;
				}
			}

			path.animate( { 'path' : path.attr('data-path-loop') }, speed, easing, function() {
				if (animType == 'loop') {
	     			resetPath(path, animType);
	     		}
 			});
		}
	}

	function resetPath(path, animType){
		if (path.attr('data-path-original')){

			if(path.attr('data-easing')){
				 var easing = eval(path.attr('data-easing'));
			}
			else{
				var easing = mina.easeinout;
			}

			if(path.attr('data-speed')){
				var speed = eval(path.attr('data-speed'));
			}else{
				if (animType == 'loop') {
					var speed = 4000;
				} else {
					var speed = 1000;
				}
			}

			path.animate( { 'path' : path.attr('data-path-original') }, speed, easing, function() {
				if (animType == 'loop') {
	     			animatePath(path, animType);
	     		}
 			});
		}
	}

	//jQuery(window).trigger('resize');
});



function setMenuItem(){
	jQuery('.navbar-tabs').each(function(){
		jQuery(this).find('button span').text(jQuery(this).find('ul li.ui-state-active a').text());
	});

	jQuery('.navbar-tabs ul li a').on('click', function(){
		jQuery(this).parents('.navbar-tabs').find('button span').text(jQuery(this).parents('.navbar-tabs').find('ul li.ui-state-active a').text());
		jQuery(this).parents('.navbar-tabs').find('button').trigger('click');
	});

}
/*
jQuery(window).on('hashchange', function(event) {
		var hash = location.hash.substr(1);
		console.log(jQuery("header:first" ).outerHeight());
		if(jQuery('#section-' + hash).length){
				jQuery('html,body').animate({
						scrollTop: jQuery('#section-' + hash).offset().top - jQuery("header:first" ).outerHeight() - 61
				}, 500);
				event.preventDefault();
		}
});
*/

jQuery(window).on('resize', function(){});

jQuery(window).scroll(function(){});

var blockHashChangeTrigger = false;
jQuery(window).on('hashchange', function(event){
	if(!blockHashChangeTrigger){

		var hash = location.hash;
		if (hash) {
			blockHashChangeTrigger = true;
			scrollToDiv(hash);

			setTimeout(function(){
				blockHashChangeTrigger = false;
			}, 500);
		}
	}
	event.preventDefault();

}).trigger('hashchange');

function scrollToDiv(hash){
	if(hash.substring(0,1) == '#'){
		strippedHash = hash.substr(1);
	}

	if(strippedHash.indexOf('#') > 0){
		strippedHash = strippedHash.substr(strippedHash.indexOf('#') + 1);
	}

	jQuery('html,body').animate({
			scrollTop: jQuery('#section-' + strippedHash).offset().top - jQuery("header:first" ).outerHeight() - 61
	}, 500);
}
